#Here is a simple makefile that just calls ROOT to compile your source code
CXX = g++

# Define flags. -D_VANILLA_ROOT_ is needed to avoid StMessMgr confusion                
CFLAGS = $(shell root-config --cflags) -O2 -fPIC -Wall -pipe -std=c++11 -D_VANILLA_ROOT_ -I.
LIBS = $(shell root-config --libs)
INCS = $(shell root-config --incdir)

# Define output library                                                                
STPICODST = libStPicoDst.so

# Compile all *.cxx classes in the directory                                           
SRC = $(shell find . -name "*.cxx")


all:
	$(MAKE) --directory=src/submodules/glaubermcmodel/
#	rm src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoDst_Dict.h
	$(MAKE) $(STPICODST) --directory=src/submodules/datacollectorreaderlibs/PicoDstReader/
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/libStPicoDst.so bin/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/PhysicalConstants.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoArrays.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoBbcHit.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoBEmcPidTraits.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoBTofHit.h inc/. 
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoBTofPidTraits.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoBTowHit.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoCommon.h inc/.	
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoDst.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoDstLinkDef.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoDstReader.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoEmcTrigger.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoEpdHit.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoEvent.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoFmsHit.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoHelix.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoMessMgr.h inc/. 
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoMtdHit.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoMtdPidTraits.h inc/. 
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoMtdTrigger.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoPhysicalHelix.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoTrackCovMatrix.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoTrack.h inc/.
	ln -sf ../src/submodules/datacollectorreaderlibs/PicoDstReader/SystemOfUnits.h inc/.
	root -l -q -b macros/makeLibs.C

$(STPICODST): $(SRC:.cxx=.o) StPicoDst_Dict.C
	$(CXX) $(CFLAGS) -shared $^ -o $(STPICODST) $(LIBS)

%.o: %.cxx
	$(CXX) -fPIC $(CFLAGS) -c -o $@ $<

StPicoDst_Dict.C: $(shell find . -name "*.h" ! -name "*LinkDef*")
	rootcint -f $@ -c -D_VANILLA_ROOT_ -DROOT_CINT -D__ROOT__ -I. -I$(INCS) $^ StPicoDstLinkDef.h


clean:
	root -l -q -b macros/makeLibs.C\(\"clean\"\)
	rm -vf *.o StarRoot/*.o StPicoDst_Dict* $(STPICODST)
	rm src/submodules/datacollectorreaderlibs/PicoDstReader/StPicoDst_Dict.h
	ln -s ../src/submodules/datacollectorreaderlibs/PicoDstReader/libStPicoDst.so bin/.	
