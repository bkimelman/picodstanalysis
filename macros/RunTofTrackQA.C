//This macro runs the compiled trackQAmaker binary.
//The source code for toftrackQAmaker.cxx can be found at
//  src/qa/toftrackQAmaker.cxx

void RunTofTrackQA(TString inputFile, TString outputFile, TString starLibrary, Bool_t trackCuts, Int_t nEvents, Double_t energy, TString eventConfig){

		//Load the Necessary Libraries
		gSystem->Load("../bin/TrackInfo_cxx.so");
    gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
    gSystem->Load("../bin/EventInfo_cxx.so");
    gSystem->Load("../bin/ParticleInfo_cxx.so");
		gSystem->Load("../bin/DavisDstReader_cxx.so");
		gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
    gSystem->Load("../bin/UserCuts_cxx.so");
    gSystem->Load("../bin/toftrackQAmaker_cxx.so");

		SetVariableUserCuts(energy,eventConfig,starLibrary);

    toftrackQAmaker(inputFile,outputFile,trackCuts,nEvents);

}
