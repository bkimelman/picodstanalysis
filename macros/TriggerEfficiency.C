//This is a simple macro demonstrating:
//1. That the DavisDstReader class can
//   now accept wildcards which allow you to loop over all of the data files
//   at the same time!
//2. That you can use the EventInfo::ContainsTriggerID() function to cut out
//   trigger IDs that you do not want

#include <iostream>
#include <iomanip>
#include <vector>
#include <utility>

//Note that you can define your own functions!
Int_t GetRunNumberIndex(Int_t runNumber, std::vector <int> *runNumbers){

  for (unsigned int i=0; i<runNumbers->size(); i++){
    if (runNumber == runNumbers->at(i))
      return i;
  }

  return -1;
}

//This is the MAIN function
void TriggerEfficiency(){

  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");

  //Variables the User may want to change
  const int trigID = 1;

  //Create a DavisDstReader Object with a wild card *
  //This will allow you to run over all the files in this directory that end with ".root"
  DavisDstReader reader("/scratch_menkar/FixedTargetData/AuAu_4_5GeV_2015/*.root");

  //Since we don't need the track info yet, lets turn off the branche.
  //This will speed up the processing time dramatically
  reader.SetBranchStatus("TrackInfo",0);
  
  //Get the Total Number of Events
  Long64_t nTotalEvents = reader.GetEntries();

  //-----------------------------------------------------------------------
  //Print the Total Number of Triggers in ALL the files
  cout <<"Total Number of Triggers: " <<nTotalEvents <<endl;

  //-----------------------------------------------------------------------
  //Loop Over all the Triggers and count the number of Triggers in each run
  std::vector <int> runNumbers;
  std::vector <long> nTriggersPerRun;
  std::vector <long> nTriggersPerRun_FXT;
  std::vector <long> nTriggersPerRun_FXT_Index0;
  std::vector <long> nTriggersPerRun_FXT_Index0_Vz;
  for (unsigned int iEvent=0; iEvent<nTotalEvents; iEvent++){

    EventInfo *event = reader.GetEntry(iEvent);

    //Get the index of this run number from the runNumber vector
    //This function returns -1 if this is a new run number
    int runIndex = GetRunNumberIndex(event->GetRunNumber(),&runNumbers);
    
    //If this is a new run number then create a new entry in the vectors
    if (runIndex == -1){
      runNumbers.push_back(event->GetRunNumber());
      nTriggersPerRun.push_back(0);
      nTriggersPerRun_FXT.push_back(0);
      nTriggersPerRun_FXT_Index0.push_back(0);
      nTriggersPerRun_FXT_Index0_Vz.push_back(0);
    }
    
    //otherwise increment the trigger counts
    else {

      //Increment the total number of triggers in this run
      nTriggersPerRun.at(runIndex)++;

      //Test for TriggerID == 1
      if (event->ContainsTriggerID(trigID)){

	//Increment the count of FXT triggers in this run
	nTriggersPerRun_FXT.at(runIndex)++;

	//Get the Primary Vertex
	PrimaryVertexInfo *vertex = event->GetPrimaryVertex(0);
	
	//Test for MuDstIndex == 0
	if (vertex->GetVertexIndex() == 0){

	  //Increment the number of FXT triggers with index 0 in this run
	  nTriggersPerRun_FXT_Index0.at(runIndex)++;

	  //Test of Z Vertex Location
	  if (vertex->GetZVertex() > 210. && vertex->GetZVertex() < 212.){

	    //Increment the number of FXT triggers with index 0 and on target in this run
	    nTriggersPerRun_FXT_Index0_Vz.at(runIndex)++;
	  }//End Test for Z Vertex Location
	  
	}//End Test for Vertex Index

      }//End Test for Trigger ID


    }//End Test for new run number

        
  }//End Loop Over Triggers

  //-----------------------------------------------------------------------
  //Sort the Run Numbers so we can Print them in numerical order
  std::vector <int> sortedIndex(runNumbers.size());
  TMath::Sort(sortedIndex.size(),&runNumbers.at(0),&sortedIndex.at(0),false);

  //-----------------------------------------------------------------------
  //Print Results
  int colWidth = 15;
  cout <<Form("%s%13s%13s%13s%13s%13s\n",
	      "Run Num","Total","FXT Trig","+Index 0","+ Vz","Trig. Eff.");

  for (unsigned int iSorted; iSorted<runNumbers.size(); iSorted++){
    
    int iRun = sortedIndex.at(iSorted);
    
    cout <<Form("%d%13d%13d%13d%13d%13.4F\n",
		runNumbers.at(iRun),nTriggersPerRun.at(iRun),
		nTriggersPerRun_FXT.at(iRun),nTriggersPerRun_FXT_Index0.at(iRun),
		nTriggersPerRun_FXT_Index0_Vz.at(iRun),
		nTriggersPerRun_FXT_Index0_Vz.at(iRun) / (double)nTriggersPerRun.at(iRun));
  }
  
  return;
  
}
