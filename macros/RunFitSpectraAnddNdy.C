//Runs the source code responsible for fitting the spectra and extracting
//the dNdy values. See src/analysis/fitSpectraAnddNdy

Bool_t drawSpectra = false; //Should the spectra be drawn? This overrides the values in the source code.

void RunFitSpectraAnddNdy(TString resultsFileName, Int_t pid, Int_t charge,
			  Double_t energy,
			  Int_t userCentBin = -999, Double_t userRapidity = -999){

  //Load the necessary Librarires
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/SpectraClass_cxx.so");
  gSystem->Load("../bin/SpectraFitFunctions_cxx.so");
  gSystem->Load("../bin/SpectraFitUtilities_cxx.so");
  gSystem->Load("../bin/SimFitter_cxx.so");
  //gSystem->Load("../bin/fitSpectra_cxx.so");
  gSystem->Load("../bin/fitPionSpectra_cxx.so");
  gSystem->Load("../bin/fitProtonSpectra_cxx.so");
  gSystem->Load("../bin/fitKaonSpectra_cxx.so");
  
  draw = drawSpectra;
  
  SetVariableUserCuts(energy,"","");

  //Pions
  if (pid == 0)
    fitPionSpectra(resultsFileName,charge,userCentBin,userRapidity);
  //Kaons
  else if (pid == 1)
    fitKaonSpectra(resultsFileName,charge,userCentBin,userRapidity);
  //Protons
  else if (pid == 2)
    fitProtonSpectra(resultsFileName,charge,userCentBin,userRapidity);
  
}
