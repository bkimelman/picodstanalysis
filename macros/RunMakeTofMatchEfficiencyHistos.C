//This macro runs the compiled code MakeTofMatchEfficiencyHistos.cxx which is
//located in src/tofMatchingEfficiency/

void RunMakeTofMatchEfficiencyHistos(TString inputFile, TString outputFile, TString starLibrary,
				     Double_t energy, TString eventConfig, Long64_t nEvents=-1){


  //Load The Necessary Libs
  gSystem->Load("../bin/utilityFunctions_cxx.so");
  gSystem->Load("../bin/TrackInfo_cxx.so");
  gSystem->Load("../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../bin/EventInfo_cxx.so");
  gSystem->Load("../bin/ParticleInfo_cxx.so");
  gSystem->Load("../bin/DavisDstReader_cxx.so");
  gSystem->Load("../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../bin/UserCuts_cxx.so");

  gSystem->Load("../bin/MakeTofMatchEfficiencyHistos_cxx.so");

  //Set the User Cuts
  SetVariableUserCuts(energy,eventConfig,starLibrary);
  
  MakeTofMatchEfficiencyHistos(inputFile,outputFile,nEvents);
  
}
