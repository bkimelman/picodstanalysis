#include <vector>
#include <utility>

#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TF1.h>
#include <TPad.h>
#include <TH1F.h>
#include <TPaveText.h>
#include <TLegend.h>
#include <TMarker.h>
#include <TSystem.h>

#include "../../inc/globalDefinitions.h"
#include "../CanvasPartition.h"
#include "../../inc/StyleSettings.h"

//________________________________________________________________________________
TPad *DrawSpectraAllRapiditySingleCentrality(TFile *file, TPad *pad, Int_t pid, Int_t charge, Int_t centBin,
					     Double_t midY, Double_t startmTm0, Double_t stopmTm0,
					     Bool_t corrected, Double_t scaleFactor,Int_t whichFit){

  const float MIN_MTM0 = .1;
  
  ParticleInfo particleInfo;
  //TString eventConfig = GetEventConfiguration();

  /*
  TString spectraClassDir = TString::Format("CombinedCorrectedSpectraClass_%s",
					    particleInfo.GetParticleName(pid,charge).Data());
  TString spectrumDir = TString::Format("CombinedCorrectedSpectra_%s",
					particleInfo.GetParticleName(pid,charge).Data());
  TString spectrumFitDir = TString::Format("CombinedCorrectedSpectra_%s_Fit",
					   spectrumDir.Data());
  */

  /*
  TString spectraClassDir = TString::Format("SpectraClass_%s",
					    particleInfo.GetParticleName(pid,charge).Data());
  TString spectrumDir = TString::Format("CorrectedSpectra_%s",
					particleInfo.GetParticleName(pid,charge).Data());
  TString spectrumFitDir = TString::Format("CombinedCorrectedSpectra_%s_Fit",
					   spectrumDir.Data());
  TString spectrumClassNamePrefix = "CorrectedSpectra";
  TString spectrumClassNamePostfix = "_Total";
  */

  TString spectraClassDir = TString::Format("FitSpectraClass_%s",
					    particleInfo.GetParticleName(pid,charge).Data());
  TString spectrumDir = TString::Format("CorrectedSpectra_%s",
					particleInfo.GetParticleName(pid,charge).Data());
  TString spectrumFitDir = TString::Format("FitSpectraClass_%s_FitFuncs",
					   particleInfo.GetParticleName(pid,charge).Data());
  TString spectrumClassNamePrefix = "CorrectedSpectra";
  TString spectrumClassNamePostfix = "_Total";

  pad->SetLogy();
  pad->SetTicks();

  Double_t yScaleMin(0), yScaleMax(0);
  if (pid == PION){
    yScaleMin = .000000025;//.00025;
    yScaleMax = 35000000;
  }
  if (pid == KAON){
    yScaleMin = .000000025;
    yScaleMax = 350000;
  }
  if (pid == PROTON && charge > 0){
    yScaleMin = .00000025;
    yScaleMax = 350000;
  }
  else if (pid == PROTON && charge < 0){
    yScaleMin = .00000000025;
    yScaleMax = 35000;
  }
  /*
  //SCALE FOR PROTON
  if (pid == PROTON){
    if (eventConfig.Contains("ColliderCenter")){
      yScaleMin = .000025;
      yScaleMax = 30000;
    }
    else if (eventConfig.Contains("ColliderPosY")){
      yScaleMin = .0025;
      yScaleMax = 300000000;
    }
  }
  //SCALE FOR PION
  else if (pid == PION){
    if (eventConfig.Contains("ColliderCenter")){
      yScaleMin = 0.000000000005;
      yScaleMax = 3000000000;
    }
    else if (eventConfig.Contains("ColliderPosY")){
      yScaleMin = .00003;
      yScaleMax = 3000000000000;
    }
    else if (eventConfig.Contains("ColliderNegY")){
      yScaleMin = .00000000000005;
      yScaleMax = 3000;
    }
      
  }
  */
  
  TH1F *frame = pad->DrawFrame(-.09,yScaleMin,stopmTm0+.6,yScaleMax);
  frame->GetXaxis()->SetTitleFont(63);
  frame->GetXaxis()->SetTitleSize(23);
  frame->GetXaxis()->SetLabelFont(63);
  frame->GetXaxis()->SetLabelSize(20);
  
  frame->GetYaxis()->SetTitleFont(63);
  frame->GetYaxis()->SetTitleSize(20);
  frame->GetYaxis()->SetLabelFont(63);
  frame->GetYaxis()->SetLabelSize(18);
  frame->GetYaxis()->SetNdivisions(8);

  frame->GetXaxis()->SetTitleOffset(1.83);
  frame->GetYaxis()->SetTitleOffset(4.50);
  
  frame->GetXaxis()->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",
				   particleInfo.GetParticleSymbol(pid).Data()));
  frame->GetYaxis()->SetTitle(Form("%s",GetSpectraAxisTitle().Data()));

  int yMin = GetMinRapidityIndexOfInterest(pid);
  int yMax = GetMaxRapidityIndexOfInterest(pid);
  int yMid = GetRapidityIndex(midY);

  //return pad;

  double binTitleYLoc(0);
  
  //Loop Over the Rapidity Bins and Draw the Spectra
  for (int yIndex = yMin; yIndex <= yMax; yIndex++){

    cout <<Form("%s/%s_%s_Cent%02d_yIndex%02d%s",
		spectraClassDir.Data(),
		spectrumClassNamePrefix.Data(),
		particleInfo.GetParticleName(pid,charge).Data(),
		centBin,yIndex,
		spectrumClassNamePostfix.Data()) <<endl;
    SpectraClass *spectrum = (SpectraClass *)file->Get(Form("%s/%s_%s_Cent%02d_yIndex%02d%s",
							    spectraClassDir.Data(),
							    spectrumClassNamePrefix.Data(),
							    particleInfo.GetParticleName(pid,charge).Data(),
							    centBin,yIndex,
							    spectrumClassNamePostfix.Data()));
    
    if (!spectrum){
      cout <<"No Spectrum: " <<centBin <<" " <<yIndex <<endl;
      continue;
    }
    spectrum->SetSpectraFile(file);
    spectrum->SetSpectraClassDir(spectraClassDir);
    spectrum->SetSpectraDir(spectrumDir);
    spectrum->SetSpectraFitDir(spectrumFitDir);
    //spectrum->SetSpectraNamePrefix("CombinedSpectrum");
    spectrum->SetSpectraNamePrefix("correctedSpectra");
    spectrum->LoadSpectra();

    //Check for fits
    int nFits = spectrum->GetNSpectrumFits();
    
    TF1 *f = NULL;
    Double_t maxDrawRange = stopmTm0+.1;
    double min,max;
    if (spectrum->GetNSpectrumFits() > 0){
 
      f = spectrum->GetScaledFunction(pow(scaleFactor,yIndex-yMid),whichFit,
				      yIndex==yMid ? kRed : kBlack);
 

      f->GetRange(min,max);
      f->SetRange(min,TMath::Min(maxDrawRange,max));
 
      f->SetNpx(1000);
      f->SetLineWidth(2);
      f->Draw("SAME");
    }
 
    TMultiGraph *g = spectrum->GetScaledCopy(pow(scaleFactor,yIndex-yMid),
					     yIndex==yMid ? kRed : kBlack);
 
    TGraphChop((TGraphErrors *)((TList *)g->GetListOfGraphs())->First(),
	       MIN_MTM0);
    g->Draw("PZ");
 
    double yLabelVal = ((TGraphErrors *)((TList *)g->GetListOfGraphs())->Last())->GetY()
      [((TGraphErrors *)((TList *)g->GetListOfGraphs())->Last())->GetN()-1] * .9;

    if (f){
      yLabelVal = f->Eval(stopmTm0+.15);
    }

    if (yLabelVal > binTitleYLoc){
      binTitleYLoc = yLabelVal*5.0;
    }

    TPaveText *yBin = new TPaveText(maxDrawRange+.1,yLabelVal,maxDrawRange+.2,yLabelVal);
    yBin->SetFillColor(kWhite);
    yBin->SetBorderSize(0);
    yBin->SetTextFont(63);
    yBin->SetTextSize(12);
    yBin->SetTextAlign(11);
    yBin->SetTextColor(yIndex==yMid ? kRed : kBlack);
    yBin->AddText(Form("y = %.01f",GetRapidityRangeCenter(yIndex)));
    yBin->Draw();
  }//End Loop Over Y indices

  /*
  TPaveText *binTitle = new TPaveText(maxDrawRange+.1,binTitleYLoc,maxDrawRange+.3,binTitleYLoc);
  binTitle->SetFillColor(kWhite);
  binTitle->SetBorderSize(1);
  binTitle->SetTextFont(63);
  binTitle->SetTextSize(12);
  binTitle->SetTextAlign(11);
  binTitle->SetTextColor(kBlack);
  binTitle->AddText("y   | #chi^{2}/NDF");
  binTitle->Draw();
  */

  pad->Modified();
  pad->Update();

  return pad;
  
}


//________________________________________________________________________________
void DrawSpectraAllRapidity(TString spectraFile, TString eventConfig, TString system, Double_t energy,
			    Int_t pid, Int_t charge, Int_t whichFit=0, int onlyCentBin=-1, Double_t midY=0.0,
			    Double_t startmTm0=0.0, Double_t stopmTm0=1.5, Bool_t corrected=true){


  Bool_t save = true;        //Should the canvas be saved?
  Double_t scaleFactor = 2.5; //The factor to scale the non- midrapidity spectra by
  //  Int_t yMin = 18;            //Minimum rapidity bin index to draw //Collider 11, PosY 18, NegY
  //Int_t yMax = 40;            //Maximum rapidity bin index to draw //Collider 29, PosY 33, NegY

  gSystem->Load("../../bin/utilityFunctions_cxx.so");
  gSystem->Load("../../bin/TrackInfo_cxx.so");
  gSystem->Load("../../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../../bin/EventInfo_cxx.so");
  gSystem->Load("../../bin/ParticleInfo_cxx.so");
  gSystem->Load("../../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../../bin/UserCuts_cxx.so");
  gSystem->Load("../../bin/SpectraClass_cxx.so");
  SetVariableUserCuts(energy,eventConfig,"");
  ParticleInfo particleInfo;



  const int nCentBins(9);

  //Load the root file
  TFile *file = new TFile(spectraFile,"READ");

  TCanvas *canvas = NULL;

  if (onlyCentBin > 0){
    cout <<"Not Currently Implemented." <<endl;
    return;
  }

  
  canvas= new TCanvas(Form("AllCentCanvas_%s",particleInfo.GetParticleName(pid,charge).Data()),
		      "canvas",20,20,1600,1000);
  CanvasPartition(canvas,5,2,0.05,0.011,0.05,0.02);

  gPad = (TPad *)gROOT->FindObject("pad_9");
  gPad->SetFillColorAlpha(kWhite,1);
  gPad->cd();
  TPaveText *title = new TPaveText(.08,.51,.97,.75,"NDC");
  title->SetFillColor(kWhite);
  title->SetBorderSize(0);
  title->SetTextFont(63);
  title->SetTextSize(40);
  title->SetTextAlign(11);
  title->AddText(Form("%s Spectra",particleInfo.GetParticleSymbol(pid,charge).Data()));
  title->AddText(Form("System: %s",system.Data()));
  title->AddText(Form("Energy: #sqrt{s_{NN}} = %.3g GeV", energy));
  //title->AddText(Form("Config: %s",eventConfig.Data()));
  title->GetLine(1)->SetTextSize(25);
  title->GetLine(2)->SetTextSize(25);
  //title->GetLine(3)->SetTextSize(25);
  title->Draw();

  TMarker *midYColor = new TMarker(0,0,kFullCircle);
  TMarker *nonMidYColor = new TMarker(0,0,kFullCircle);
  midYColor->SetMarkerColor(kRed);
  nonMidYColor->SetMarkerColor(kBlack);
  midYColor->SetMarkerSize(2.0);
  nonMidYColor->SetMarkerSize(2.0);

  TMarker *tpcStyle = new TMarker(0,0,kFullCircle);
  TMarker *tofStyle = new TMarker(0,0,kFullSquare);
  tpcStyle->SetMarkerColor(kGray+1);
  tofStyle->SetMarkerColor(kGray+1);
  tpcStyle->SetMarkerSize(2.0);
  tofStyle->SetMarkerSize(2.0);
  
  TLegend *leg = new TLegend(.21,.13,.78,.42);
  leg->SetFillColor(kWhite);
  leg->SetBorderSize(0);
  leg->SetTextFont(63);
  leg->SetTextSize(25);
  leg->AddEntry(midYColor,"Mid-Rapidity","P");
  leg->AddEntry(nonMidYColor,Form("#times %.2g^{#pm n}",
				  scaleFactor),"P");
  leg->AddEntry((TObject *)NULL,"","");
  leg->AddEntry(tpcStyle,"TPC","P");
  leg->AddEntry(tofStyle,"TOF","P");
  leg->Draw();

  //gPad->Range(-.09,0,2.5,1);
  TGaxis *axis = new TGaxis(0,1,.94,1,-.09,2.1,8,"+",.02);
  axis->SetTitle(Form("m_{T}-m_{%s} (GeV/c^{2})",particleInfo.GetParticleSymbol(pid).Data()));
  axis->SetTitleFont(63);
  axis->SetTitleSize(23);
  axis->SetLabelFont(63);
  axis->SetLabelSize(20);
  axis->SetTitleOffset(1.83);
  axis->Draw();

  TGaxis *axisY = new TGaxis(0,.095,0,1,0,0,0,"U",0.00);
  axisY->Draw();
  
  TPad *pad[nCentBins];
  for (int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    
    gPad = (TPad *)gROOT->FindObject(Form("pad_%i",iCentBin));
    pad[iCentBin] = DrawSpectraAllRapiditySingleCentrality(file,gPad,pid,charge,iCentBin,
							   midY,startmTm0,stopmTm0,corrected,scaleFactor,whichFit);
    //pad[iCentBin]->Draw();

    TPaveText *centTitle = new TPaveText();
    if (iCentBin <= 4){
      centTitle->SetY1NDC(.80);
      centTitle->SetY2NDC(.90);
    }
    else {
      centTitle->SetY1NDC(.85);
      centTitle->SetY2NDC(.93);
    }

    if (iCentBin == 0 || iCentBin == 5){
      centTitle->SetX1NDC(.65);
      centTitle->SetX2NDC(.92);
    }
    else {
      centTitle->SetX1NDC(.58);
      centTitle->SetX2NDC(.93);
    }
    
    centTitle->SetFillColor(kWhite);
    centTitle->SetBorderSize(0);
    centTitle->SetTextFont(63);
    centTitle->SetTextSize(30);
    centTitle->SetTextAlign(31);
    centTitle->AddText(Form("%d-%d%%",
			    iCentBin!=nCentBins-1 ? (int)GetCentralityPercents().at(iCentBin+1):0,
			    (int)GetCentralityPercents().at(iCentBin)));
    centTitle->Draw();
    
  }
  
 

  /*
  TLegend *leg1 = new TLegend(.24,.23,.6,.6);
  leg1->SetFillColor(kWhite);
  leg1->SetBorderSize(1);
  leg1->SetTextFont(63);
  leg1->SetTextSize(25);
  leg1->AddEntry(tpcStyle,"TPC","P");
  leg1->AddEntry(tofStyle,"TOF","P");
  leg1->Draw();
  */
  
  canvas->Modified();
  canvas->Update();

  TString name = TString::Format("SpectraSummary_%s%02d_%s_%s.pdf",
				 system.Data(),
				 (int)energy,
				 eventConfig.Data(),
				 particleInfo.GetParticleName(pid,charge).Data());
  
  if (save){
    canvas->SaveAs(name.Data());
    file->Close();
  }
}


