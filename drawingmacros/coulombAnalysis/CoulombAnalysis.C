#include "../../inc/globalDefinitions.h"
#include "../../inc/StyleSettings.h"
#include "../CanvasPartition.h"

const bool save(true);
const int nEnergies(7);

//________________________________________________________________________________
void CoulombAnalysis(TString eventConfig="ColliderCenter", TString system="AuAu"){

  gStyle->SetOptStat(0);
  
  const int pid = 0; //PION

  //Load the Necessary Libraries
  gSystem->Load("../../bin/utilityFunctions_cxx.so");
  gSystem->Load("../../bin/TrackInfo_cxx.so");
  gSystem->Load("../../bin/PrimaryVertexInfo_cxx.so");
  gSystem->Load("../../bin/EventInfo_cxx.so");
  gSystem->Load("../../bin/ParticleInfo_cxx.so");
  gSystem->Load("../../bin/StRefMultExtendedCorr_cxx.so");
  gSystem->Load("../../bin/UserCuts_cxx.so");
  gSystem->Load("../../bin/SpectraClass_cxx.so");
  gSystem->Load("../../bin/SpectraFitFunctions_cxx.so");
  SetVariableUserCuts(7.7,eventConfig,"");
  ParticleInfo particleInfo;

  //********************************************
  //Ratio Canvas
  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,800,600);
  canvas->cd();
  canvas->SetTopMargin(.05);
  canvas->SetRightMargin(.05);
  canvas->SetLeftMargin(.12);
  canvas->SetTicks();
  
  TH1F *frame = canvas->DrawFrame(0,.79,0.8,1.09);
  
  frame->GetYaxis()->SetTitleFont(63);
  frame->GetYaxis()->SetTitleSize(30);
  frame->GetYaxis()->SetLabelFont(63);
  frame->GetYaxis()->SetLabelSize(22);
  frame->GetYaxis()->SetTitleOffset(1.11);

  frame->GetXaxis()->SetTitleFont(63);
  frame->GetXaxis()->SetTitleSize(25);
  frame->GetXaxis()->SetLabelFont(63);
  frame->GetXaxis()->SetLabelSize(22);
  
  frame->SetTitle(";m_{T}-m_{0} (GeV/c^{2});#pi^{+}/#pi^{-}");

  //Create the Graph Title
  TPaveText *title = new TPaveText(.13,.8,.56,.92,"NDC");
  title->SetFillColor(kWhite);
  title->SetBorderSize(0);
  title->SetTextFont(63);
  title->SetTextSize(35);
  title->SetTextAlign(12);
  title->AddText("Pion Ratio at Midrapidity");
  title->AddText("#scale[.8]{AuAu Top 5% Centrality}");
  title->Draw("SAME");

  //Create the Legend
  TLegend *leg = new TLegend(.6,.13,.92,.30);
  leg->SetFillColor(kWhite);
  leg->SetBorderSize(0);
  leg->SetTextFont(63);
  leg->SetTextSize(25);
  leg->SetNColumns(4);
  leg->SetHeader("#sqrt{s_{NN}} (GeV)");

  //*******************************************************
  //Coulomb Potential and Initial Ratio Canvas
  TCanvas *coulombCanvas = new TCanvas("coulombCanvas","coulombCanvas",900,20,600,1100);
  CanvasPartition(coulombCanvas,1,2,0.12,0.011,0.05,0.02);
  //coulombCanvas->Divide(1,2);
  
  gPad = (TPad *)gROOT->FindObject("pad_0");
  //coulombCanvas->cd(1);
  gPad->SetTicks();
  gPad->SetLogx();
  gPad->SetFillColor(0);
  gPad->SetFrameFillStyle(0);
  TH1F *frame1 = gPad->DrawFrame(1.01,.38,200,1.2);
  frame1->SetTitle(";#sqrt{s_{NN}} (GeV);R_{i}");
  frame1->GetYaxis()->SetTitleFont(63);
  frame1->GetYaxis()->SetTitleSize(30);
  frame1->GetYaxis()->SetTitleOffset(2.05);
  frame1->GetYaxis()->SetLabelFont(63);
  frame1->GetYaxis()->SetLabelSize(20);

  TPaveText *title1 = new TPaveText(.15,.8,.70,.93,"NDC");
  title1->SetFillColor(kWhite);
  title1->SetBorderSize(0);
  title1->SetTextFont(63);
  title1->SetTextSize(35);
  title1->SetTextAlign(12);
  title1->AddText("Initial Pion Ratio");
  title1->AddText("#scale[.8]{Most Central | Midrapidity}");
  title1->Draw("SAME");
  
  gPad = (TPad *)gROOT->FindObject("pad_1");
  //coulombCanvas->cd(2);
  gPad->SetTicks();
  gPad->SetLogx();
  gPad->SetFillColor(0);
  gPad->SetFrameFillStyle(0);
  gPad->SetBottomMargin(.12);
  TH1F *frame2 = gPad->DrawFrame(1.01,0,200,33);
  frame2->SetTitle(";#sqrt{s_{NN}} (GeV);V_{C} (MeV)");
  frame2->GetYaxis()->SetTitleFont(63);
  frame2->GetYaxis()->SetTitleSize(30);
  frame2->GetYaxis()->SetTitleOffset(2.05);
  frame2->GetYaxis()->SetLabelFont(63);
  frame2->GetYaxis()->SetLabelSize(20);

  frame2->GetXaxis()->SetTitleFont(63);
  frame2->GetXaxis()->SetTitleSize(30);
  frame2->GetXaxis()->SetTitleOffset(1.9);
  frame2->GetXaxis()->SetLabelFont(63);
  frame2->GetXaxis()->SetLabelSize(20);

  TPaveText *title2 = new TPaveText(.40,.83,.95,.96,"NDC");
  title2->SetFillColor(kWhite);
  title2->SetBorderSize(0);
  title2->SetTextFont(63);
  title2->SetTextSize(35);
  title2->SetTextAlign(12);
  title2->AddText("Coulomb Potential");
  title2->AddText("#scale[.8]{Most Central | Midrapidity}");
  title2->Draw("SAME");

  //***************************************************
  //****************************************************
  //WORLD DATA - Obtained from Daniel Cebra (June 2017)

  //Coulomb Potentials
  TGraphErrors *kaosVc = new TGraphErrors();
  kaosVc->SetMarkerStyle(33);
  kaosVc->SetMarkerSize(2.6);
  kaosVc->SetMarkerColor(kGray+1);
  kaosVc->SetPoint(kaosVc->GetN(),        2.33,  28.8);
  kaosVc->SetPointError(kaosVc->GetN()-1, 0,     0.7);

  TGraphErrors *e895Vc = new TGraphErrors();
  e895Vc->SetMarkerStyle(kFullTriangleUp);
  e895Vc->SetMarkerSize(2.0);
  e895Vc->SetMarkerColor(kGray+1);
  e895Vc->SetPoint(e895Vc->GetN(),        2.6,  27.1);
  e895Vc->SetPointError(e895Vc->GetN()-1, 0,     .6);
  e895Vc->SetPoint(e895Vc->GetN(),        3.3,  21.6);
  e895Vc->SetPointError(e895Vc->GetN()-1, 0,     0.5);
  e895Vc->SetPoint(e895Vc->GetN(),        3.8,  18.6);
  e895Vc->SetPointError(e895Vc->GetN()-1, 0,     0.4);
  e895Vc->SetPoint(e895Vc->GetN(),        4.5,   17.0);
  e895Vc->SetPointError(e895Vc->GetN()-1, 0,     0.5);

  TGraphErrors *na49Vc = new TGraphErrors();
  na49Vc->SetMarkerStyle(kFullSquare);
  na49Vc->SetMarkerSize(2.0);
  na49Vc->SetMarkerColor(kGray+1);
  na49Vc->SetPoint(na49Vc->GetN(),        6.4,  13.3);
  na49Vc->SetPointError(na49Vc->GetN()-1, 0,    0.9);
  na49Vc->SetPoint(na49Vc->GetN(),        7.7,  8.6);
  na49Vc->SetPointError(na49Vc->GetN()-1, 0,    1.1);

  TGraphErrors *na44Vc = new TGraphErrors();
  na44Vc->SetMarkerStyle(kFullCircle);
  na44Vc->SetMarkerSize(2.0);
  na44Vc->SetMarkerColor(kGray+1);
  na44Vc->SetPoint(na44Vc->GetN(),        17.3,  11.8);
  na44Vc->SetPointError(na44Vc->GetN()-1, 0,     0.7);

  TGraphErrors *wa98Vc = new TGraphErrors();
  wa98Vc->SetMarkerStyle(kFullCross);
  wa98Vc->SetMarkerSize(2.0);
  wa98Vc->SetMarkerColor(kGray+1);
  wa98Vc->SetPoint(wa98Vc->GetN(),        17.3,  8.4);
  wa98Vc->SetPointError(wa98Vc->GetN()-1, 0,     0.5);

  //Initial Pion Ratios
  TGraphErrors *kaosRi = new TGraphErrors();
  kaosRi->SetMarkerStyle(33);
  kaosRi->SetMarkerSize(2.6);
  kaosRi->SetMarkerColor(kGray+1);
  kaosRi->SetPoint(kaosRi->GetN(),        2.33,  0.460);
  kaosRi->SetPointError(kaosRi->GetN()-1, 0,     0.005);

  TGraphErrors *e895Ri = new TGraphErrors();
  e895Ri->SetMarkerStyle(kFullTriangleUp);
  e895Ri->SetMarkerSize(2.0);
  e895Ri->SetMarkerColor(kGray+1);
  e895Ri->SetPoint(e895Ri->GetN(),        2.6,  0.501);
  e895Ri->SetPointError(e895Ri->GetN()-1, 0,    0.006);
  e895Ri->SetPoint(e895Ri->GetN(),        3.3,  0.637);
  e895Ri->SetPointError(e895Ri->GetN()-1, 0,    0.004);
  e895Ri->SetPoint(e895Ri->GetN(),        3.8,  0.690);
  e895Ri->SetPointError(e895Ri->GetN()-1, 0,    0.003);
  e895Ri->SetPoint(e895Ri->GetN(),        4.5,  0.711);
  e895Ri->SetPointError(e895Ri->GetN()-1, 0,    0.004);

  TGraphErrors *na49Ri = new TGraphErrors();
  na49Ri->SetMarkerStyle(kFullSquare);
  na49Ri->SetMarkerSize(2.0);
  na49Ri->SetMarkerColor(kGray+1);
  na49Ri->SetPoint(na49Ri->GetN(),        6.4,  0.837);
  na49Ri->SetPointError(na49Ri->GetN()-1, 0,    0.008);
  na49Ri->SetPoint(na49Ri->GetN(),        7.7,  0.872);
  na49Ri->SetPointError(na49Ri->GetN()-1, 0,    0.009);

  TGraphErrors *na44Ri = new TGraphErrors();
  na44Ri->SetMarkerStyle(kFullCircle);
  na44Ri->SetMarkerSize(2.0);
  na44Ri->SetMarkerColor(kGray+1);
  na44Ri->SetPoint(na44Ri->GetN(),        17.3,  0.932);
  na44Ri->SetPointError(na44Ri->GetN()-1, 0,     0.003);

  TGraphErrors *wa98Ri = new TGraphErrors();
  wa98Ri->SetMarkerStyle(kFullCross);
  wa98Ri->SetMarkerSize(2.0);
  wa98Ri->SetMarkerColor(kGray+1);
  wa98Ri->SetPoint(wa98Ri->GetN(),        17.3,  0.940);
  wa98Ri->SetPointError(wa98Ri->GetN()-1, 0,     0.005);
  
  //******END WORLD DATA*********************************
  
  //*******************************************************
  //DATA FROM THIS ANALYSIS
  //Load the Files, Draw and Fit the Ratios
  canvas->cd();
  TFile *file[nEnergies];
  TGraphErrors *ratios[nEnergies];
  TF1 *coulombFits[nEnergies];
  TGraphErrors *coulombPotentialGraph = new TGraphErrors();
  TGraphErrors *initialRatioGraph = new TGraphErrors();
  for (int iFile=0; iFile<nEnergies; iFile++){
    file[iFile] = new TFile(Form("../../userfiles/%s%02d/outputFiles/%s%02d_%s_Spectra.root",
				 system.Data(),(int)GetEnergy(iFile),
				 system.Data(),(int)GetEnergy(iFile),eventConfig.Data()),"READ");
    if (!file[iFile])
      continue;
    
    ratios[iFile] = (TGraphErrors)file[iFile]->Get("SpectraRatio_Pion/rawSpectraRatio_Pion_Cent08_yIndex20");
    ratios[iFile]->SetMarkerStyle(kFullCircle);
    ratios[iFile]->SetMarkerColor(GetEnergyColor(GetEnergy(iFile)));
    ratios[iFile]->SetMarkerSize(1.2);

    ratios[iFile]->Draw("PZ");


    //Get the Pion and Proton Slopes from the Nominal Fits
    double pionSlope(-1);
    double protonSlope(-1);
    double midRapidity(0.0);

    TF1 *pionPlusFit = (TF1 *)file[iFile]->Get(Form("FitSpectraClass_PionPlus_FitFuncs/CorrectedSpectra_PionPlus_Cent08_yIndex20_Total_Fit0"));
    TF1 *pionMinusFit = (TF1 *)file[iFile]->Get(Form("FitSpectraClass_PionMinus_FitFuncs/CorrectedSpectra_PionMinus_Cent08_yIndex20_Total_Fit0"));
    TF1 *protonPlusFit = (TF1 *)file[iFile]->Get(Form("FitSpectraClass_ProtonPlus_FitFuncs/CorrectedSpectra_ProtonPlus_Cent08_yIndex20_Total_Fit0"));

    if (pionPlusFit && pionMinusFit)
      pionSlope = (pionPlusFit->GetParameter(1) + pionMinusFit->GetParameter(1)) / 2.0;
    if (protonPlusFit)
      protonSlope = protonPlusFit->GetParameter(1);

    cout <<pionSlope <<" " <<protonSlope <<endl;
    if (pionSlope < 0 || protonSlope < 0)
      continue;
    
    //Create the Fit Function, Seed Parameters
    coulombFits[iFile] = new TF1(Form("CoulombFit_%d",(int)GetEnergy(iFile)),
				 PionRatioFit,0,.8,5);
    coulombFits[iFile]->SetNpx(1000);
    coulombFits[iFile]->SetLineColor(GetEnergyColor(GetEnergy(iFile)));
    coulombFits[iFile]->SetParameter(0,1); //Initial Pion Ratio
    coulombFits[iFile]->SetParameter(1,.25); //Coulomb Potential
    coulombFits[iFile]->FixParameter(2,pionSlope); //Pion Temp
    coulombFits[iFile]->FixParameter(3,protonSlope); //Proton Temp
    coulombFits[iFile]->FixParameter(4,midRapidity); //MidRapidity

    //Fit the Ratio
    ratios[iFile]->Fit(coulombFits[iFile],"REX0");

    coulombPotentialGraph->SetPoint(coulombPotentialGraph->GetN(),
				    GetEnergy(iFile),
				    coulombFits[iFile]->GetParameter(1));
    coulombPotentialGraph->SetPointError(coulombPotentialGraph->GetN()-1,
					 0,
					 coulombFits[iFile]->GetParError(1));
    initialRatioGraph->SetPoint(initialRatioGraph->GetN(),
				GetEnergy(iFile),
				coulombFits[iFile]->GetParameter(0));
    initialRatioGraph->SetPointError(initialRatioGraph->GetN()-1,
				     0,
				     coulombFits[iFile]->GetParError(0));

    cout <<coulombFits[iFile]->GetNDF() <<endl;
    
    leg->AddEntry(ratios[iFile],Form("#scale[.8]{%.01f}",GetEnergy(iFile)),"P");
    //leg->AddEntry((TObject *)NULL,Form("#scale[.8]{%.03g}",
    //				   coulombFits[iFile]->GetChisquare()/(float)coulombFits[iFile]->GetNDF()),"");
    
  }

  leg->Draw("SAME");


  //*********************************************************
  //Draw the Coulomb Potential and initial Ratios
  coulombCanvas->cd();

  coulombPotentialGraph->SetMarkerStyle(kFullStar);
  coulombPotentialGraph->SetMarkerColor(kRed);
  coulombPotentialGraph->SetMarkerSize(3.0);

  initialRatioGraph->SetMarkerStyle(kFullStar);
  initialRatioGraph->SetMarkerColor(kRed);
  initialRatioGraph->SetMarkerSize(3.0);
  
  gPad = (TPad *)gROOT->FindObject("pad_0");

  //Draw World Vc Data
  kaosRi->Draw("PZ");
  e895Ri->Draw("PZ");
  na49Ri->Draw("PZ");
  na44Ri->Draw("PZ");
  wa98Ri->Draw("PZ");

  //Draw Data from this Analysis
  initialRatioGraph->Draw("PZ");

  TLegend *leg1 = new TLegend(.65,.05,.95,.43);
  leg1->SetFillColor(kWhite);
  leg1->SetBorderSize(0);
  leg1->SetTextFont(63);
  leg1->SetTextSize(25);
  leg1->SetTextAlign(12);
  leg1->AddEntry(kaosRi,"KaoS AuAu","P");
  leg1->AddEntry(e895Ri,"E895 AuAu","P");
  leg1->AddEntry(na49Ri,"NA49 PbPb","P");
  leg1->AddEntry(na44Ri,"NA44 PbPb","P");
  leg1->AddEntry(wa98Ri,"WA98 PbPb","P");
  leg1->AddEntry(initialRatioGraph,"STAR AuAu","P");
  leg1->Draw("SAME");
  
  gPad = (TPad *)gROOT->FindObject("pad_1");

  //Draw World Vc Data
  kaosVc->Draw("PZ");
  e895Vc->Draw("PZ");
  na49Vc->Draw("PZ");
  na44Vc->Draw("PZ");
  wa98Vc->Draw("PZ");
  
  //Draw Data from this Analysis
  coulombPotentialGraph->Draw("PZ");

  canvas->Update();
  coulombCanvas->Update();

  if (save){
    canvas->SaveAs("PionRatioFits.pdf");
    coulombCanvas->SaveAs("CoulombAndRatio.pdf");
  }

}
