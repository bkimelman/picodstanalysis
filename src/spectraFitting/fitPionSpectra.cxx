#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TAxis.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <TLine.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraClass.h"
#include "SpectraFitUtilities.h"
#include "SpectraFitFunctions.h"
#include "SimFitter.h"
#include "StyleSettings.h"

bool draw = false;
bool save = true;

//___________________________________________________________________
void fitPionSpectra(TString spectraFileName, Int_t charge, Int_t userCentBin = -999, Double_t userRapidity = -999){

  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const unsigned int nCentBins(GetNCentralityBins());
  const int pid = PION;

  //Set the Charge Vector
  std::vector<int> charges;
  charges.push_back(charge);

  //Set the Pid Vector
  std::vector<int> particles;
  particles.push_back(pid);
  
  const unsigned int nCharges(charges.size());
  const unsigned int nParticles(particles.size());
  
  //Open the Spectra File
  TFile *spectraFile = new TFile(spectraFileName,save ? "UPDATE" : "READ");
  TString saveDir = TString::Format("FitSpectraClass_%s",particleInfo->GetParticleName(pid,charge).Data());
  TString saveDirFit = TString::Format("%s_FitFuncs",saveDir.Data());

  //Create a canvas if drawing
  TCanvas *canvas = NULL;
  TH1F *spectraFrame = NULL;
  TCanvas *dNdyCanvas = NULL;
  TCanvas *chi2Canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas->SetTopMargin(.05);
    canvas->SetRightMargin(.05);
    gPad->SetLogy();
   
    dNdyCanvas = new TCanvas("parCanvas","parCanvas",20,700,500*nParticles,1000);
    dNdyCanvas->Divide(nParticles,2);
    dNdyCanvas->cd(1);
    gPad->DrawFrame(-1.5,0,1.5,250);
    dNdyCanvas->cd(2);
    gPad->DrawFrame(-1.5,0,1.5,.5);

    chi2Canvas = new TCanvas("chi2Canvas","chi2Canvas",20,20,800,600);
    gPad->DrawFrame(-1.5,0,1.5,35);
  }

  //Create the Fit Function that will be copied to each spectrum's individual fit
  TF1 *nominalFit = new TF1("nominalFit",BoseEinsteinFitFunc,0,2.0,3);
  TF1 *sysFit = new TF1("sysFit",pTExponentialFitFunc,0,2.0,3);
  nominalFit->SetNpx(1000);
  sysFit->SetNpx(1000);

  //Parameter Enumerations for the Simultaneous Fit
  int nNonAmpPars(2);
  enum simFitPars {TEMP,PMASS,AMP};

  //Create the dNdy Graphs and Chi2 Graphs
  std::vector<std::vector<TGraphErrors *> >dNdyGraphs
    (nCentBins, std::vector<TGraphErrors *>
     (nParticles,(TGraphErrors *)NULL));
  std::vector<std::vector<TGraphErrors *> >chi2Graphs
    (nCentBins, std::vector<TGraphErrors *>
     (2,(TGraphErrors *) NULL)); //Nominal / Systematic
  std::vector<std::vector<std::vector< TGraphErrors *> > > parameterGraphs
    (nCentBins, std::vector<std::vector<TGraphErrors *> >
     (nParticles, std::vector<TGraphErrors *>
      (PMASS, (TGraphErrors *) NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > parameterFits
    (nCentBins, std::vector<std::vector<TF1 *> >
     (nParticles, std::vector<TF1 *>
      (PMASS, (TF1 *) NULL)));
  for (unsigned int iCentBin = 0; iCentBin<nCentBins; iCentBin++){
    chi2Graphs.at(iCentBin).at(0) = new TGraphErrors();
    chi2Graphs.at(iCentBin).at(0)->SetName(Form("Chi2Graph_%s_Cent%02d",
					  particleInfo->GetParticleName(pid,charge).Data(),
					  iCentBin));
    chi2Graphs.at(iCentBin).at(0)->SetMarkerStyle(kFullCircle);
    chi2Graphs.at(iCentBin).at(0)->SetMarkerColor(GetCentralityColor(iCentBin));

    chi2Graphs.at(iCentBin).at(1) = new TGraphErrors();
    chi2Graphs.at(iCentBin).at(1)->SetName(Form("Chi2Graph_%s_Cent%02d_Sys",
					  particleInfo->GetParticleName(pid,charge).Data(),
					  iCentBin));
    chi2Graphs.at(iCentBin).at(1)->SetMarkerStyle(kOpenCircle);
    chi2Graphs.at(iCentBin).at(1)->SetMarkerColor(GetCentralityColor(iCentBin));
    
    for (unsigned int i=0; i<nParticles; i++){
      dNdyGraphs.at(iCentBin).at(i) = new TGraphErrors();
      dNdyGraphs.at(iCentBin).at(i)->SetMarkerStyle(kFullCircle);
      dNdyGraphs.at(iCentBin).at(i)->SetMarkerColor(GetCentralityColor(iCentBin));
      for (unsigned int j=0; j<parameterGraphs.at(iCentBin).at(i).size(); j++){
	parameterGraphs.at(iCentBin).at(i).at(j) = new TGraphErrors();
	parameterGraphs.at(iCentBin).at(i).at(j)->SetName(Form("FitPar%d_%s_Cent%02d",
							       j,
							       particleInfo->GetParticleName(pid,charge).Data(),
							       iCentBin));
	parameterGraphs.at(iCentBin).at(i).at(j)->SetMarkerStyle(kFullCircle);
	parameterGraphs.at(iCentBin).at(i).at(j)->SetMarkerColor(GetCentralityColor(iCentBin));

	parameterFits.at(iCentBin).at(i).at(j) = new TF1(Form("%s_Fit",
							       parameterGraphs.at(iCentBin).at(i).at(j)->GetName()),
							  "gaus(0)",-1.5,1.5);
	parameterFits.at(iCentBin).at(i).at(j)->SetParameter(0,.2);
	parameterFits.at(iCentBin).at(i).at(j)->FixParameter(1,0);
	parameterFits.at(iCentBin).at(i).at(j)->SetParameter(2,5);

	parameterFits.at(iCentBin).at(i).at(j)->SetParLimits(0,.08,.3);
	parameterFits.at(iCentBin).at(i).at(j)->SetParLimits(2,2,5);
	
      }
    }
  }

  //Load the Spectra
  //The most internal vector holds all the spectra of equal |y|
  std::vector<std::vector<std::vector<SpectraClass *> > > spectra
    (nCentBins,std::vector<std::vector<SpectraClass *> >
     (nRapidityBins, std::vector<SpectraClass *>
      (0,(SpectraClass *)NULL)));
  std::vector<std::vector<std::vector<TGraphErrors *> > > spectraToFit
    (nCentBins,std::vector<std::vector<TGraphErrors *> >
     (nRapidityBins, std::vector<TGraphErrors *>
      (0,(TGraphErrors *)NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > fitFuncs 
    (nCentBins,std::vector<std::vector<TF1 *> >
     (nRapidityBins, std::vector<TF1 *>
      (0,(TF1 *)NULL)));
  std::vector<std::vector<std::vector<TF1 *> > > sysFuncs 
    (nCentBins,std::vector<std::vector<TF1 *> >
     (nRapidityBins, std::vector<TF1 *>
      (0,(TF1 *)NULL)));
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int yIndex=GetMinRapidityIndexOfInterest(pid); yIndex<=nRapidityBins/2; yIndex++){
      
      std::vector<int> yIndexVec;
      yIndexVec.push_back(yIndex);
      if ((int)yIndex != GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)))
	yIndexVec.push_back(GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)));
      
      
      for (unsigned int iY=0; iY<yIndexVec.size(); iY++){
	for (unsigned int iCharge=0; iCharge<nCharges; iCharge++){
	  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

	    TString spectraName = TString::Format("CorrectedSpectra_%s_Cent%02d_yIndex%02d_Total",
						  particleInfo->GetParticleName(pid,charge).Data(),
						  iCentBin,yIndexVec.at(iY));
	    TString spectraClassDir = TString::Format("SpectraClass_%s",
						      particleInfo->GetParticleName(pid,charge).Data());
	    TString spectrumDir = TString::Format("CorrectedSpectra_%s",
						  particleInfo->GetParticleName(pid,charge).Data());
	    TString spectrumFitDir = TString::Format("%s_FitFuncs",
						     spectrumDir.Data());
	    TString dNdyDir = TString::Format("dNdyGraph_%s",
					      particleInfo->GetParticleName(pid,charge).Data());
	    
	    spectraFile->cd(spectraClassDir.Data());
	    SpectraClass *temp = (SpectraClass *)gDirectory->Get(spectraName.Data());
	    
	    if (temp){

	      spectra.at(iCentBin).at(yIndex).push_back(temp);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraFile(spectraFile);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraClassDir(spectraClassDir);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraDir(spectrumDir);
	      spectra.at(iCentBin).at(yIndex).back()->SetSpectraNamePrefix("correctedSpectra");
	      spectra.at(iCentBin).at(yIndex).back()->LoadSpectra();
	      
	      spectraToFit.at(iCentBin).at(yIndex).push_back(spectra.at(iCentBin).at(yIndex).back()->GetTGraph());

	      fitFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*nominalFit));
	      sysFuncs.at(iCentBin).at(yIndex).push_back(new TF1(*sysFit));
	      sysFuncs.at(iCentBin).at(yIndex).back()->SetLineStyle(9);

	      fitFuncs.at(iCentBin).at(yIndex).back()->SetName(Form("%s_Fit0",spectraName.Data()));
	      sysFuncs.at(iCentBin).at(yIndex).back()->SetName(Form("%s_Fit1",spectraName.Data()));
	    }//End If Spectra Exists
	    else{
              std::cout << "Warning in FitSpectraPions: Spectra Not Found: " << spectraName << std::endl;
            } 
	  }//End Loop Over Particles
	}//End Loop Over Charges
      }//End Loop Over Mirrored RapidityIndices
    }//End Loop Over yIndex
  }//End Loop Over CentIndex	  


  //NOW FOR THE FITS
  std::vector<double> prevFitParams;
  for (int iCentBin=nCentBins-1; iCentBin>=0; iCentBin--){

    //Skip Bins that are not requested by the user
    if (userCentBin != -999 && userCentBin >= 0 && (int)iCentBin != userCentBin)
      continue;

    //-----LOOP 1
    for (int yIndex=(nRapidityBins/2); yIndex>=0; yIndex--){

      //Skip Bins that are not requested by the user
      if (userRapidity != -999 && userRapidity != GetRapidityRangeCenter(yIndex))
	continue;

      //Get the Rapidity of This bin
      Double_t rapidity = fabs(GetRapidityRangeCenter(yIndex));
      
      //Skip this bin if there are no spectra
      const unsigned int nSpectra = spectraToFit.at(iCentBin).at(yIndex).size();
      if (nSpectra == 0)
	continue;

      //Skip this bin if none of the spectra have enough points
      bool skipDueToLackOfPoints = false;
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	if (spectraToFit.at(iCentBin).at(yIndex).at(iSpectrum)->GetN() < 5)
	  skipDueToLackOfPoints = true;
	else{
	  skipDueToLackOfPoints = false;
	  break;
	}	  
      }
      if (skipDueToLackOfPoints){
	cout <<"Skipping bin since no spectrum has enough points" <<endl;
	continue;
      }

      //Compute the Total Number of Parameters in the Sim Fit
      const int nTotalPars = nNonAmpPars + nSpectra;

      //Define the Parameter Relationships
      std::vector<std::vector<int> > parRelations(nSpectra,std::vector<int> (0,-1));
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){

	parRelations.at(iSpectrum).resize(fitFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetNpar(),0);
	parRelations.at(iSpectrum).at(0) = AMP+iSpectrum;
	parRelations.at(iSpectrum).at(1) = TEMP;
	parRelations.at(iSpectrum).at(2) = PMASS;
      }

      //Seed the Parameters of the Total Fit
      std::vector<double> totalFitPars(nTotalPars);
      if (prevFitParams.size() > 0 && prevFitParams.size() == totalFitPars.size()){
	for (unsigned int iPar=0; iPar<totalFitPars.size(); iPar++){
	  totalFitPars.at(iPar) = prevFitParams.at(iPar);
	}
      }
      else {
	totalFitPars.at(TEMP) = .12;
	totalFitPars.at(PMASS) = particleInfo->GetParticleMass(pid);
	for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	  totalFitPars.at(AMP+iSpectrum) = 10000;
	}
      }

      //*************************
      //***** NOMINAL FIT *******
      //*************************
      //Create the Sim Fitter and Configure the Parameters
      SimFitter simFitter(spectraToFit.at(iCentBin).at(yIndex),fitFuncs.at(iCentBin).at(yIndex));
      simFitter.Init(totalFitPars);
      simFitter.SetPrintLevel(1);
      simFitter.SetParameterRelationships(parRelations);

      //Fix the Mass
      simFitter.FixParameter(PMASS);

      //Set Parameter Limits
      simFitter.SetParLimits(TEMP,.08,.4);
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	simFitter.SetParLimits(AMP+iSpectrum,100,10000);
      }

      //Do the Fit
      ROOT::Fit::FitResult fitResults = simFitter.Fit();
      int iters(0);
      while (fitResults.Status() != 0 && iters < 10){
	simFitter.Fit();
	iters++;
      }
      
      if (fitResults.Status() != 0){
	cout <<"FIT FAILED!" <<endl;
	continue;
      }
      
      std::vector<TF1 *> fitResultFuncs = simFitter.GetFitFunctions();
      
      //Transfer the the fit parameters to the previous vector and into the parameter graphs
      if (prevFitParams.size() > 0)
	prevFitParams.clear();
      prevFitParams.resize(nTotalPars);
      for (unsigned int iPar=0; iPar<prevFitParams.size(); iPar++){
	prevFitParams.at(iPar) = fitResults.Parameter(iPar);
      }
      for (unsigned int iPar=TEMP; iPar<PMASS; iPar++){
	parameterGraphs.at(iCentBin).at(0).at(iPar)->
	  SetPoint(parameterGraphs.at(iCentBin).at(0).at(iPar)->GetN(),
		   rapidity,fitResults.Parameter(iPar));
	parameterGraphs.at(iCentBin).at(0).at(iPar)->
	  SetPointError(parameterGraphs.at(iCentBin).at(0).at(iPar)->GetN()-1,
			rapidityBinWidth/2.0,fitResults.ParError(iPar));
      }
      
    }//End Loop 1 over rapidity bins

    //Fit the Parameters
    for (unsigned int iPar=TEMP; iPar<PMASS; iPar++){
      bool isGoodFit(false);
      int nIters(0);
      while (!isGoodFit && nIters < 10){
	int status = (int)parameterGraphs.at(iCentBin).at(0).at(iPar)->
	  Fit(parameterFits.at(iCentBin).at(0).at(iPar));

	double chi2NDF = parameterFits.at(iCentBin).at(0).at(iPar)->GetChisquare() /
	  (double) parameterFits.at(iCentBin).at(0).at(iPar)->GetNDF();
	
	if (status == 0 && chi2NDF < 3.0)
	  isGoodFit = true;

	nIters++;
      }
    }
    

    //-----LOOP 2
    for (int yIndex=(nRapidityBins/2); yIndex>=0; yIndex--){

      //Skip Bins that are not requested by the user
      if (userRapidity != -999 && userRapidity != GetRapidityRangeCenter(yIndex))
	continue;

      //Get the Rapidity of This bin
      Double_t rapidity = fabs(GetRapidityRangeCenter(yIndex));
      
      //Skip this bin if there are no spectra
      const unsigned int nSpectra = spectraToFit.at(iCentBin).at(yIndex).size();
      if (nSpectra == 0)
	continue;

      //Skip this bin if none of the spectra have enough points
      bool skipDueToLackOfPoints = false;
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	if (spectraToFit.at(iCentBin).at(yIndex).at(iSpectrum)->GetN() < 5)
	  skipDueToLackOfPoints = true;
	else{
	  skipDueToLackOfPoints = false;
	  break;
	}	  
      }
      if (skipDueToLackOfPoints){
	cout <<"Skipping bin since no spectrum has enough points" <<endl;
	continue;
      }

      //Compute the Total Number of Parameters in the Sim Fit
      const int nTotalPars = nNonAmpPars + nSpectra;

      //Define the Parameter Relationships
      std::vector<std::vector<int> > parRelations(nSpectra,std::vector<int> (0,-1));
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){

	parRelations.at(iSpectrum).resize(fitFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetNpar(),0);
	parRelations.at(iSpectrum).at(0) = AMP+iSpectrum;
	parRelations.at(iSpectrum).at(1) = TEMP;
	parRelations.at(iSpectrum).at(2) = PMASS;
      }

      //Seed the Parameters of the Total Fit
      std::vector<double> totalFitPars(nTotalPars);
      if (prevFitParams.size() > 0 && prevFitParams.size() == totalFitPars.size()){
	for (unsigned int iPar=0; iPar<totalFitPars.size(); iPar++){
	  totalFitPars.at(iPar) = prevFitParams.at(iPar);
	}
      }
      else {
	totalFitPars.at(TEMP) = .12;
	totalFitPars.at(PMASS) = particleInfo->GetParticleMass(pid);
	for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	  totalFitPars.at(AMP+iSpectrum) = 10000;
	}
      }

      //Since this is the second round of fitting we override the value of the temp
      //with the value from the parameterization
      totalFitPars.at(TEMP) = parameterFits.at(iCentBin).at(0).at(TEMP)->Eval(rapidity);

      //*************************
      //***** NOMINAL FIT *******
      //*************************
      //Create the Sim Fitter and Configure the Parameters
      SimFitter simFitter(spectraToFit.at(iCentBin).at(yIndex),fitFuncs.at(iCentBin).at(yIndex));
      simFitter.Init(totalFitPars);
      simFitter.SetPrintLevel(1);
      simFitter.SetParameterRelationships(parRelations);

      //Fix the Mass
      simFitter.FixParameter(PMASS);

      //Fix the Temp since this is the second round of fitting
      simFitter.FixParameter(TEMP);
      
      //Set Parameter Limits
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	simFitter.SetParLimits(AMP+iSpectrum,100,10000);
      }

      //Do the Fit
      ROOT::Fit::FitResult fitResults = simFitter.Fit();
      std::vector<TF1 *> fitResultFuncs = simFitter.GetFitFunctions();

      //Add the Fit to the Spectrum
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	fitResultFuncs.at(iSpectrum)->SetName(fitFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetName());
	fitResultFuncs.at(iSpectrum)->SetChisquare(fitResults.Chi2());
	fitResultFuncs.at(iSpectrum)->SetNDF(fitResults.Ndf());
	spectra.at(iCentBin).at(yIndex).at(iSpectrum)->AddSpectraFit(fitResultFuncs.at(iSpectrum),
								     simFitter.GetCovarianceMatrix(iSpectrum),0);
      }

      //Add the Chi2 of the fit to the graph
      chi2Graphs.at(iCentBin).at(0)->SetPoint(chi2Graphs.at(iCentBin).at(0)->GetN(),
					      rapidity,fitResults.Chi2()/(float)fitResults.Ndf());
      
      //Move the dNdy To the Graphs
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	int index(0);
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPoint(dNdyGraphs.at(iCentBin).at(index)->GetN(),
		   GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetRapidityIndex()),
		   spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdy(0));
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPointError(dNdyGraphs.at(iCentBin).at(index)->GetN()-1,
			rapidityBinWidth/2.0,
			spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdyErr(0));
      }
      
      //***************************
      //****** SYSTEMATIC FIT *****
      //***************************
      //Create the Sim Fitter and Configure the Parameters
      SimFitter sysFitter(spectraToFit.at(iCentBin).at(yIndex),sysFuncs.at(iCentBin).at(yIndex));
      sysFitter.Init(totalFitPars);
      sysFitter.SetPrintLevel(1);
      sysFitter.SetParameterRelationships(parRelations);

      //Fix the Mass
      sysFitter.FixParameter(PMASS);

      //Set Parameter Limits
      sysFitter.SetParLimits(TEMP,.08,.4);
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	sysFitter.SetParLimits(AMP+iSpectrum,100,10000);
      }

      //Do the Fit
      cout <<"SYSTEMATIC FIT" <<endl;
      ROOT::Fit::FitResult fitResultsSys = sysFitter.Fit();
      std::vector<TF1 *> fitResultFuncsSys = sysFitter.GetFitFunctions();

      //Add the Fit to the Spectrum
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	fitResultFuncsSys.at(iSpectrum)->SetName(sysFuncs.at(iCentBin).at(yIndex).at(iSpectrum)->GetName());
	fitResultFuncsSys.at(iSpectrum)->SetChisquare(fitResultsSys.Chi2());
	fitResultFuncsSys.at(iSpectrum)->SetNDF(fitResultsSys.Ndf());
	spectra.at(iCentBin).at(yIndex).at(iSpectrum)->AddSpectraFit(fitResultFuncsSys.at(iSpectrum),
								     sysFitter.GetCovarianceMatrix(iSpectrum),1);
      }
      
      //Add the Chi2 of the fit to the graph
      chi2Graphs.at(iCentBin).at(1)->SetPoint(chi2Graphs.at(iCentBin).at(1)->GetN(),
					      rapidity,fitResultsSys.Chi2()/(float)fitResultsSys.Ndf());
      
      //Move the dNdy To the Graphs
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	int index(0);
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPoint(dNdyGraphs.at(iCentBin).at(index)->GetN(),
		   GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetRapidityIndex()),
		   spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdy(1));
	dNdyGraphs.at(iCentBin).at(index)->
	  SetPointError(dNdyGraphs.at(iCentBin).at(index)->GetN()-1,
			rapidityBinWidth/2.0,
			spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdyErr(1));
      }

      //************************
      //**** DRAW IF DESIRED ****
      //************************
      if (draw){
	canvas->cd();
	spectraFrame = canvas->DrawFrame(0.0,.0001,2,500);
	
	//Loop Over the Spectra Involved in the Simulatneous Fit
	for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){
	  canvas->cd();
	  spectraToFit.at(iCentBin).at(yIndex).at(iSpectrum)->Draw("PZ");
	  fitResultFuncs.at(iSpectrum)->Draw("SAME");
	  fitResultFuncsSys.at(iSpectrum)->Draw("SAME");

	  //Draw the dNdy Distributions
	  dNdyCanvas->cd(1);
	  dNdyGraphs.at(iCentBin).at(0)->Draw("PZ");
	  dNdyCanvas->cd(2);
	  parameterGraphs.at(iCentBin).at(0).at(TEMP)->Draw("PZ");

	  
	  chi2Canvas->cd();
	  chi2Graphs.at(iCentBin).at(0)->Draw("PZ");
	  chi2Graphs.at(iCentBin).at(1)->Draw("PZ");
	  
	}//End Loop Over Spectra
	
	canvas->Update();
	dNdyCanvas->Update();
	gSystem->Sleep(100);	

      }//End Draw

      
    }//End Loop Over Rapidity Bins
    
  }//End Loop Over Centrality Bins
  
  //************************
  //**** SAVE IF DESIRED ***
  //************************
  if (save){

    spectraFile->cd();
    spectraFile->mkdir(saveDir.Data());
    spectraFile->mkdir(saveDirFit.Data());
    spectraFile->mkdir(Form("%s_FitPars",saveDirFit.Data()));
     
    for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
	
	for (unsigned int i=0; i<spectra.at(iCentBin).at(yIndex).size(); i++){
	  
	  spectraFile->cd();
	  spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraFile(spectraFile);
	  spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraClassDir(saveDir);
	  spectra.at(iCentBin).at(yIndex).at(i)->SetSpectraFitDir(saveDirFit);

	  //Save the Spectra Class and Fit Funcs, but do not overwrite the spectra themselves.
	  spectra.at(iCentBin).at(yIndex).at(i)->WriteSpectrum(false,true);
	  
	}//End Loop Over Spectra

      }//End Loop Over Rapidity Bins
      
      spectraFile->cd();
      spectraFile->cd(Form("%s_FitPars",saveDirFit.Data()));
      chi2Graphs.at(iCentBin).at(0)->Write("",TObject::kOverwrite);
      chi2Graphs.at(iCentBin).at(1)->Write("",TObject::kOverwrite);
      for (unsigned int iPar=TEMP; iPar<PMASS; iPar++){
	parameterGraphs.at(iCentBin).at(0).at(iPar)->Write("",TObject::kOverwrite);
      }
      
    }//End Loop Over Centrality Bins      
  }//End SAVE
  
}
