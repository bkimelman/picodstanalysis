//Applies cuts to skim the data at the event, vertex, and track level. Then bins the
//tracks in centrality, rapidity, and transverse mass for various particle species.

#include <iostream>
#include <vector>
#include <utility>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>

#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDstReader.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDst.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoEvent.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoTrack.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoBTofPidTraits.h"

#include "globalDefinitions.h"
#include "TrackInfo.h"
#include "PrimaryVertexInfo.h"
#include "EventInfo.h"
#include "DavisDstReader.h"
#include "ParticleInfo.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"


//Changes: 21 March 2019
//Matt Harasty: The dEdx is no longer dEdx trucated like in StMuTrack PiDtraits
// Removed  track->GetTofMatchFlag() < 1  check (see comment below)

//___MAIN___________________________
void skimmerAndBinner(TString inputDataFile,TString starLibrary, 
		      Long64_t nEvents=-1, TString outputFile="",
		      Double_t energy=0, TString eventConfig=""){
  TFile* ftemp = new TFile(inputDataFile.Data(),"READ");
  if(ftemp->TestBit(TFile::kRecovered)){
    cout << "Warning: SKIPPING FILE : " << inputDataFile << "  : Due to some form of corruption..." << endl;
    return;
  }
  if(ftemp->IsZombie()){
    cout << "Warning: " << inputDataFile << " is a Zombie" << endl;
  }
  ftemp->Close();
  delete ftemp; 

  //Read the PicoDst
  StPicoDstReader* picoReader = new StPicoDstReader(inputDataFile);
  picoReader->Init();
  //if(trackCuts == false){
    picoReader->SetStatus("*",0);
    picoReader->SetStatus("Event",1);
    picoReader->SetStatus("Track",1);
  //}

  if( !picoReader->chain() ) {
    std::cout << "No chain has been found." << std::endl;
  }



  //Create Pointers needed for reading the tree
  //  TrackInfo *track = NULL;
  //  PrimaryVertexInfo *primaryVertex = NULL;
  //  EventInfo *event = NULL;

  //If no output file was specified then we won't produce one
  TFile *outFile = NULL;
  if (outputFile.CompareTo(""))
    outFile = new TFile(outputFile,"RECREATE");

  //Create an instance of the Particle Info Class
  ParticleInfo *particleInfo = new ParticleInfo(starLibrary,true,TRUNCATED);

  //************************************************************************
  //Temporary solution since nSigma there is not an nSigma 
  //value for deteruon,triton,helio,alpha,muon
  //  const int nParticles = 3;
  const int nParticles = 3;
  //************************************************************************

  //Get the centrality vector from the User's function
  const int nCentralityBins = GetNCentralityBins(); 
  //  const int nCentralityBins = 1; 
  
  //Declare Histogram to hold the binned Particles and the Number of Events

  //Contains the number of events in each centrality bin
  TH1D *nEventsHisto = new TH1D("nEvents","Events in Each Centrality Bin",
				nCentralityBins,0,nCentralityBins);
  std::vector<TH1D *> centVarHisto(nCentralityBins,(TH1D *)NULL);

  //The Yield Histograms for the difference centrality bins and particle species
  std::vector<std::vector<TH3D *> > ZTPCHistoPlus(nParticles,vector<TH3D *>(nCentralityBins,(TH3D *)NULL));
  std::vector<std::vector<TH3D *> > ZTOFHistoPlus(nParticles,vector<TH3D *>(nCentralityBins,(TH3D *)NULL));
  std::vector<std::vector<TH3D *> > ZTPCHistoMinus(nParticles,vector<TH3D *>(nCentralityBins,(TH3D *)NULL));
  std::vector<std::vector<TH3D *> > ZTOFHistoMinus(nParticles,vector<TH3D *>(nCentralityBins,(TH3D *)NULL));
  
  //Number of tracks seen in the TPC and TOF - Will be used for TOF Matching Efficiency
  std::vector<std::vector<TH2D *> > TPCTracksPlus(nParticles,vector<TH2D *>(nCentralityBins,(TH2D *)NULL));
  std::vector<std::vector<TH2D *> > TOFTracksPlus(nParticles,vector<TH2D *>(nCentralityBins,(TH2D *)NULL));
  std::vector<std::vector<TH2D *> > TPCTracksMinus(nParticles,vector<TH2D *>(nCentralityBins,(TH2D *)NULL));
  std::vector<std::vector<TH2D *> > TOFTracksMinus(nParticles,vector<TH2D *>(nCentralityBins,(TH2D *)NULL));
  
  //Loop Over the Particle Species and centrality bins and define the histograms
  for (Int_t iParticle=0; iParticle<nParticles; iParticle++){
    
    //Skip Electrons and Muons
    if (iParticle == ELECTRON || iParticle == MUON)
      continue;
    
    for (Int_t iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
      
      if (!centVarHisto.at(iCentBin)){
	      centVarHisto.at(iCentBin) = 
	        new TH1D(Form("centralityVariable_Cent%d",iCentBin),
		        Form("CentralityVariable Distribution in Centrality Bin %d",iCentBin),
		        1000,0,1000);
      }
      
      cout << "Defining Yield Histograms for particle: " << iParticle << " and centrality: " << iCentBin << endl;

      //Define Yield Histograms
      ZTPCHistoPlus.at(iParticle).at(iCentBin) = 
	new TH3D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,1000,-5,5);
      ZTOFHistoPlus.at(iParticle).at(iCentBin) = 
	new TH3D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,1000,-5,5);
      
      ZTPCHistoMinus.at(iParticle).at(iCentBin) =
        new TH3D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,1000,-5,5);
      ZTOFHistoMinus.at(iParticle).at(iCentBin) =
        new TH3D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max,1000,-5,5);

      TPCTracksPlus.at(iParticle).at(iCentBin) =
	      new TH2D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max);
      TOFTracksPlus.at(iParticle).at(iCentBin) = 
	      new TH2D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max);
      
      TPCTracksMinus.at(iParticle).at(iCentBin) =
	      new TH2D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max);
      TOFTracksMinus.at(iParticle).at(iCentBin) =
	new TH2D("","",nRapidityBins,rapidityMin,rapidityMax,nmTm0Bins,mTm0Min,mTm0Max);

      //Set the Names and Titles of the Yield Histograms
      const char *particleSymbolPlus = particleInfo->GetParticleSymbol(iParticle,1).Data();
      const char *particleSymbolMinus = particleInfo->GetParticleSymbol(iParticle,-1).Data();
      
      ZTPCHistoPlus.at(iParticle).at(iCentBin)->
	      SetName(Form("zTPC_%s_Cent%d",particleInfo->GetParticleName(iParticle,1).Data(),iCentBin));
      ZTPCHistoPlus.at(iParticle).at(iCentBin)->
        SetTitle(Form("Z_{TPC} of %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TPC,%s}",
		      particleSymbolPlus,iCentBin,particleSymbolPlus,
		      particleSymbolPlus,particleSymbolPlus));
      
      ZTOFHistoPlus.at(iParticle).at(iCentBin)->
        SetName(Form("zTOF_%s_Cent%d",particleInfo->GetParticleName(iParticle,1).Data(),iCentBin));
      ZTOFHistoPlus.at(iParticle).at(iCentBin)->
	      SetTitle(Form("Z_{TOF} of %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TOF,%s}",
		      particleSymbolPlus,iCentBin,particleSymbolPlus,
		      particleSymbolPlus,particleSymbolPlus));
      ZTPCHistoMinus.at(iParticle).at(iCentBin)->
        SetName(Form("zTPC_%s_Cent%d",particleInfo->GetParticleName(iParticle,-1).Data(),iCentBin));
      ZTPCHistoMinus.at(iParticle).at(iCentBin)->
	      SetTitle(Form("Z_{TPC} of %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TPC,%s}",
		      particleSymbolMinus,iCentBin,particleSymbolMinus,
		      particleSymbolMinus,particleSymbolMinus));
      
      ZTOFHistoMinus.at(iParticle).at(iCentBin)->
	      SetName(Form("zTOF_%s_Cent%d",particleInfo->GetParticleName(iParticle,-1).Data(),iCentBin));
      ZTOFHistoMinus.at(iParticle).at(iCentBin)->
	SetTitle(Form("Z_{TOF} of %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2});Z_{TOF,%s}",
		      particleSymbolMinus,iCentBin,particleSymbolMinus,
		      particleSymbolMinus,particleSymbolMinus));

      TPCTracksPlus.at(iParticle).at(iCentBin)->
	      SetName(Form("nTPCTracks_%s_Cent%d",particleInfo->GetParticleName(iParticle,1).Data(),iCentBin));
      TPCTracksPlus.at(iParticle).at(iCentBin)->
	      SetTitle(Form("n TPC Tracks for %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2})",
		      particleSymbolPlus,iCentBin,particleSymbolPlus,particleSymbolPlus));
      
      TOFTracksPlus.at(iParticle).at(iCentBin)->
	      SetName(Form("nTOFTracks_%s_Cent%d",particleInfo->GetParticleName(iParticle,1).Data(),iCentBin));
      TOFTracksPlus.at(iParticle).at(iCentBin)->
	SetTitle(Form("n TOF Tracks for %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2})",
		      particleSymbolPlus,iCentBin,particleSymbolPlus,particleSymbolPlus));

      TPCTracksMinus.at(iParticle).at(iCentBin)->
	      SetName(Form("nTPCTracks_%s_Cent%d",particleInfo->GetParticleName(iParticle,-1).Data(),iCentBin));
      TPCTracksMinus.at(iParticle).at(iCentBin)->
	SetTitle(Form("n TPC Tracks for %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2})",
		      particleSymbolMinus,iCentBin,particleSymbolMinus,particleSymbolMinus));

      TOFTracksMinus.at(iParticle).at(iCentBin)->
        SetName(Form("nTOFTracks_%s_Cent%d",particleInfo->GetParticleName(iParticle,-1).Data(),iCentBin));
      TOFTracksMinus.at(iParticle).at(iCentBin)->
        SetTitle(Form("n TOF Tracks for %s Centrality Bin: %d;y_{%s};m_{T}-m_{%s} (GeV/c^{2})",
                      particleSymbolMinus,iCentBin,particleSymbolMinus,particleSymbolMinus));
      
      
    }//End Loop Over Centrality Bins
    
  }//End Loop Over particle species

  //If the user has passed a number of events to process then use it,
  //otherwise use all the entries in the tree
  Long64_t eventsInTree = picoReader->tree()->GetEntries();
  std::cout << "eventsInTree: "  << eventsInTree << std::endl;
  Long64_t events2read = picoReader->chain()->GetEntries();
  if(nEvents < events2read && nEvents != -1) events2read = nEvents;
  std::cout << "events2read: " << events2read << endl;  

  //-------------------------------------------------------------------------------
  //SKIM DATA: Loop Over the tree to apply the event and vertex cuts. Track cuts
  //           are applied during the binning phase below.
  //-------------------------------------------------------------------------------
  std::vector<int> goodEntries;
  picoReader->SetStatus("Track",0);
  for (int iEntry=0; iEntry<(int)events2read; iEntry++){
    //std::cout << "reading event #" << iEntry << endl;
    Bool_t readEvent = picoReader->ReadPicoEvent(iEntry);
    if( !readEvent ) {
      std::cout << "Something went wrong, no events" << endl;
      break;
    }
    
    StPicoDst *dst = picoReader->picoDst();
    
    //access data and fill trigger level histograms                                                        
    StPicoEvent *event = dst->event();
    if( !event ) {
      cout << "Something went wrong, event is empty" << endl;
      break;
    }
    
    if (!IsGoodEvent(event)) continue;
    if (!IsGoodVertex(event)) continue;
    
    //If there was at least one good primary vertex then add this event to the 
    //vector of goodEntries
    goodEntries.push_back(iEntry);
    
  }//End Loop Over Events
  //END APPLY CUTS
  
  std::cout << "Number of Good Entries: " << goodEntries.size() << std::endl;

  //--------------------------------------------------------------------------------
  //BIN DATA: Loop Over the entries in the goodEntries vector. Bin the events in 
  //          centrality and the tracks in rapidity and transverse mass if they
  //          pass the track cuts.  
  //--------------------------------------------------------------------------------
  picoReader->Init();
  //if(trackCuts == false){
  picoReader->SetStatus("*",0);

  picoReader->SetStatus("Event",1);
  picoReader->SetStatus("Track",1);
  picoReader->SetStatus("BTofPidTraits",1);
  std::cout << "Number of Good Entries to Loop Over: " << goodEntries.size() << std::endl;
  for ( int goodIndex = 0; 
	goodIndex < (int) goodEntries.size(); goodIndex++){
    //cout << "On good Index: " << goodEntries[goodIndex] << endl;
    Bool_t readEvent = picoReader->ReadPicoEvent(goodEntries[goodIndex]);
    if( !readEvent ) {
      std::cout << "Something went wrong, no events" << endl;
      break;
    }
    
    StPicoDst *dst = picoReader->picoDst();
    
    //access data and fill trigger level histograms
    //cout << "got here" << endl;                                                                                                     
    StPicoEvent* event = dst->event();
    //StPicoBTofPidTraits* btofPidTraits = dst->
    if( !event ) {
      cout << "Something went wrong, event is empty" << endl;
      break;
    }
    
    
    //Get the good Entry
    //StPicoEvent *event = dst->GetEntry((*goodEntry).first);
    


    //Get the ith primary vertex
    //primaryVertex = event->GetPrimaryVertex(*iPrimaryVertex);
    //Figure out the centrality bin here
    //Int_t centralityVariable = GetUserCentralityVariable(event);
    //Int_t centralityVariable = event->refMult();
    Int_t centralityVariable = GetCentralityVariable(dst, event);
    //vector<double> bob; bob[3]=4;
    //std::cout << "  centVar= " << centralityVariable << endl;
    Int_t centralityBin = GetCentralityBin(centralityVariable);
    //std::cout << "  centBin= " << centralityBin << endl;
    //Skip this vertex if it has a bad centrality bin determination
    if (centralityBin < 0){
      //cout << "Negative Centrality Bin" << endl;
	     continue;
    }
    //Fill the Centrality Histograms
    nEventsHisto->Fill(centralityBin);
    centVarHisto.at(centralityBin)->Fill((double)centralityVariable);
    //Loop Over the Primary Tracks of this primary vertex
    Int_t nPrimaryTracks = (Int_t)dst->numberOfTracks();
    StPicoTrack* track = NULL;
    for (Int_t iTrackIndex=0; iTrackIndex<nPrimaryTracks; iTrackIndex++){

	    //Get the ith primary track and check if it passes the cuts
	    track = dst->track(iTrackIndex);

      //will check if primary among other things
	    if (!IsGoodTrack(track,event->primaryVertex(),true))
	      continue;
     
	    //Loop over Each of the Particle Species and Fill the Yield Histograms
	    for (Int_t iParticle=0; iParticle<nParticles; iParticle++){
     
	      //Skip Electrons and Muons
	      if (iParticle == ELECTRON || iParticle == MUON)
	        continue;
     
	      Double_t rapidity = track->getRapidity(particleInfo->GetParticleMass(iParticle));
	      Double_t mTm0 = track->getmTm0(particleInfo->GetParticleMass(iParticle));
	      
        Double_t nSigmaTPC = 0;
        if(iParticle == PION) nSigmaTPC = track->nSigmaPion();
        else if(iParticle == KAON) nSigmaTPC = track->nSigmaKaon();
        else if(iParticle == PROTON) nSigmaTPC = track->nSigmaProton();
        else if(iParticle == ELECTRON) nSigmaTPC = track->nSigmaElectron();
        else { cout << "nSigmaTPC not found " << endl; nSigmaTPC = -999; }
     
	      
	      

        

        //Compute the zTPC variable and fill the yield histogram
        //NEED TO CHANGE dEdx to TRUNCATED IN LINE BELOW... How?
	      Double_t zTPC = particleInfo->ComputeZTPC(track->dEdx()/pow(10,6),track->pPt(),
	    					    track->pMom().Z(),iParticle);
	      //std::cout << "zTPC = " << zTPC << std::endl;      
	      if (track->charge() > 0){
	        ZTPCHistoPlus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTPC);
	        if (fabs(nSigmaTPC) < 2.0)
	          TPCTracksPlus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0);
	      }
	      else if (track->charge() < 0){
	        ZTPCHistoMinus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTPC);
	        if (fabs(nSigmaTPC) < 2.0)
	          TPCTracksMinus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0);
	      }
     
              //Read BTOF Beta
              Int_t bTofPidIndex = track->bTofPidTraitsIndex();
              Float_t tofBeta = -999;
              if (bTofPidIndex!=-1){
                StPicoBTofPidTraits* traits = dst->btofPidTraits(bTofPidIndex);
                tofBeta = traits->btofBeta();
              }
              //Check to make sure this track has a TOF hit and a physical 1/Beta
              //if (track->GetTofMatchFlag() < 1 || 1.0/track->GetTofBeta() < 1.0)
              if (tofBeta > 1.0 || tofBeta < 0.0) continue;

              //std::cout << "TOF Beta: " << tofBeta << std::endl;
              //std::cout << "Rapidity: " << rapidity << std::endl;
              //std::cout << "mTm0: " << mTm0 << std::endl;
             

	      //Compute the zTOF variable and fill the yield Histogram
	      Double_t zTOF = particleInfo->ComputeZTOF(1.0/tofBeta,track->pPt(),
	    					    track->pMom().Z(),iParticle);
     

	      if (track->charge() > 0){
	        ZTOFHistoPlus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTOF);
	        if (fabs(nSigmaTPC) < 2.0)
	          TOFTracksPlus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0);
	      }
	      else if (track->charge() < 0){
	        ZTOFHistoMinus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0,zTOF);
	        if (fabs(nSigmaTPC) < 2.0)
	          TOFTracksMinus.at(iParticle).at(centralityBin)->Fill(rapidity,mTm0);
	      }
     
	    }//End Loop Over Particle Species

    }//End Loop Over Primary Tracks

  }//End Loop Over Events/Entry

  picoReader->Finish();
  //Write the Histograms to File
  outFile->cd();
  nEventsHisto->Write();

  outFile->mkdir("CentralityVariableHistograms");
  outFile->cd("CentralityVariableHistograms");
  for (Int_t iCentBin=0; iCentBin<nCentralityBins; iCentBin++){
    centVarHisto.at(iCentBin)->Write();
  }
  outFile->cd();

  for (Int_t iParticle=0; iParticle<nParticles; iParticle++){

    //Skip Electrons and Muons
    if (iParticle == ELECTRON || iParticle == MUON)
      continue;

    outFile->mkdir(Form("%s",particleInfo->GetParticleName(iParticle,-1).Data()));
    outFile->mkdir(Form("%s",particleInfo->GetParticleName(iParticle,1).Data()));

    for (Int_t iCentBin=0; iCentBin<nCentralityBins; iCentBin++){

      outFile->cd(Form("%s",particleInfo->GetParticleName(iParticle,1).Data()));
      ZTPCHistoPlus.at(iParticle).at(iCentBin)->Write();
      ZTOFHistoPlus.at(iParticle).at(iCentBin)->Write();
      TPCTracksPlus.at(iParticle).at(iCentBin)->Write();
      TOFTracksPlus.at(iParticle).at(iCentBin)->Write();

      outFile->cd();

      outFile->cd(Form("%s",particleInfo->GetParticleName(iParticle,-1).Data()));
      ZTPCHistoMinus.at(iParticle).at(iCentBin)->Write();
      ZTOFHistoMinus.at(iParticle).at(iCentBin)->Write();
      TPCTracksMinus.at(iParticle).at(iCentBin)->Write();
      TOFTracksMinus.at(iParticle).at(iCentBin)->Write();
      
      outFile->cd();
    }

  }//End Loop Over particle species

  outFile->Close();


}
