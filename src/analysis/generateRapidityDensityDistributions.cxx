#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TAxis.h>
#include <TDirectory.h>
#include <TH1F.h>
#include <TLine.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraClass.h"
#include "SpectraFitUtilities.h"
#include "SpectraFitFunctions.h"
#include "SimFitter.h"
#include "StyleSettings.h"

bool draw = false;
bool save = true;

//_________________________________________________________________
void generateRapidityDensityDistributions(TString spectraFileName, const Int_t pid, const Int_t charge,
					  Int_t userCentBin = -999){

  //gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const unsigned int nCentBins(GetNCentralityBins());

  //Set the Pid Vector
  std::vector<int> particles;
  particles.push_back(pid);
  
  //Open the Spectra File
  TFile *spectraFile = new TFile(spectraFileName,save ? "UPDATE" : "READ");
  TString saveDir = TString::Format("dNdyDistributions_%s",particleInfo->GetParticleName(pid,charge).Data());

  //Create a Canvas if drawing
  TCanvas *canvas = NULL;
  TH1F *dNdyFrame = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,800,600);
    canvas->SetTopMargin(.05);
    canvas->SetRightMargin(.05);
  }
  cout <<"Initializing Histograms and Graphs..." <<endl;
  //Create the dNdy Graphs and Chi2 Graphs
  std::vector<std::vector<TH1F *> >dNdyGraphs
    (nCentBins, std::vector<TH1F *>
     (2,(TH1F *)NULL)); //Measured and Reflected
  std::vector<std::vector<TGraphAsymmErrors *> > dNdyGraphsSys
    (nCentBins, std::vector<TGraphAsymmErrors *>
     (2,(TGraphAsymmErrors *)NULL));
  std::vector<std::vector<TGraphAsymmErrors *> > dNdyGraphsTotalErr
    (nCentBins, std::vector<TGraphAsymmErrors *>
     (2,(TGraphAsymmErrors *)NULL));
  for (unsigned int iCentBin = 0; iCentBin<nCentBins; iCentBin++){

    //Histogram with Statistical Errors
    dNdyGraphs.at(iCentBin).at(0) = new TH1F(Form("dNdyHisto_%s_Cent%02d",
						  particleInfo->GetParticleName(pid,charge).Data(),
						  iCentBin),
					     Form("%s dNdy Distribution | Cent=[%d,%d]%%;y_{%s};dNdy",
						  particleInfo->GetParticleSymbol(pid,charge).Data(),
						  iCentBin != nCentBins-1 ? (int)GetCentralityPercents().at(iCentBin+1) : 0,
						  (int)GetCentralityPercents().at(iCentBin),
						  particleInfo->GetParticleSymbol(pid,charge).Data()),
					     nRapidityBins,rapidityMin,rapidityMax);
    dNdyGraphs.at(iCentBin).at(0)->SetMarkerStyle(kFullCircle);
    dNdyGraphs.at(iCentBin).at(0)->SetMarkerColor(GetCentralityColor(iCentBin));
    
    dNdyGraphs.at(iCentBin).at(1) = new TH1F(Form("dNdyHisto_%s_Cent%02d_Reflected",
						  particleInfo->GetParticleName(pid,charge).Data(),
						  iCentBin),
					     Form("%s dNdy Distribution (Reflected) | Cent=[%d,%d]%%;y_{%s};dNdy",
						  particleInfo->GetParticleSymbol(pid,charge).Data(),
						  iCentBin != nCentBins-1 ? (int)GetCentralityPercents().at(iCentBin+1) : 0,
						  (int)GetCentralityPercents().at(iCentBin),
						  particleInfo->GetParticleSymbol(pid,charge).Data()),
					     nRapidityBins,rapidityMin,rapidityMax);
    dNdyGraphs.at(iCentBin).at(1)->SetMarkerStyle(kOpenCircle);
    dNdyGraphs.at(iCentBin).at(1)->SetMarkerColor(GetCentralityColor(iCentBin));

    //Graphs with Systematic Errors
    dNdyGraphsSys.at(iCentBin).at(0) = new TGraphAsymmErrors();
    dNdyGraphsSys.at(iCentBin).at(0)->SetName(Form("%s_Sys",dNdyGraphs.at(iCentBin).at(0)->GetName()));
    dNdyGraphsSys.at(iCentBin).at(0)->SetTitle(Form("%s (Systematic);%s;%s",
						    dNdyGraphs.at(iCentBin).at(0)->GetTitle(),
						    dNdyGraphs.at(iCentBin).at(0)->GetXaxis()->GetTitle(),
						    dNdyGraphs.at(iCentBin).at(0)->GetYaxis()->GetTitle()));					  
    dNdyGraphsSys.at(iCentBin).at(0)->SetMarkerStyle(kFullSquare);
    dNdyGraphsSys.at(iCentBin).at(0)->SetMarkerColor(GetCentralityColor(iCentBin));
    dNdyGraphsSys.at(iCentBin).at(0)->SetFillColor(GetCentralityColor(iCentBin));
    dNdyGraphsSys.at(iCentBin).at(0)->SetFillStyle(3001);
    
    dNdyGraphsSys.at(iCentBin).at(1) = new TGraphAsymmErrors();
    dNdyGraphsSys.at(iCentBin).at(1)->SetName(Form("%s_Sys",dNdyGraphs.at(iCentBin).at(1)->GetName()));
    dNdyGraphsSys.at(iCentBin).at(1)->SetTitle(Form("%s (Systematic);%s;%s",
						    dNdyGraphs.at(iCentBin).at(1)->GetTitle(),
						    dNdyGraphs.at(iCentBin).at(1)->GetXaxis()->GetTitle(),
						    dNdyGraphs.at(iCentBin).at(1)->GetYaxis()->GetTitle()));
    dNdyGraphsSys.at(iCentBin).at(1)->SetMarkerStyle(kOpenSquare);
    dNdyGraphsSys.at(iCentBin).at(1)->SetMarkerColor(GetCentralityColor(iCentBin));
    dNdyGraphsSys.at(iCentBin).at(1)->SetFillColor(GetCentralityColor(iCentBin));
    dNdyGraphsSys.at(iCentBin).at(1)->SetFillStyle(3001);

    //Graphs with total Errors
    dNdyGraphsTotalErr.at(iCentBin).at(0) = new TGraphAsymmErrors();
    dNdyGraphsTotalErr.at(iCentBin).at(0)->SetName(Form("%s_Total",dNdyGraphs.at(iCentBin).at(0)->GetName()));
    dNdyGraphsTotalErr.at(iCentBin).at(0)->SetTitle(Form("%s (Total Error);%s;%s",
						      dNdyGraphs.at(iCentBin).at(0)->GetTitle(),
						      dNdyGraphs.at(iCentBin).at(0)->GetXaxis()->GetTitle(),
						      dNdyGraphs.at(iCentBin).at(0)->GetYaxis()->GetTitle()));					  
    dNdyGraphsTotalErr.at(iCentBin).at(0)->SetMarkerStyle(kFullSquare);
    dNdyGraphsTotalErr.at(iCentBin).at(0)->SetMarkerColor(GetCentralityColor(iCentBin));
    dNdyGraphsTotalErr.at(iCentBin).at(0)->SetFillColor(GetCentralityColor(iCentBin));
    dNdyGraphsTotalErr.at(iCentBin).at(0)->SetFillStyle(3001);
    
    dNdyGraphsTotalErr.at(iCentBin).at(1) = new TGraphAsymmErrors();
    dNdyGraphsTotalErr.at(iCentBin).at(1)->SetName(Form("%s_Total",dNdyGraphs.at(iCentBin).at(1)->GetName()));
    dNdyGraphsTotalErr.at(iCentBin).at(1)->SetTitle(Form("%s (TotalError);%s;%s",
						      dNdyGraphs.at(iCentBin).at(1)->GetTitle(),
						      dNdyGraphs.at(iCentBin).at(1)->GetXaxis()->GetTitle(),
						      dNdyGraphs.at(iCentBin).at(1)->GetYaxis()->GetTitle()));
    dNdyGraphsTotalErr.at(iCentBin).at(1)->SetMarkerStyle(kOpenSquare);
    dNdyGraphsTotalErr.at(iCentBin).at(1)->SetMarkerColor(GetCentralityColor(iCentBin));
    dNdyGraphsTotalErr.at(iCentBin).at(1)->SetFillColor(GetCentralityColor(iCentBin));
    dNdyGraphsTotalErr.at(iCentBin).at(1)->SetFillStyle(3001);
    
  }

  int minRapidityBin = GetMinRapidityIndexOfInterest(pid);
  int maxRapidityBin = GetMaxRapidityIndexOfInterest(pid);
  cout <<"Loading Spectra ..." <<endl;
  //Load the Spectra
  //The most internal vector holds all the spectra of equal |y|
  std::vector<std::vector<std::vector<SpectraClass *> > > spectra
    (nCentBins,std::vector<std::vector<SpectraClass *> >
     (nRapidityBins, std::vector<SpectraClass *>
      (0,(SpectraClass *)NULL)));
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    for (unsigned int yIndex=0; yIndex<=nRapidityBins/2; yIndex++){

      //Skip if this rapidity bin lies outside the range of interest
      if ((int)yIndex < minRapidityBin || (int)yIndex > maxRapidityBin)
	continue;
      
      std::vector<int> yIndexVec;
      yIndexVec.push_back(yIndex);
      if ((int)yIndex != GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)))
	yIndexVec.push_back(GetRapidityIndex(-1.*GetRapidityRangeCenter(yIndex)));
      
      for (unsigned int iY=0; iY<yIndexVec.size(); iY++){
	
	TString spectraName = TString::Format("CorrectedSpectra_%s_Cent%02d_yIndex%02d_Total",
					      particleInfo->GetParticleName(pid,charge).Data(),
					      iCentBin,yIndexVec.at(iY));
	TString spectraClassDir = TString::Format("FitSpectraClass_%s",
						  particleInfo->GetParticleName(pid,charge).Data());
	TString spectrumDir = TString::Format("CorrectedSpectra_%s",
					      particleInfo->GetParticleName(pid,charge).Data());
	TString spectrumFitDir = TString::Format("%s_FitFuncs",
						 spectraClassDir.Data());
	TString dNdyDir = TString::Format("%s",saveDir.Data());
	
	spectraFile->cd(spectraClassDir.Data());
	SpectraClass *temp = (SpectraClass *)gDirectory->Get(spectraName.Data());
	
	if (temp){
	  
	  spectra.at(iCentBin).at(yIndex).push_back(temp);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraFile(spectraFile);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraClassDir(spectraClassDir);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraDir(spectrumDir);
	  spectra.at(iCentBin).at(yIndex).back()->SetSpectraNamePrefix("correctedSpectra");
	  spectra.at(iCentBin).at(yIndex).back()->LoadSpectra();
	  
	}//End If Spectra Exists
	
      }//End Loop Over Mirrored RapidityIndices
    }//End Loop Over yIndex
  }//End Loop Over CentIndex

  cout <<"Constructing rapidity density distributions..." <<endl;
  
  //Construct the Rapidity Density Distributions
  for (int iCentBin=nCentBins-1; iCentBin>=0; iCentBin--){
    
    //Skip Bins that are not requested by the user
    if (userCentBin != -999 && userCentBin >= 0 && (int)iCentBin != userCentBin)
      continue;

    //Vector of weighted RMS Vals - used for determining the error at mid rapidity
    std::vector<double> weightedRMSVals;
    for (int yIndex=(nRapidityBins/2); yIndex>=0; yIndex--){
      
      //Skip this bin if there are no spectra
      const unsigned int nSpectra = spectra.at(iCentBin).at(yIndex).size();
      if (nSpectra == 0)
	continue;

      std::vector<double> dNdyVals, dNdyValsSysHigh, dNdyValsSysLow;
      std::vector<double> dNdyWeights;
      for (unsigned int iSpectrum=0; iSpectrum<nSpectra; iSpectrum++){

	double dNdy = spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdy(0);
	dNdyVals.push_back(dNdy);
	if (spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdyErr(0) > 0)
	  dNdyWeights.push_back(TMath::Power(spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdyErr(0),-2));
	else
	  dNdyWeights.push_back(1);
	
	if (spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetNSpectrumFits() > 1){
	  double dNdySys = spectra.at(iCentBin).at(yIndex).at(iSpectrum)->GetdNdy(1);
	  if (dNdySys > dNdy){
	    dNdyValsSysHigh.push_back(fabs(dNdySys - dNdy));
	  }
	  else {
	    dNdyValsSysLow.push_back(fabs(dNdySys - dNdy));
	  }
	}
	
      }//End Loop Over Spectra
      
      //Compute the Weighted Averages
      double avgdNdy = TMath::Mean(dNdyVals.size(),&dNdyVals.at(0),&dNdyWeights.at(0));
      double avgdNdyErr = TMath::RMS(dNdyVals.size(),&dNdyVals.at(0),&dNdyWeights.at(0));
      
      double maxdNdySysHigh(0), maxdNdySysLow(0);
      if (dNdyValsSysHigh.size() > 0)
	maxdNdySysHigh = TMath::MaxElement(dNdyValsSysHigh.size(),&dNdyValsSysHigh.at(0));
      if (dNdyValsSysLow.size() > 0)
	maxdNdySysLow  = TMath::MaxElement(dNdyValsSysLow.size(),&dNdyValsSysLow.at(0));      
            
      //Get the Rapidity of this bin pair
      Double_t rapidityAbs =
	fabs(GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(0)->GetRapidityIndex()));
      
      if (rapidityAbs > rapidityBinWidth/2.0){
	weightedRMSVals.push_back(avgdNdyErr/avgdNdy);
      }
      
      //Find the Bin in the histogram
      int iBin = dNdyGraphs.at(iCentBin).at(0)->FindBin(rapidityAbs);
      
      //Set the Bin Contents of the Bin
      dNdyGraphs.at(iCentBin).at(0)->SetBinContent(iBin,avgdNdy);
      dNdyGraphsSys.at(iCentBin).at(0)->SetPoint(dNdyGraphsSys.at(iCentBin).at(0)->GetN(),rapidityAbs,avgdNdy);
      dNdyGraphsSys.at(iCentBin).at(0)->SetPointError(dNdyGraphsSys.at(iCentBin).at(0)->GetN()-1,
						      rapidityBinWidth/2.0,rapidityBinWidth/2.0,
						      maxdNdySysLow,maxdNdySysHigh);
      dNdyGraphsTotalErr.at(iCentBin).at(0)->SetPoint(dNdyGraphsTotalErr.at(iCentBin).at(0)->GetN(),rapidityAbs,avgdNdy);
      dNdyGraphsTotalErr.at(iCentBin).at(0)->SetPointError(dNdyGraphsTotalErr.at(iCentBin).at(0)->GetN()-1,
							rapidityBinWidth/2.0,rapidityBinWidth/2.0,
							maxdNdySysLow,maxdNdySysHigh); //Will Be Combined with Stat Error Below
      if (rapidityAbs > rapidityBinWidth/2.0){
	dNdyGraphs.at(iCentBin).at(1)->SetBinContent(dNdyGraphs.at(iCentBin).at(1)->FindBin(rapidityAbs*-1),avgdNdy);
	dNdyGraphsSys.at(iCentBin).at(1)->SetPoint(dNdyGraphsSys.at(iCentBin).at(1)->GetN(),rapidityAbs*-1,avgdNdy);
	dNdyGraphsSys.at(iCentBin).at(1)->SetPointError(dNdyGraphsSys.at(iCentBin).at(1)->GetN()-1,
							rapidityBinWidth/2.0,rapidityBinWidth/2.0,
							maxdNdySysLow,maxdNdySysHigh);
	dNdyGraphsTotalErr.at(iCentBin).at(1)->SetPoint(dNdyGraphsTotalErr.at(iCentBin).at(1)->GetN(),rapidityAbs*-1,avgdNdy);
	dNdyGraphsTotalErr.at(iCentBin).at(1)->SetPointError(dNdyGraphsTotalErr.at(iCentBin).at(1)->GetN()-1,
							     rapidityBinWidth/2.0,rapidityBinWidth/2.0,
							     maxdNdySysLow,maxdNdySysHigh);//Will be combined with stat error below
							
      }
      
    }//End Loop Over Rapidity Bins

    //Assign the Errors
    double averageRelativeRMS = TMath::Mean(weightedRMSVals.size(),&weightedRMSVals.at(0));
    for (int yIndex=(nRapidityBins/2); yIndex>=0; yIndex--){
      
      //Skip this bin if there are no spectra
      const unsigned int nSpectra = spectra.at(iCentBin).at(yIndex).size();
      if (nSpectra == 0)
	continue;

      Double_t rapidityAbs =
	fabs(GetRapidityRangeCenter(spectra.at(iCentBin).at(yIndex).at(0)->GetRapidityIndex()));
      
      //Find the Bin in the histogram
      int iBin = dNdyGraphs.at(iCentBin).at(0)->FindBin(rapidityAbs);
      int iPoint = TGraphFindPoint(dNdyGraphsTotalErr.at(iCentBin).at(0),rapidityAbs);
      int iPointRef = TGraphFindPoint(dNdyGraphsTotalErr.at(iCentBin).at(1),rapidityAbs*-1);

      double error = averageRelativeRMS * dNdyGraphs.at(iCentBin).at(0)->GetBinContent(iBin);
      
      dNdyGraphs.at(iCentBin).at(0)->SetBinError(iBin,error);
      dNdyGraphsTotalErr.at(iCentBin).at(0)->SetPointError(iPoint,
							   rapidityBinWidth/2.0,rapidityBinWidth/2.0,
							   sqrt(pow(dNdyGraphsTotalErr.at(iCentBin).at(0)->GetEYlow()[iPoint],2) +
								pow(error,2)),
							   sqrt(pow(dNdyGraphsTotalErr.at(iCentBin).at(0)->GetEYhigh()[iPoint],2) +
								pow(error,2)));

      if (rapidityAbs > rapidityBinWidth/2.0){
	dNdyGraphs.at(iCentBin).at(1)->SetBinError(dNdyGraphs.at(iCentBin).at(1)->FindBin(rapidityAbs*-1),error);
	dNdyGraphsTotalErr.at(iCentBin).at(1)->SetPointError(iPointRef,
							     rapidityBinWidth/2.0,rapidityBinWidth/2.0,
							     sqrt(pow(dNdyGraphsTotalErr.at(iCentBin).at(1)->GetEYlow()[iPointRef],2) +
								  pow(error,2)),
							     sqrt(pow(dNdyGraphsTotalErr.at(iCentBin).at(1)->GetEYhigh()[iPointRef],2) +
								  pow(error,2)));
      }
      
    }//End Loop Over Rapidity Bins


    //*************************
    //**** DRAW IF DESIRED ****
    //*************************
    if (draw){
      canvas->cd();
      
      dNdyGraphs.at(iCentBin).at(0)->Draw(iCentBin == (int)(nCentBins-1) ? "E" : "ESAME");
      dNdyGraphs.at(iCentBin).at(1)->Draw("ESAME");
      dNdyGraphsSys.at(iCentBin).at(0)->Draw("2");
      dNdyGraphsSys.at(iCentBin).at(1)->Draw("2");
      dNdyGraphsTotalErr.at(iCentBin).at(0)->Draw("PZ");
      dNdyGraphsTotalErr.at(iCentBin).at(1)->Draw("PZ");
      
      canvas->Update();
      
    }//End Draw
    
  }//End Loop Over Centrality Bins


  //*************************
  //**** SAVE IF DESIRED ****
  //*************************
  if (save){
    spectraFile->cd();
    spectraFile->mkdir(saveDir.Data());
    spectraFile->cd(saveDir.Data());
    for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
      for (int i=0; i<2; i++){
	dNdyGraphs.at(iCentBin).at(i)->Write("",TObject::kOverwrite);
	dNdyGraphsSys.at(iCentBin).at(i)->Write("",TObject::kOverwrite);
	dNdyGraphsTotalErr.at(iCentBin).at(i)->Write("",TObject::kOverwrite);
      }//End Loop Over Measured/Reflected      
    }//End Loop Over Cent Bin
  }//End Save

}
