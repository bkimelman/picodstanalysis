//Fit the Pion Spectra

#include <iostream>
#include <vector>
#include <utility>

#include <TStyle.h>
#include <TMath.h>
#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TSystem.h>
#include <TVirtualFitter.h>
#include <TAxis.h>
#include <TDirectory.h>

#include "globalDefinitions.h"
#include "utilityFunctions.h"
#include "StRefMultExtendedCorr.h"
#include "UserCuts.h"
#include "ParticleInfo.h"
#include "SpectraClass.h"
#include "SpectraFitUtilities.h"
#include "SpectraFitFunctions.h"
#include "StyleSettings.h"

bool draw = true;

//_______________________________________________________________________________________________
bool SkipSpectrum(SpectraClass *sp, int centBin, int yIndex, int userCentBin, double userRapidity){

  if (!sp)
    return true;
    
  const int userRapidityBin = GetRapidityIndex(userRapidity);
  if (userRapidityBin >=0 && userRapidityBin != yIndex)
    return true;
  
  const unsigned int nCentBins = GetNCentralityBins();
  if (userCentBin != -999 && userCentBin >= 0 &&
      userCentBin < (int)nCentBins && (int)centBin != userCentBin)
    return true;
  
  return false;
}

//_____________________________________________________________________________________________
void fitPionSpectra(TString spectraFileName, Int_t charge,
		    Int_t userCentBin = -999, Double_t userRapidity = -999){

  gStyle->SetOptFit(1);
  ParticleInfo *particleInfo = new ParticleInfo();
  const unsigned int pid = PION;
  const double pidMass = particleInfo->GetParticleMass(pid);
  const unsigned int nCentBins = GetNCentralityBins();

  //Open the SpectraFile
  TFile *spectraFile = new TFile(spectraFileName,"UPDATE");
  TString spectraClassDir = TString::Format("CombinedCorrectedSpectraClass_%s",
					    particleInfo->GetParticleName(pid,charge).Data());
  TString spectrumDir = TString::Format("CombinedCorrectedSpectra_%s",
					particleInfo->GetParticleName(pid,charge).Data());
  TString spectrumFitDir = TString::Format("%s_Fit",
					   spectrumDir.Data());
  TString dNdyDir = TString::Format("dNdyGraph_%s",
				    particleInfo->GetParticleName(pid,charge).Data());
  
  //Create a Canvas if Drawing
  TCanvas *canvas = NULL;
  if (draw){    
    canvas = new TCanvas("canvas","canvas",20,20,1600,1000);
    canvas->Divide(3,2);
    canvas->cd(1);
    gPad->SetLogy();
  }

  //**********************************************************
  //Create all the Objects we need
  //**********************************************************
  std::vector < std::vector <SpectraClass *> > spectra
    (nCentBins, std::vector<SpectraClass *>
     (nRapidityBins, (SpectraClass *)NULL));
  std::vector < std::vector <TF1 *> > spectraFit
    (nCentBins, std::vector<TF1 *>
     (nRapidityBins, (TF1 *)NULL));
  std::vector <TGraphErrors *> lowmTSlopeGraph
    (nCentBins, (TGraphErrors *)NULL);
  std::vector <TGraphErrors *> highmTSlopeGraph
    (nCentBins, (TGraphErrors *)NULL);
  std::vector <TF1 *> lowmTSlopeGraphFit
    (nCentBins, (TF1 *)NULL);
  std::vector <TF1 *> highmTSlopeGraphFit
    (nCentBins, (TF1 *)NULL);
  std::vector <TGraphErrors *> stitchParamGraph
    (nCentBins, (TGraphErrors *)NULL);
  std::vector <TF1 *> stitchParamGraphFit
    (nCentBins, (TF1 *)NULL);
  std::vector <TGraphErrors *> chi2Graph
    (nCentBins, (TGraphErrors *)NULL);
  std::vector <TGraphErrors *> dNdyGraph     //Will have total Error
    (nCentBins, (TGraphErrors *)NULL);
  std::vector <TGraphErrors *> dNdyGraphStat //Will have Stat Only
    (nCentBins, (TGraphErrors *)NULL);
  std::vector <TGraphErrors *> dNdyGraphSys  //Will have Sys only
    (nCentBins, (TGraphErrors *)NULL);

  
  //Loop Over Centrality Bins
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){

    //Set up the Parameter Graphs and Fits
    lowmTSlopeGraph.at(iCentBin) = new TGraphErrors();
    lowmTSlopeGraph.at(iCentBin)->SetName(Form("lowmTSlopeGraph_%s_Cent%02d",
					       particleInfo->GetParticleName(pid,charge).Data(),
					       iCentBin));
    lowmTSlopeGraph.at(iCentBin)->SetTitle(Form("Low m_{T}-m_{0} Bose-Einstein Slope;y_{%s};T_{Bose}",
						particleInfo->GetParticleSymbol(pid,charge).Data()));
    lowmTSlopeGraph.at(iCentBin)->SetMarkerStyle(kFullCircle);
    lowmTSlopeGraph.at(iCentBin)->SetMarkerColor(GetCentralityColor(iCentBin));

    highmTSlopeGraph.at(iCentBin) = new TGraphErrors();
    highmTSlopeGraph.at(iCentBin)->SetName(Form("highmTSlopeGraph_%s_Cent%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin));
    highmTSlopeGraph.at(iCentBin)->SetTitle(Form("High m_{T}-m_{0} m_{T}Exponential Slope;y_{%s};T_{mTExpo}",
						particleInfo->GetParticleSymbol(pid,charge).Data()));
    highmTSlopeGraph.at(iCentBin)->SetMarkerStyle(kFullCircle);
    highmTSlopeGraph.at(iCentBin)->SetMarkerColor(GetCentralityColor(iCentBin));
    
    lowmTSlopeGraphFit.at(iCentBin) = new TF1(Form("%s_Fit",
						   lowmTSlopeGraph.at(iCentBin)->GetName()),
					      "gaus(0)",-2,2);
    highmTSlopeGraphFit.at(iCentBin) = new TF1(Form("%s_Fit",
						   highmTSlopeGraph.at(iCentBin)->GetName()),
					      "gaus(0)",-2,2);
    lowmTSlopeGraphFit.at(iCentBin)->SetParameters(.2,0.0,2.0);
    highmTSlopeGraphFit.at(iCentBin)->SetParameters(.2,0.0,2.0);
    lowmTSlopeGraphFit.at(iCentBin)->FixParameter(1,0.0);
    highmTSlopeGraphFit.at(iCentBin)->FixParameter(1,0.0);

    stitchParamGraph.at(iCentBin) = new TGraphErrors();
    stitchParamGraph.at(iCentBin)->SetName(Form("stitchParamGraph_%s_Cent%02d",
						particleInfo->GetParticleName(pid,charge).Data(),
						iCentBin));
    stitchParamGraph.at(iCentBin)->SetTitle(Form("Stitch Parameter;y_{%s};stitch",
						 particleInfo->GetParticleSymbol(pid,charge).Data()));
    stitchParamGraph.at(iCentBin)->SetMarkerStyle(kFullCircle);
    stitchParamGraph.at(iCentBin)->SetMarkerColor(GetCentralityColor(iCentBin));

    stitchParamGraphFit.at(iCentBin) = new TF1(Form("%s_Fit",
						    stitchParamGraph.at(iCentBin)->GetName()),
					       "gaus(0)",-2,2);
    stitchParamGraphFit.at(iCentBin)->SetParameters(.7,0.0,1.5);
    stitchParamGraphFit.at(iCentBin)->FixParameter(1,0.0);

    dNdyGraph.at(iCentBin) = new TGraphErrors();
    dNdyGraph.at(iCentBin)->SetName(Form("dNdyGraph_%s_Cent%02d",
					 particleInfo->GetParticleName(pid,charge).Data(),
					 iCentBin));
    dNdyGraph.at(iCentBin)->SetTitle(Form("dN/dy of %s;y_{%s};dNdy",
					  particleInfo->GetParticleSymbol(pid,charge).Data(),
					  particleInfo->GetParticleSymbol(pid,charge).Data()));
    dNdyGraph.at(iCentBin)->SetMarkerStyle(kFullCircle);
    dNdyGraph.at(iCentBin)->SetMarkerColor(GetCentralityColor(iCentBin));

    dNdyGraphSys.at(iCentBin) = new TGraphErrors();
    dNdyGraphSys.at(iCentBin)->SetName(Form("%s_Sys",dNdyGraph.at(iCentBin)->GetName()));
    dNdyGraphSys.at(iCentBin)->SetMarkerColor(GetCentralityColor(iCentBin));

    dNdyGraphStat.at(iCentBin) = new TGraphErrors();
    dNdyGraphStat.at(iCentBin)->SetName(Form("%s_Stat",dNdyGraph.at(iCentBin)->GetName()));
    dNdyGraphStat.at(iCentBin)->SetMarkerColor(GetCentralityColor(iCentBin));
    
    chi2Graph.at(iCentBin) = new TGraphErrors();
    chi2Graph.at(iCentBin)->SetName(Form("chi2Graph_%s_Cent%02d",
					 particleInfo->GetParticleName(pid,charge).Data(),
					 iCentBin));
    chi2Graph.at(iCentBin)->SetTitle(Form("#chi^{2}/NDF of %s;y_{%s};#chi^{2}/NDF",
					  particleInfo->GetParticleSymbol(pid,charge).Data(),
					  particleInfo->GetParticleSymbol(pid,charge).Data()));
    chi2Graph.at(iCentBin)->SetMarkerStyle(kFullCircle);
    chi2Graph.at(iCentBin)->SetMarkerColor(GetCentralityColor(iCentBin));
    

    //Loop Over Rapidity Bins
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      //Get the Spectrum from the file
      spectraFile->cd(spectraClassDir.Data());
      TString spectrumName = TString::Format("combinedSpectra_%s_Cent%02d_yIndex%02d",
					     particleInfo->GetParticleName(pid,charge).Data(),
					     iCentBin,yIndex);
      spectra.at(iCentBin).at(yIndex) = (SpectraClass *)gDirectory->
	Get(spectrumName.Data());

      //Skip if the spectrum does not exist
      if (!spectra.at(iCentBin).at(yIndex))
	continue;
      spectra.at(iCentBin).at(yIndex)->SetSpectraFile(spectraFile);
      spectra.at(iCentBin).at(yIndex)->SetSpectraClassDir(spectraClassDir);
      spectra.at(iCentBin).at(yIndex)->SetSpectraDir(spectrumDir);
      spectra.at(iCentBin).at(yIndex)->SetSpectraNamePrefix("CombinedSpectrum");
      spectra.at(iCentBin).at(yIndex)->LoadSpectra();

      //Create the Fit
      const Double_t minFitRange(0), maxFitRange(10); 
      spectraFit.at(iCentBin).at(yIndex) =
	new TF1(Form("%s_Fit1",spectrumName.Data()),
		PiecewiseBosemTExpoFitNoInversion,minFitRange,maxFitRange,5);
      spectraFit.at(iCentBin).at(yIndex)->SetNpx(100000);
      spectraFit.at(iCentBin).at(yIndex)->SetParameter(0,400);
      spectraFit.at(iCentBin).at(yIndex)->SetParameter(1,.16);
      spectraFit.at(iCentBin).at(yIndex)->SetParameter(2,.18);
      spectraFit.at(iCentBin).at(yIndex)->FixParameter(3,pidMass);
       
      spectraFit.at(iCentBin).at(yIndex)->SetParLimits(0,1,1500);
      spectraFit.at(iCentBin).at(yIndex)->SetParLimits(1,.1,.25);
      spectraFit.at(iCentBin).at(yIndex)->SetParLimits(2,.1,.25);
      spectraFit.at(iCentBin).at(yIndex)->SetLineWidth(3);
      spectraFit.at(iCentBin).at(yIndex)->SetLineColor(GetCentralityColor(iCentBin));

      //Compute Some Quantities about the length of the spectrum
      Double_t spectrumMax = spectra.at(iCentBin).at(yIndex)->GetX()
	[spectra.at(iCentBin).at(yIndex)->GetN()-1];
      Double_t spectrumTwoThirds =  spectra.at(iCentBin).at(yIndex)->GetX()
	[TMath::Nint((2./3.) * spectra.at(iCentBin).at(yIndex)->GetN())];

      //The state of the stitch parameter depends on how long the
      //spectrum is. The stitch parameter should never be less than
      //minStitchVal, but then we need a spectrum which is at least
      //minExpoLength longer to adequetly contstrain the expo part
      Double_t minStitchVal = 0.35;  //GeV
      Double_t minExpoLength = 0.1; //GeV
      
      //If the Spectrum is shorter than minStitchVal+minExpoLength then set the stitch parameter to
      //some silly high value which will never be encountered since the fit
      //range is at MOST 0-10 GeV. This turns the piecewise function into
      //a simple Bose-Einstein Fit.
      if (spectrumMax < (minStitchVal + minExpoLength) ){
	spectraFit.at(iCentBin).at(yIndex)->FixParameter(4,999);
      }

      //Otherwise Make sure the stitch parameter is no further than 2/3
      //the way down the measured spectrum and no earlier than .5 GeV
      else {
	spectraFit.at(iCentBin).at(yIndex)->
	  SetParameter(4,(minStitchVal+spectrumTwoThirds)/2.0);
	spectraFit.at(iCentBin).at(yIndex)->
	  SetParLimits(4,minStitchVal,spectrumTwoThirds);
      }
	 
      /*
      spectraFit.at(iCentBin).at(yIndex)->
	SetParameter(4,spectra.at(iCentBin).at(yIndex)->GetX()
		     [TMath::Nint(spectra.at(iCentBin).at(yIndex)->GetN()/2.0)]);
      
      spectraFit.at(iCentBin).at(yIndex)->SetParLimits(
      */

     
      
    }//End Loop Over Rapidity Bins
    
  }//End Loop Over Centrality Bins


  //**********************************************************
  //Round 1 - Parameterize Low mT-m0 Slope
  //**********************************************************
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){

    //Loop Over Rapidity Bins
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      double rapidity = GetRapidityRangeCenter(yIndex);
      SpectraClass *spectrum = spectra.at(iCentBin).at(yIndex);
      TF1 *spectrumFit = spectraFit.at(iCentBin).at(yIndex);

      if (SkipSpectrum(spectrum,iCentBin,yIndex,userCentBin,userRapidity))
	continue;

      //Fit the Spectrum
      spectrum->Fit(spectrumFit,"NQME");

      //Only Add the Point if the stitch value is valid
      if (spectrumFit->GetParameter(4) < 10){
	lowmTSlopeGraph.at(iCentBin)->SetPoint(lowmTSlopeGraph.at(iCentBin)->GetN(),rapidity,
					       spectrumFit->GetParameter(1));
	lowmTSlopeGraph.at(iCentBin)->SetPointError(lowmTSlopeGraph.at(iCentBin)->GetN()-1,rapidityBinWidth/2.0,
						    spectrumFit->GetParError(1));
      }
      
      //DRAW
      if (draw){
	canvas->cd(1);
	spectrum->DrawSpectrum("APZ");
	spectrumFit->Draw("SAME");
	canvas->cd(2);
	lowmTSlopeGraph.at(iCentBin)->Draw("APZ");

	canvas->Update();
      }//END DRAW
      
    }//End Loop Over Rapidity Bins

    //Fit the Low mT-m0 Parameter
    if (lowmTSlopeGraph.at(iCentBin)->GetN() > 0)
      lowmTSlopeGraph.at(iCentBin)->Fit(lowmTSlopeGraphFit.at(iCentBin),"QEX0");

    if (draw)
      canvas->Update();

  }//End Loop Over Centrality Bins

  //**********************************************************
  //Round 2 - Parameterize High mT-m0 Slope
  //**********************************************************
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){
    
    //Loop Over Rapidity Bins
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      double rapidity = GetRapidityRangeCenter(yIndex);
      SpectraClass *spectrum = spectra.at(iCentBin).at(yIndex);
      TF1 *spectrumFit = spectraFit.at(iCentBin).at(yIndex);
      
      if (SkipSpectrum(spectrum,iCentBin,yIndex,userCentBin,userRapidity))
	continue;

      //Fix The Low mT-m0 Slope Parameter
      spectrumFit->FixParameter(1,lowmTSlopeGraphFit.at(iCentBin)->Eval(rapidity));

      //Fit the Spectrum
      spectrum->Fit(spectrumFit,"NQME");

      //Only Add the Point if the stitch Value is valid
      if (spectrumFit->GetParameter(4) < 10){
	highmTSlopeGraph.at(iCentBin)->SetPoint(highmTSlopeGraph.at(iCentBin)->GetN(),rapidity,
						spectrumFit->GetParameter(2));
	highmTSlopeGraph.at(iCentBin)->SetPointError(highmTSlopeGraph.at(iCentBin)->GetN()-1,rapidityBinWidth/2.0,
						     spectrumFit->GetParError(2));
      }
      
      //DRAW
      if (draw){
	canvas->cd(1);
	spectrum->DrawSpectrum("APZ");
	spectrumFit->Draw("SAME");
	canvas->cd(2);
	lowmTSlopeGraph.at(iCentBin)->Draw("APZ");
	canvas->cd(3);
	highmTSlopeGraph.at(iCentBin)->Draw("APZ");
	
	canvas->Update();
      }//END DRAW
      
    }//End Loop Over Rapidity Bins

    //Fit the High mT-m0 Slope Parameter
    if (highmTSlopeGraph.at(iCentBin)->GetN() > 0)
      highmTSlopeGraph.at(iCentBin)->Fit(highmTSlopeGraphFit.at(iCentBin),"QEX0");
    
  }//End Loop Over Centrality Bins

  //**********************************************************
  //Round 3 - Stitch Parameter
  //**********************************************************
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){
    
    //Loop Over Rapidity Bins
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      double rapidity = GetRapidityRangeCenter(yIndex);
      SpectraClass *spectrum = spectra.at(iCentBin).at(yIndex);
      TF1 *spectrumFit = spectraFit.at(iCentBin).at(yIndex);
      
      if (SkipSpectrum(spectrum,iCentBin,yIndex,userCentBin,userRapidity))
	continue;

      //Fix The Low and High mT-m0 Slope Parameters
      spectrumFit->FixParameter(1,lowmTSlopeGraphFit.at(iCentBin)->Eval(rapidity));
      spectrumFit->FixParameter(2,highmTSlopeGraphFit.at(iCentBin)->Eval(rapidity));
 
      //Fit the Spectrum
      spectrum->Fit(spectrumFit,"NQME");

      //Only add the point to the graph if it is valid
      if (spectrumFit->GetParameter(4) < 999){
	stitchParamGraph.at(iCentBin)->SetPoint(stitchParamGraph.at(iCentBin)->GetN(),rapidity,
						spectrumFit->GetParameter(4));
	stitchParamGraph.at(iCentBin)->SetPointError(stitchParamGraph.at(iCentBin)->GetN()-1,rapidityBinWidth/2.0,
						     spectrumFit->GetParError(4));
      }
      
      //DRAW
      if (draw){
	canvas->cd(1);
	spectrum->DrawSpectrum("APZ");
	spectrumFit->Draw("SAME");
	canvas->cd(2);
	lowmTSlopeGraph.at(iCentBin)->Draw("APZ");
	canvas->cd(3);
	highmTSlopeGraph.at(iCentBin)->Draw("APZ");
	canvas->cd(4);
	stitchParamGraph.at(iCentBin)->Draw("APZ");
	
	canvas->Update();
      }//END DRAW
      
    }//End Loop Over Rapidity Bins

    //Fit the High mT-m0 Slope Parameter
    if (stitchParamGraph.at(iCentBin)->GetN() > 3){
      /*
      Double_t minF = TMath::MinElement(stitchParamGraph.at(iCentBin)->GetN(),
					stitchParamGraph.at(iCentBin)->GetX());
      Double_t maxF = TMath::MaxElement(stitchParamGraph.at(iCentBin)->GetN(),
					stitchParamGraph.at(iCentBin)->GetX());
      */
      stitchParamGraph.at(iCentBin)->Fit(stitchParamGraphFit.at(iCentBin),"QEX0","",-2,2);
    }
    else {
      delete stitchParamGraph.at(iCentBin);
      stitchParamGraph.at(iCentBin) = NULL;
    }
    
    if (draw)
      canvas->Update();
    
  }//End Loop Over Centrality Bins

  cout <<"STARTING ROUND 4" <<endl;
  //**********************************************************
  //Round 4 - EXTRACT THE dN/dy
  //**********************************************************
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){
    
    //Loop Over Rapidity Bins
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      double rapidity = GetRapidityRangeCenter(yIndex);
      SpectraClass *spectrum = spectra.at(iCentBin).at(yIndex);
      TF1 *spectrumFit = spectraFit.at(iCentBin).at(yIndex);
      
      if (SkipSpectrum(spectrum,iCentBin,yIndex,userCentBin,userRapidity))
	continue;

      //Fix The Low and High mT-m0 Slope Parameters and the stitch parameter
      spectrumFit->FixParameter(1,lowmTSlopeGraphFit.at(iCentBin)->Eval(rapidity));
      spectrumFit->FixParameter(2,highmTSlopeGraphFit.at(iCentBin)->Eval(rapidity));
      spectrumFit->FixParameter(4,stitchParamGraphFit.at(iCentBin)->Eval(rapidity));
      /*
      //Only Fix the stitch parameter if the stitch val fit is valid
      if (stitchParamGraphFit.at(iCentBin)){
	Double_t minF(0), maxF(0);
	spectrumFit->GetRange(minF,maxF);
	if (rapidity >= minF && rapidity <= maxF)
	  spectrumFit->FixParameter(4,stitchParamGraphFit.at(iCentBin)->Eval(rapidity));
	else
	  spectrumFit->FixParameter(4,999);
      }
      else
	spectrumFit->FixParameter(4,999);
      */
      
      //Fit the Spectrum
      spectrum->Fit(spectrumFit,"MEN");

      //Get the Confidence Interval
      TGraphErrors *confInterval = GetConfidenceIntervalOfFit(spectrumFit);
 
      spectrum->AddSpectraFit(spectrumFit,confInterval,0);
 
      chi2Graph.at(iCentBin)->SetPoint(chi2Graph.at(iCentBin)->GetN(),rapidity,
				       spectrumFit->GetChisquare()/(double)spectrumFit->GetNDF());

      //DRAW
      if (draw){
	canvas->cd(1);
	spectrum->DrawSpectrum("APZ");
	spectrumFit->Draw("SAME");
	canvas->cd(2);
	lowmTSlopeGraph.at(iCentBin)->Draw("APZ");
	canvas->cd(3);
	highmTSlopeGraph.at(iCentBin)->Draw("APZ");
	canvas->cd(4);
	if (stitchParamGraph.at(iCentBin))
	  stitchParamGraph.at(iCentBin)->Draw("APZ");
	
	canvas->Update();
      }//END DRAW
      
      if (confInterval)
	delete confInterval;
      
    }//End Loop Over Rapidity Bins
    
  }//End Loop Over Centrality Bins

  //**********************************************************
  //INVESTIGATE ERRORS ASSOCIATED WITH FIXING ALL PARAMETERS
  //    AND BY VARYING THE FIT FUNCTION
  //**********************************************************
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){
    
    //Loop Over Rapidity Bins
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      double rapidity = GetRapidityRangeCenter(yIndex);
      SpectraClass *spectrum = spectra.at(iCentBin).at(yIndex);
      
      if (SkipSpectrum(spectrum,iCentBin,yIndex,userCentBin,userRapidity))
	continue;

      //------------------------------------------------------------
      //UNFIX THE PARAMETERS IN THE DEFAULT FIT
      TF1 unFixSpectrumFit(*spectraFit.at(iCentBin).at(yIndex));
      unFixSpectrumFit.SetParLimits(1,.075,.25);
      unFixSpectrumFit.SetParLimits(2,.075,.25);
      unFixSpectrumFit.SetLineColor(kBlack);
 
      //Fit the Spectrum And Compute the dNdy
      spectrum->Fit(&unFixSpectrumFit,"MENQ");
      TGraphErrors *unFixConfInterval = GetConfidenceIntervalOfFit(&unFixSpectrumFit);
      std::pair<double,double> unFixdNdy = spectrum->ComputeTotaldNdy(&unFixSpectrumFit,unFixConfInterval);
      double statError = sqrt( pow(spectrum->GetdNdyErr(0),2) +
			       pow(fabs(spectrum->GetdNdy(0) - unFixdNdy.first),2) );

      //------------------------------------------------------------
      //TRY A TSALLIS FIT FUNCTION
      TF1 tsallisSpectrumFit(Form("%s_Fit2",spectrum->GetSpectrumName().Data()),TsallisFitNoInversion,0,10,4);
      tsallisSpectrumFit.SetParameters(100,.16,20,pidMass);
      tsallisSpectrumFit.FixParameter(3,pidMass);
      tsallisSpectrumFit.SetParLimits(0,5,1000);
      tsallisSpectrumFit.SetParLimits(1,.1,.3);
      tsallisSpectrumFit.SetParLimits(2,10,50);      
      tsallisSpectrumFit.SetLineColor(kBlack);
      tsallisSpectrumFit.SetLineWidth(3);

      //Fit the Spectrum and Compute the dNdy
      spectrum->Fit(&tsallisSpectrumFit,"MERNQ");
      TGraphErrors *tsallisConfInterval = GetConfidenceIntervalOfFit(&tsallisSpectrumFit);      
      spectrum->AddSpectraFit(&tsallisSpectrumFit,tsallisConfInterval,1);
      std::pair<double,double> tsallisdNdy =
	std::make_pair(spectrum->GetdNdy(spectrum->GetNSpectrumFits()-1),
		       spectrum->GetdNdyErr(spectrum->GetNSpectrumFits()-1));
      
      //------------------------------------------------------------
      //COMPUTE THE SYSTEMATIC ERROR ON THE DNDY
      //  1. Find the difference between the dNdy of the default fit (zeroth fit)
      //     and all of the other fits that have been added to the spectrum.
      //  2. Find the Maximum Difference and use it as the systematic error
      std::vector<double> dNdyDiffsHigh;
      std::vector<double> dNdyDiffsLow;
      for (int iFit=0; iFit<spectrum->GetNSpectrumFits()-1; iFit++){

	if (spectrum->GetdNdy(iFit+1) > spectrum->GetdNdy(0))
	  dNdyDiffsHigh.push_back(fabs(spectrum->GetdNdy(0) - spectrum->GetdNdy(iFit+1)));
	else
	  dNdyDiffsLow.push_back(fabs(spectrum->GetdNdy(0) - spectrum->GetdNdy(iFit+1)));
	
      }
      
      double dNdySysErrHigh(0), dNdySysErrLow(0);
      if (dNdyDiffsHigh.size() > 0)
	dNdySysErrHigh = TMath::MaxElement(dNdyDiffsHigh.size(),&dNdyDiffsHigh.at(0));
      if (dNdyDiffsLow.size() > 0)
	dNdySysErrLow = TMath::MaxElement(dNdyDiffsLow.size(),&dNdyDiffsLow.at(0));
      
 
      //-------------------------------------------------------------
      //COMPUTE THE SYSTEMATIC ERROR ON THE MEAN MT AND PT
      //  1. Find the difference between the mean mT and pT of the default fit
      //     and all of the other fits that have been added to the spectrum
      //  2. Find the Maxiumum difference and use it as the systematic error
      std::vector<double> meanmTDiff(spectrum->GetNSpectrumFits()-1,0);
      std::vector<double> meanpTDiff(spectrum->GetNSpectrumFits()-1,0);
      for (int iFit=0; iFit<spectrum->GetNSpectrumFits()-1; iFit++){
	meanmTDiff.at(iFit) = fabs( spectrum->GetMeanmT(0) - spectrum->GetMeanmT(iFit+1) );
	meanpTDiff.at(iFit) = fabs( spectrum->GetMeanpT(0) - spectrum->GetMeanpT(iFit+1) );
      }

      double meanmTSysErr = TMath::MaxElement(meanmTDiff.size(),&meanmTDiff.at(0));
      double meanpTSysErr = TMath::MaxElement(meanpTDiff.size(),&meanpTDiff.at(0));

      double meanmTTotalErr = sqrt( pow(meanmTSysErr,2) + pow(spectrum->GetMeanmTErr(0),2) );
      double meanpTTotalErr = sqrt( pow(meanpTSysErr,2) + pow(spectrum->GetMeanpTErr(0),2) );

      //-------------------------------------------------------------
      //SET THE RESULT QUANTITIES OF THE SPECTRUM
      //The Result dNdy is the dNdy from the default fit (zeroth fit)
      spectrum->SetdNdyResult(spectrum->GetdNdy(0));
      spectrum->SetdNdyResultStatErr(statError);
      spectrum->SetdNdyResultSysErrHigh(dNdySysErrHigh);
      spectrum->SetdNdyResultSysErrLow(dNdySysErrLow);
      //spectrum->SetdNdyResultTotalErr(dNdyTotalErr);

      //The Mean mT and pT are the results from the default fit (zeroth fit)
      spectrum->SetMeanmTResult(spectrum->GetMeanmT(0));
      spectrum->SetMeanmTResultTotalErr(meanmTTotalErr);
      spectrum->SetMeanpTResult(spectrum->GetMeanpT(0));
      spectrum->SetMeanpTResultTotalErr(meanpTTotalErr); 
      

      //-------------------------------------------------------------
      //SET POINT ON THE DNDY Graph
      dNdyGraph.at(iCentBin)->SetPoint(dNdyGraph.at(iCentBin)->GetN(),
				       rapidity,
				       spectrum->GetdNdyResult());
      dNdyGraph.at(iCentBin)->SetPointError(dNdyGraph.at(iCentBin)->GetN()-1,
					    rapidityBinWidth/2.0,
					    spectrum->GetdNdyResultStatErr());

      for (int iFit=0; iFit<spectrum->GetNSpectrumFits(); iFit++)
      
      //DRAW
      if (draw){
      	canvas->cd(1);
      	spectrum->DrawSpectrum("APZ");
	for (int iFit=0; iFit<spectrum->GetNSpectrumFits(); iFit++){
	  spectrum->GetSpectrumFit(iFit)->Draw("SAME");
	}
      	canvas->cd(2);
      	lowmTSlopeGraph.at(iCentBin)->Draw("APZ");
      	canvas->cd(3);
      	highmTSlopeGraph.at(iCentBin)->Draw("APZ");
      	canvas->cd(4);
	if (stitchParamGraph.at(iCentBin))
	  stitchParamGraph.at(iCentBin)->Draw("APZ");
      	canvas->cd(5);
      	dNdyGraph.at(iCentBin)->Draw("APZ");
      	canvas->cd(6);
      	chi2Graph.at(iCentBin)->Draw("APZ");
	
      	canvas->Update();
      }//END DRAW
      
      //Clean Up
      if (unFixConfInterval)
	delete unFixConfInterval;
      if (tsallisConfInterval)
	delete tsallisConfInterval;
      
    }//End Loop Over Rapidity Bins

  }//End Loop Over Centrality Bins
  
  
  //*********************************************************
  //SUMMARY PLOTS
  //*********************************************************
  
  //Create MultiGraphs for the dNdy and chi2
  TMultiGraph *multidNdy = new TMultiGraph();
  TMultiGraph *multiChi2 = new TMultiGraph();
  std::vector <double> chi2Vec(0);
  
  multidNdy->SetTitle(Form("dNdy of %s All Centralities;y_{%s};dN/dy",
			   particleInfo->GetParticleSymbol(pid,charge).Data(),
			   particleInfo->GetParticleSymbol(pid,charge).Data()));
  multiChi2->SetTitle(Form("#chi^{2}/NDF of %s All Centralities;y_{%s};#chi^{2}/NDF",
			   particleInfo->GetParticleSymbol(pid,charge).Data(),
			   particleInfo->GetParticleSymbol(pid,charge).Data()));
  
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    
    if (!dNdyGraph.at(iCentBin))
      continue;
    if (dNdyGraph.at(iCentBin)->GetN() == 0)
      continue;
    
    for (int iPoint=0; iPoint<chi2Graph.at(iCentBin)->GetN(); iPoint++)
      chi2Vec.push_back(chi2Graph.at(iCentBin)->GetY()[iPoint]);
    
    
    multidNdy->Add(dNdyGraph.at(iCentBin));
    multiChi2->Add(chi2Graph.at(iCentBin));
    
  }  
  
  Double_t avgChi2Val = TMath::Mean(chi2Vec.size(),&chi2Vec.at(0));
  Double_t rmsChi2Val = TMath::RMS(chi2Vec.size(),&chi2Vec.at(0));
  
  TF1 *avgLine = new TF1("avgLine","pol0",-2,2);
  TF1 *rmsLineLow = new TF1("rmsLineLow","pol0",-2,2);
  TF1 *rmsLineHigh = new TF1("rmsLineHigh","pol0",-2,2);
  
  if (draw){
    avgLine->SetLineWidth(3);
    avgLine->SetLineStyle(1);
    avgLine->SetLineColor(kBlack);

    rmsLineLow->SetLineWidth(3);
    rmsLineLow->SetLineStyle(9);
    rmsLineLow->SetLineColor(kBlack);

    rmsLineHigh->SetLineWidth(3);
    rmsLineHigh->SetLineStyle(9);
    rmsLineHigh->SetLineColor(kBlack);

    avgLine->FixParameter(0,avgChi2Val);
    rmsLineLow->FixParameter(0,avgChi2Val-rmsChi2Val);
    rmsLineHigh->FixParameter(0,avgChi2Val+rmsChi2Val);

    TCanvas *canvas1 = new TCanvas("canvas1","canvas1",20,20,1200,600);
    canvas1->Divide(2,1);
    canvas1->cd(1);
    multidNdy->Draw("APZ");
    canvas1->cd(2);
    multiChi2->Draw("APZ");
    
    avgLine->Draw("SAME");
    rmsLineLow->Draw("SAME");
    rmsLineHigh->Draw("SAME");
  }

  TF1 *gaus = new TF1("g","gaus(0)",-1,1);
  gaus->SetParameters(100,0.0,1.0);
  gaus->FixParameter(1,0.0);
  dNdyGraph.at(8)->Fit(gaus,"EX0");


  //*********************************************************
  //SAVE
  //*********************************************************
  spectraFile->cd();
  if (!gDirectory->GetKey(spectraClassDir.Data()))
    spectraFile->mkdir(spectraClassDir.Data());
  if (!gDirectory->GetKey(spectrumDir.Data()))
    spectraFile->mkdir(spectrumDir.Data());
  if (!gDirectory->GetKey(dNdyDir.Data()))
    spectraFile->mkdir(dNdyDir.Data());
  if (!gDirectory->GetKey(spectrumFitDir.Data()))
    spectraFile->mkdir(spectrumFitDir.Data());
  
  for (unsigned int iCentBin=0; iCentBin<nCentBins; iCentBin++){
    spectraFile->cd();
    spectraFile->cd(dNdyDir.Data());
    if (dNdyGraph.at(iCentBin)->GetN() > 0)
      dNdyGraph.at(iCentBin)->Write(dNdyGraph.at(iCentBin)->GetName(),TObject::kOverwrite);
    spectraFile->cd();
    
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){

      SpectraClass *spectrum = spectra.at(iCentBin).at(yIndex);
      TF1 *spectrumFit = spectraFit.at(iCentBin).at(yIndex);
      
      if (SkipSpectrum(spectrum,iCentBin,yIndex,userCentBin,userRapidity))
	continue;
      
      spectrum->SetSpectraFile(spectraFile);
      spectrum->SetSpectraDir(spectrumDir);
      spectrum->SetSpectraClassDir(spectraClassDir);
      spectrum->SetSpectraFitDir(spectrumFitDir);

      //Do not overwrite the spectra,
      //Do overwrite the SpectraClass Object
      spectrum->WriteSpectrum(false,true);
 
      
      }//End Loop Over Rapidity Bins      
    
  }//End Loop Over Centrality Bins

  //**********************************************************
  //PRINT SPECTRUM SUMMARY
  //**********************************************************
  for (unsigned int iCentBin=0; iCentBin < nCentBins; iCentBin++){
    
    //Loop Over Rapidity Bins
    for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      double rapidity = GetRapidityRangeCenter(yIndex);
      SpectraClass *spectrum = spectra.at(iCentBin).at(yIndex);
      
      if (SkipSpectrum(spectrum,iCentBin,yIndex,userCentBin,userRapidity))
	continue;

      if (spectrum->GetN() < 1)
	continue;
      
      spectrum->PrintSummary();

    }//End Loop Over Rapidity Bins

  }//End Loop Over Centrality Bins
  
}//End Main
