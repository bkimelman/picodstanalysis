#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TLatex.h>
#include <THistPainter.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TVector3.h>
#include <TMath.h>

//after- verify that you need all these classes
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDstReader.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDst.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoEvent.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoTrack.h" 
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoBTofPidTraits.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

using namespace std;

//_____MAIN____________________
void trackQAmaker(TString inputDataFile, TString outputFile, Float_t yCM, Bool_t trackCuts = false, Bool_t RatCut = false, Int_t nEvents = -1){
//This function takes your input data file and produces an output file with vertex qa plots
//with no vertex cuts or with vertex-level cuts. Note that no track qa plots are made 
//you must use trackQAmaker.cxx for track qa. The purpose of this function is to allow 
//the user to optimize vertex cuts.

StPicoDstReader* picoReader = new StPicoDstReader(inputDataFile);
 picoReader->Init();
 if(trackCuts == false){
   //   picoReader->SetStatus("*",0);
   picoReader->SetStatus("Event",1);
   picoReader->SetStatus("Track",1);                                          
   picoReader->SetStatus("BTofPidTraits",1);
   //   picoReader->SetStatus("
 }

 if( !picoReader->chain() ) {
   std::cout << "No chain has been found." << std::endl;
 }
 Long64_t eventsInTree = picoReader->tree()->GetEntries();
 std::cout << "eventsInTree: "  << eventsInTree << std::endl;
 Long64_t events2read = picoReader->chain()->GetEntries();
 cout << "events2read: " << events2read << endl;

 TFile *outFile  = new TFile(outputFile,"RECREATE");


 TH1D *nEventsHist = new TH1D("nEventsHist","Number of events",2,-0.5,1.5);
 TH1D *bbcERate = new TH1D("bbcERate","BBC East Rate",501,-0.5,1000000.5);
 TH1D *nGlobalTracks = new TH1D("nGlobalTracks","Number of Global Tracks",1001,-0.5,1000.5);
 TH1D *nPrimaryTracksFlag = new TH1D("nPrimaryTracksFlag","Number of Primary Tracks Flagged",1001,-0.5,1000.5);
 TH1D *nPrimaryTracks = new TH1D("nPrimaryTracks","Number of Primary Tracks",1001,-0.5,1000.5);
 TH1D *nPions = new TH1D("nPions","Number of Pions",501,-0.5,500.5);

 TH2D *etaZP = new TH2D("etaZP","Primary V_{Z} vs #eta",500,-100,100,500,-2,2);
 TH2D *etaZPFlag = new TH2D("etaZPFlag","Primary Flag V_{Z} vs #eta",500,-100,100,500,-2,2);

 TH2D *etaNP = new TH2D("etaNP","Number of Primary Tracks vs #eta",500,0,1000,500,-2,2);
 TH2D *etaNPFlag = new TH2D("etaNPFlag","Number of Flagged Primary Tracks vs #eta",500,0,1000,500,-2,2);
 
 TH2D *dcaEvt = new TH2D("dcaEvt","Signed DCA vs Event Number",100001,-0.5,1000000.5,125,-2.5,2.5);

 TH2D *etaphiHist = new TH2D("etaphiHist","#phi vs #eta",500,-2.,2.,500,-4,4);
 TH2D *etaphiPlusHist = new TH2D("etaphiPlusHist","#phi vs #eta, q>0",500,-2.,2.,500,-4,4);
 TH2D *etaphiMinusHist = new TH2D("etaphiMinusHist","#phi vs #eta, q<0",500,-2.,2.,500,-4,4);
 TH2D *etaphi_low_dEdx_Hist = new TH2D("etaphi_low_dEdx_Hist","#phi vs #eta",500,-2.,2.,500,-4,4);
 TH2D *etaphi_norm_dEdx_Hist = new TH2D("etaphi_norm_dEdx_Hist","#phi vs #eta",500,-2.,2.,500,-4,4);
 //TH2D *etaphiHistTOF = new TH2D("etaphiHistTOF","#phi vs #eta",500,-2.,2.,500,-4,4);
 TH2D *pTphiHist = new TH2D("pTphiHist","#phi vs p_{T}",500,0,5,500,-4,4);
 TH2D *pTetaHist = new TH2D("pTetaHist","#eta vs p_{T}",500,0,5,500,-2.,2.);
 TH2D *pTeta_low_dEdx_Hist = new TH2D("pTeta_low_dEdx_Hist","#eta vs p_{T}",500,0,5.,500,-2,2);
 TH2D *pTeta_norm_dEdx_Hist = new TH2D("pTeta_norm_dEdx_Hist","#eta vs p_{T}",500,0,5,500,-2,2);
 TH2D *dEdxHist   = new TH2D("dEdxHist","Energy Loss in TPC",500,-6,6,500,0,100);
 TH2D *dEdxHistZoom   = new TH2D("dEdxHistZoom","Energy Loss in TPC",1000,-2,2,1000,0,10);
 TH2D *dEdxHistZoomPos   = new TH2D("dEdxHistZoomPos","Energy Loss in TPC",1000,0,4,1000,0,10);
 TH1D *dEdxSliceEta = new TH1D("dEdxSliceEta","Log of Energy Loss in TPC 0.45<p<5GeV/c",200,0,1.5);
 TH1D *dEdxSliceY = new TH1D("dEdxSliceY","Log of Energy Loss in TPC 0.45<p<5GeV/c",200,0,1.5);
 TH2D *dEdxEtaHist = new TH2D("dEdxEtaHist","dE/dx vs #eta",1000,0,10,1000,-2.,2.);
 TH2D *tofBeta    = new TH2D("tofBeta","TOF 1/#beta",500,-6,6,500,0,5);
 TH2D *tofBetaZoom    = new TH2D("tofBetaZoom","TOF 1/#beta",300,-3,3,200,0.6,2);
 TH1D *hdca = new TH1D("hdca","dca",100,0,10);
 TH1D *hDcaZP = new TH1D("hDcaZP","Primary Track DCA_{Z}",100,-10,10);
 TH1D *hDcaZPFlag = new TH1D("hDcaZPFlag","Flagged Primary Track DCA_{Z}",100,-10,10);
 TH2D *hdcaEta = new TH2D("hdcaEta","dca vs eta",500,-2.,2.,100,0,10);
 TH2D *nRatVsEta    = new TH2D("nRatVsEta","nHitsRatio vs. #eta",500,-2.,2.,50,0.0,1.05);
 TH1D *hnHits       = new TH1D("hnHits","nHits",81,-0.5,80.5);
 TH1D *hnHitsFit    = new TH1D("hnHitsFit","nHits Fit",81,-0.5,80.5);
 TH1D *hnHitsMax   = new TH1D("hnHitsMax","nHits Max",81,-0.5,80.5);
 TH1D *hnHitsRat    = new TH1D("hnHitsRat","nHitsFit/nHitsMax",101,-0.05,1.05);
 TH1D *hnHitsdEdx   = new TH1D("hnHitsdEdx","nHitsdEdx",81,-0.5,80.5);
 TH2D *pimAccept    = new TH2D("pimAccept","#pi^{-} Acceptance",500,-4.3,0,500,0,2.5);
 TH2D *pipAccept    = new TH2D("pipAccept","#pi^{+} Acceptance",500,-2,2,500,0,2.5);
 TH2D *proAccept    = new TH2D("proAccept","Proton Acceptance",500,-4.3,0,500,0,4);
 TH2D *proAcceptExc    = new TH2D("proAcceptExc","Proton Acceptance with Pion Exclusion",500,-4.3,0,500,0,4);
 TH2D *pimAcceptYCM = new TH2D("pimAcceptYCM","#pi^{-} Acceptance",500,-2.2,2.2,500,0,2.5);
 TH2D *proAcceptYCM = new TH2D("proAcceptYCM","Proton Acceptance",500,-2.2,2.2,500,0,4);
 TH2D *proAcceptYCMExc = new TH2D("proAcceptYCMExc","Proton Acceptance with Pion Exclusion",500,-2.2,2.2,500,0,4);

 TH1D *nCentEvents = new TH1D("nCentEvents","Number of Central events",2,-0.5,1.5);
 TH1D *nMidPart = new TH1D("nMidPart","Number of Midrapidity particles",3,-0.5,2.5);

 //TH2D *pimAcceptTOF = new TH2D("pimAcceptTOF","#pi^{-} Acceptance TOF",500,-2,0,500,0,5);
//TH2D *pipAcceptTOF = new TH2D("pipAcceptTOF","#pi^{+} Acceptance TOF",500,-2,0,500,0,5);
//TH2D *proAcceptTOF = new TH2D("proAcceptTOF","Proton Acceptance TOF",500,-2,0,500,0,5);


/*//rapidity binning
TH1D *pTbin1       = new TH1D("pTbin1","p_{T} for Protons in -0.5 < y < 0",500,0,4);
TH1D *pTbin2       = new TH1D("pTbin2","p_{T} for Protons in -1.0 < y < -0.5",500,0,4);
TH1D *pTbin3       = new TH1D("pTbin3","p_{T} for Protons in -1.5 < y < -1.0",500,0,4);
TH1D *pTbin4       = new TH1D("pTbin4","p_{T} for Protons in -1.9 < y < -1.5",500,0,4);

TH1D *mTbin1       = new TH1D("mTbin1","m_{T} for Protons in -0.5 < y < 0",500,0,4);
TH1D *mTbin2       = new TH1D("mTbin2","m_{T} for Protons in -1.0 < y < -0.5",500,0,4);
TH1D *mTbin3       = new TH1D("mTbin3","m_{T} for Protons in -1.5 < y < -1.0",500,0,4);
TH1D *mTbin4       = new TH1D("mTbin4","m_{T} for Protons in -1.9 < y < -1.5",500,0,4);

TH2D *hdEdx1 = new TH2D("hdEdx1","dE/dx -0.3 < #eta < 0",500,-6,6,500,0,10);
TH2D *hdEdx2 = new TH2D("hdEdx2","dE/dx -0.6 < #eta < -0.3",500,-6,6,500,0,10);
TH2D *hdEdx3 = new TH2D("hdEdx3","dE/dx -0.9 < #eta < -0.6",500,-6,6,500,0,10);
TH2D *hdEdx4 = new TH2D("hdEdx4","dE/dx -1.2 < #eta < -0.9",500,-6,6,500,0,10);
TH2D *hdEdx5 = new TH2D("hdEdx5","dE/dx -1.5 < #eta < -1.2",500,-6,6,500,0,10);
TH2D *hdEdx6 = new TH2D("hdEdx6","dE/dx -1.8 < #eta < -1.5",500,-6,6,500,0,10);

TH1D *hnHitsRat1 = new TH1D("hnHitsRat1","nHitsFit/nHitsMax -0.3 < #eta < 0",101,-0.05,1.5);
TH1D *hnHitsRat2 = new TH1D("hnHitsRat2","nHitsFit/nHitsMax -0.6 < #eta < -0.3",101,-0.05,1.5);
TH1D *hnHitsRat3 = new TH1D("hnHitsRat3","nHitsFit/nHitsMax -0.9 < #eta < -0.6",101,-0.05,1.5);
TH1D *hnHitsRat4 = new TH1D("hnHitsRat4","nHitsFit/nHitsMax -1.2 < #eta < -0.9",101,-0.05,1.5);
TH1D *hnHitsRat5 = new TH1D("hnHitsRat5","nHitsFit/nHitsMax -1.5 < #eta < -1.2",101,-0.05,1.5);
TH1D *hnHitsRat6 = new TH1D("hnHitsRat6","nHitsFit/nHitsMax -1.8 < #eta < -1.5",101,-0.05,1.5);

TH1D *hnHitsFit1 = new TH1D("hnHitsFit1","nHitsFit -0.3 < #eta < 0",81,-0.5,79.5);
TH1D *hnHitsFit2 = new TH1D("hnHitsFit2","nHitsFit -0.6 < #eta < -0.3",81,-0.5,79.5);
TH1D *hnHitsFit3 = new TH1D("hnHitsFit3","nHitsFit -0.9 < #eta < -0.6",81,-0.5,79.5);
TH1D *hnHitsFit4 = new TH1D("hnHitsFit4","nHitsFit -1.2 < #eta < -0.9",81,-0.5,79.5);
TH1D *hnHitsFit5 = new TH1D("hnHitsFit5","nHitsFit -1.5 < #eta < -1.2",81,-0.5,79.5);
TH1D *hnHitsFit6 = new TH1D("hnHitsFit6","nHitsFit -1.8 < #eta < -1.5",81,-0.5,79.5);

TH1D *hnHitsMax1 = new TH1D("hnHitsMax1","nHitsMax -0.3 < #eta < 0",81,-0.5,79.5);
TH1D *hnHitsMax2 = new TH1D("hnHitsMax2","nHitsMax -0.6 < #eta < -0.3",81,-0.5,79.5);
TH1D *hnHitsMax3 = new TH1D("hnHitsMax3","nHitsMax -0.9 < #eta < -0.6",81,-0.5,79.5);
TH1D *hnHitsMax4 = new TH1D("hnHitsMax4","nHitsMax -1.2 < #eta < -0.9",81,-0.5,79.5);
TH1D *hnHitsMax5 = new TH1D("hnHitsMax5","nHitsMax -1.5 < #eta < -1.2",81,-0.5,79.5);
TH1D *hnHitsMax6 = new TH1D("hnHitsMax6","nHitsMax -1.8 < #eta < -1.5",81,-0.5,79.5);
*/

Int_t pvEntries;
double mPion = 0.13957018;
double mProton = 0.938272046;

 int eventCount = 0;

 int modNum=1;

for(Int_t iEvent=0; iEvent<events2read; iEvent++){//loop over triggers
  
  int nG = 0;
  int nPFlag = 0;
  int nP = 0;

  int nPi=0;
  
  Bool_t readEvent = picoReader->ReadPicoEvent(iEvent);
  if( !readEvent ) {
    std::cout << "Something went wrong, no events" << endl;
    break;
  }

  StPicoDst *dst = picoReader->picoDst();

  if( iEvent % modNum == 0 ){
    cout << "TrackQA::trackCuts(" << trackCuts << ")::RatCut(" << RatCut << ") - Working on event " << iEvent << endl;
    if (iEvent == 10*modNum) modNum *= 10;
  }

  //access data and fill trigger level histograms                                      
  StPicoEvent *event = dst->event();
  if( !event ) {
    cout << "Something went wrong, event is empty" << endl;
    break;
  }
  if (!IsGoodEvent(event)) continue;
  if (!IsGoodVertex(event)) continue;

  bbcERate->Fill(event->bbcEastRate());

  Double_t evNum = event->eventId();
        
  TVector3 primVertex = event->primaryVertex();
  
  //  eventCount++;
  
  nEventsHist->Fill(1);
  Int_t centVar = GetCentralityVariable(dst,event);
  Int_t centBin = GetCentralityBin(centVar);
  if (centBin == 0) nCentEvents->Fill(1);
  Int_t nTracks = dst->numberOfTracks();
  nG = nTracks;
  // Loop to get numbers of primary tracks
  for(Int_t iTrk = 0; iTrk<nTracks;iTrk++){
    StPicoTrack *picoTrack = dst->track(iTrk);

    float dEdx_err = picoTrack->dEdxError();

    if (picoTrack->pMom().Mag() > 0 ) nPFlag++;

    if (trackCuts) {
      if (!IsGoodTrack(picoTrack,primVertex,RatCut)) continue;
      //      if (dEdx_err<0.04 || dEdx_err>0.15) continue;
    }

    nP++;

  }
 
  nGlobalTracks->Fill(nG);
  nPrimaryTracksFlag->Fill(nPFlag);
  nPrimaryTracks->Fill(nP);

  for(Int_t iTrk = 0; iTrk<nTracks;iTrk++){//loop over tracks
    StPicoTrack *picoTrack = dst->track(iTrk);

    if (picoTrack->pMom().Mag() > 0 ){
      double etaPFlag = picoTrack->gMom().Eta();
      etaZPFlag->Fill(primVertex.Z(),etaPFlag);
      etaNPFlag->Fill(nP,etaPFlag);
      hDcaZPFlag->Fill(picoTrack->gDCA(primVertex).Z());
    }
    TVector3 momentum = picoTrack->gMom();
    if (trackCuts) {
      momentum = picoTrack->pMom();
      if (!IsGoodTrack(picoTrack,primVertex,RatCut)) continue;
    }


    //hits variables
    Double_t nHitsFit = picoTrack->nHitsFit();
    Double_t nHitsMax = picoTrack->nHitsMax();
    Double_t nHitsRatio = nHitsFit/nHitsMax;
    
    hnHits->Fill(picoTrack->nHits());
    hnHitsFit->Fill(nHitsFit);
    hnHitsMax->Fill(nHitsMax);
    hnHitsRat->Fill(nHitsRatio);
    hnHitsdEdx->Fill(picoTrack->nHitsDedx());         
    
    TVector3 gdca = picoTrack->gDCA(primVertex);
    
    Double_t dcaD      = gdca.Mag();
    Double_t eta       = momentum.Eta();
    Double_t phi       = momentum.Phi();
    
    TVector3 orig = picoTrack->origin();
    Double_t psDCA = ((orig.X() - primVertex.X())*momentum.Y()-(orig.Y() - primVertex.Y())*momentum.X())/sqrt(pow(momentum.X(),2)+pow(momentum.Y(),2));

    dcaEvt->Fill(evNum,psDCA);

        nRatVsEta->Fill(eta,nHitsRatio);
        hdca->Fill(dcaD);
	hDcaZP->Fill(gdca.Z());
        hdcaEta->Fill(eta,dcaD);

	etaZP->Fill(primVertex.Z(),eta);
	etaNP->Fill(nP,eta);

        //pid variables and acceptance plots 
        Double_t q = picoTrack->charge();
        Double_t pT = momentum.Pt();
        Double_t p = momentum.Mag();
        Double_t dEdx = picoTrack->dEdx();
        Double_t nSigmaPi = picoTrack->nSigmaPion();
        Double_t nSigmaPro = picoTrack->nSigmaProton();
        Double_t yPi = TMath::ATanH(momentum.Z() / sqrt(mPion*mPion + p*p));
        Double_t yPro = TMath::ATanH(momentum.Z() / sqrt(mProton*mProton + p*p));
        Double_t mTpi = sqrt(mPion*mPion + pT*pT);
        Double_t mTpro = sqrt(mProton*mProton + pT*pT);


        if(centBin == 0){
          if(q<0 && abs(nSigmaPi) < 2 && abs(yPi) < 0.5) nMidPart->Fill(0);
          if(q>0 && abs(nSigmaPro) < 2 && abs(yPro) < 0.5 && pT <= 0.4) nMidPart->Fill(1);
          if(q>0 && abs(nSigmaPro) < 2 && abs(yPro) < 0.5 && pT > 0.4) nMidPart->Fill(2);
        }

	if(abs(nSigmaPi) < 2) nPi++;

        if(q < 0 && abs(nSigmaPi) < 2){
					pimAccept->Fill(yPi,pT);
					pimAcceptYCM->Fill(-(yPi-yCM),pT);
					//	if (trackCuts && IsGoodTofTrack(track)) pimAcceptTOF->Fill(yPi, mTpi-mPion);
				}
        if(q > 0 && abs(nSigmaPi) < 2 && nSigmaPro < -1){
					 pipAccept->Fill(yPi,pT);
					 //					 if (trackCuts && IsGoodTofTrack(track)) pipAcceptTOF->Fill(yPi, mTpi-mPion);
        }
        if(q > 0 && abs(nSigmaPro) < 2){
          proAccept->Fill(yPro,pT);
          proAcceptYCM->Fill(-(yPro-yCM),pT);
	  if (nSigmaPi > 1){
	    proAcceptExc->Fill(yPro,pT);
	    proAcceptYCMExc->Fill(-(yPro-yCM),pT);
	  }
	  //          if (trackCuts && IsGoodTofTrack(track)) proAcceptTOF->Fill(yPro, mTpro-mProton);
/*          if(-0.5 < yPro && yPro < 0){
            pTbin1->Fill(pT,1./pT);
            mTbin1->Fill(mTpro,1./mTpro);
          }
          if(-1.0 < yPro && yPro < -0.5){
            pTbin2->Fill(pT,1./pT);
            mTbin2->Fill(mTpro,1./mTpro);
          }
          if(-1.5 < yPro && yPro < -1.0){
            pTbin3->Fill(pT,1./pT);
            mTbin3->Fill(mTpro,1./mTpro);
          }
          if(-1.9 < yPro && yPro < -1.5){
            pTbin4->Fill(pT,1./pT);
            mTbin4->Fill(mTpro,1./mTpro);
	    }*/
        }
        etaphiHist->Fill(eta,phi);
	if(q>0) etaphiPlusHist->Fill(eta,phi);
	if(q<0) etaphiMinusHist->Fill(eta,phi);
	if(dEdx<1){
	  etaphi_low_dEdx_Hist->Fill(eta,phi);
	  pTeta_low_dEdx_Hist->Fill(pT,eta);
	}
	else{
	  etaphi_norm_dEdx_Hist->Fill(eta,phi);
	  pTeta_norm_dEdx_Hist->Fill(pT,eta);
	}
	pTphiHist->Fill(pT,phi);
	pTetaHist->Fill(pT,eta);
	//        if(track->GetTofMatchFlag() > 0) etaphiHistTOF->Fill(track->GetEta(),track->GetPhi());
        dEdxHist->Fill(q*p,dEdx);
        dEdxHistZoom->Fill(q*p,dEdx);
        dEdxHistZoomPos->Fill(q*p,dEdx);

	dEdxEtaHist->Fill(dEdx,eta);
	
	if (yCM < -1.5){
	  if (p>0.45 && p<0.5 && eta<-1.5){
	    dEdxSliceEta->Fill(TMath::Log10(dEdx));
	  }
	  
	}
	else{
	  if (p>0.45 && p<0.5 && eta>-0.25 && eta<0.25){
	    dEdxSliceEta->Fill(TMath::Log10(dEdx));
	  }
	}
	if (p>0.45 && p<0.5 && -(yPi-yCM)>-0.25 && -(yPi-yCM)<0.25){
	  dEdxSliceY->Fill(TMath::Log10(dEdx));
	}

	Int_t idx = picoTrack->bTofPidTraitsIndex();
	if (idx!=-1){
	  StPicoBTofPidTraits *traits = dst->btofPidTraits(idx);
	  Float_t trackBeta = traits->btofBeta();
	  tofBeta->Fill(q*p,1.0/trackBeta);
	  tofBetaZoom->Fill(q*p,1.0/trackBeta);
	}
	        
        //rapidity binning 
	/*        if(eta < 0 && eta > -0.3){
          hdEdx1->Fill(q*p,dEdx);
          hnHitsRat1->Fill(nHitsRatio);
          hnHitsFit1->Fill(nHitsFit);
          hnHitsMax1->Fill(nHitsMax);
        }
        if(eta < -0.3 && eta > -0.6){
          hdEdx2->Fill(q*p,dEdx);
          hnHitsRat2->Fill(nHitsRatio);
          hnHitsFit2->Fill(nHitsFit);
          hnHitsMax2->Fill(nHitsMax);
        }
        if(eta < -0.6 && eta > -0.9){
          hdEdx3->Fill(p*q,dEdx);
          hnHitsRat3->Fill(nHitsRatio);
          hnHitsFit3->Fill(nHitsFit);
          hnHitsMax3->Fill(nHitsMax);
        }
        if(eta < -0.9 && eta > -1.2){
          hdEdx4->Fill(p*q,dEdx);
          hnHitsRat4->Fill(nHitsRatio);
          hnHitsFit4->Fill(nHitsFit);
          hnHitsMax4->Fill(nHitsMax);
        }
        if(eta < -1.2 && eta > -1.5){
         hdEdx5->Fill(p*q,dEdx);
         hnHitsRat5->Fill(nHitsRatio);
         hnHitsFit5->Fill(nHitsFit);
         hnHitsMax5->Fill(nHitsMax);
        }
        if(eta < -1.5 && eta > -1.8){
          hdEdx6->Fill(p*q,dEdx);
          hnHitsRat6->Fill(nHitsRatio);
          hnHitsFit6->Fill(nHitsFit);
          hnHitsMax6->Fill(nHitsMax);
        }
        */
  }//end loop over tracks

  nPions->Fill(nPi);

 }//end of loop over triggers

 outFile->Write();
 outFile->Close();
}//end of function
