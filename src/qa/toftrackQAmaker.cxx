#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TLatex.h>
#include <THistPainter.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TVector3.h>
#include <TMath.h>

//after- verify that you need all these classes
#include "../submodules/datacollectorreaderlibs/TrackInfo/TrackInfo.h"
#include "../submodules/datacollectorreaderlibs/PrimaryVertexInfo/PrimaryVertexInfo.h"
#include "../submodules/datacollectorreaderlibs/EventInfo/EventInfo.h"
#include "../submodules/datacollectorreaderlibs/DavisDstReader/DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

using namespace std;

//_____MAIN____________________
void toftrackQAmaker(TString inputDataFile, TString outputFile, Bool_t toftrackCuts = false, Int_t nEvents = -1){
//This function takes your input data file and produces an output file with vertex qa plots
//with no vertex cuts or with vertex-level cuts. Note that no track qa plots are made 
//you must use trackQAmaker.cxx for track qa. The purpose of this function is to allow 
//the user to optimize vertex cuts.

DavisDstReader davisDst(inputDataFile);

TFile *outFile  = new TFile(outputFile,"RECREATE");

TrackInfo *track = NULL;
PrimaryVertexInfo *primaryVertex = NULL;
EventInfo *event = NULL;

//Initializing PRE-cut track histograms
//----------------------------------------------------------------------------------
TH2D *etaphiHistTOFNoCuts = new TH2D("etaphiHistTOFNoCuts","#phi vs #eta",500,-2.,0.5,500,-4,4);
TH2D *pimAcceptTOFNoCuts   = new TH2D("pimAcceptTOFNoCuts","#pi^{-} Acceptance TOF",500,-2,0,500,0,5);
TH2D *pipAcceptTOFNoCuts   = new TH2D("pipAcceptTOFNoCuts","#pi^{+} Acceptance TOF",500,-2,0,500,0,5);
TH2D *proAcceptTOFNoCuts   = new TH2D("proAcceptTOFNoCuts","Proton Acceptance TOF",500,-2,0,500,0,5);

TH2D *tofBetaNoCuts    = new TH2D("tofBetaNoCuts","TOF 1/#beta",500,-6,6,500,0,5);
TH2D *tofBetaZoomNoCuts    = new TH2D("tofBetaZoomNoCuts","TOF 1/#beta",300,-3,3,200,0.6,2);

TH1D *htofmatchNoCuts = new TH1D("htofmatchNoCuts","tofmatch",5,0,5);
TH1D *hytofNoCuts = new TH1D("hytofNoCuts","y Local TOF",200,-10,10);
TH1D *hztofNoCuts = new TH1D("hztofNoCuts","z Local TOF",200,-10,10);
TH2D *h2yztofNoCuts = new TH2D("h2yztofNoCuts","Local TOF y vs z",200,-10,10,200,-10,10);


//eta binned tof pads histograms
TH2D *h2yztofNoCuts6 = new TH2D("h2yztofNoCuts6","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts7 = new TH2D("h2yztofNoCuts7","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts8 = new TH2D("h2yztofNoCuts8","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts9 = new TH2D("h2yztofNoCuts9","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts10 = new TH2D("h2yztofNoCuts10","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts11 = new TH2D("h2yztofNoCuts11","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts12 = new TH2D("h2yztofNoCuts12","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts13 = new TH2D("h2yztofNoCuts13","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts14 = new TH2D("h2yztofNoCuts14","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts15 = new TH2D("h2yztofNoCuts15","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts16 = new TH2D("h2yztofNoCuts16","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts17 = new TH2D("h2yztofNoCuts17","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts18 = new TH2D("h2yztofNoCuts18","Local TOF y vs z",200,-10,10,200,-10,10);
TH2D *h2yztofNoCuts19 = new TH2D("h2yztofNoCuts19","Local TOF y vs z",200,-10,10,200,-10,10);


//post-cut histos
TH1D *htofmatch, *hytof, *hztof;
TH2D *etaphiHistTOF, *pimAcceptTOF, *pipAcceptTOF, *proAcceptTOF, *tofBeta, *tofBetaZoom, *h2yztof;

//Initializing POST-cut track histograms
//----------------------------------------------------------------------------------
if (toftrackCuts){
	etaphiHistTOF = new TH2D("etaphiHistTOF","#phi vs #eta",500,-2.,0.5,500,-4,4);
	pimAcceptTOF  = new TH2D("pimAcceptTOF","#pi^{-} Acceptance TOF",500,-2,0,500,0,5);
	pipAcceptTOF  = new TH2D("pipAcceptTOF","#pi^{+} Acceptance TOF",500,-2,0,500,0,5);
	proAcceptTOF  = new TH2D("proAcceptTOF","Proton Acceptance TOF",500,-2,0,500,0,5);

	tofBeta       = new TH2D("tofBeta","TOF 1/#beta",500,-6,6,500,0,5);
	tofBetaZoom   = new TH2D("tofBetaZoom","TOF 1/#beta",300,-3,3,200,0.6,2);

	htofmatch 	  = new TH1D("htofmatch","tofmatch",5,0,5);
	hytof         = new TH1D("hytof","y Local TOF",200,-10,10);
	hztof         = new TH1D("hztof","z Local TOF",200,-10,10);
	h2yztof       = new TH2D("h2yztof","Local TOF y vs z",200,-10,10,200,-10,10);
}


Int_t pvEntries;
double mPion = 0.13957018;
double mProton = 0.938272046;
Double_t entries;
if (nEvents > 0) entries = nEvents;
else entries = davisDst.GetEntries();
for(Int_t i=0; i<entries; i++){//loop over triggers
	event = davisDst.GetEntry(i);
	if (!IsGoodEvent(event)) continue;
	pvEntries = event->GetNPrimaryVertices();
  for (Int_t j=0; j<pvEntries; j++){//loop over vertices
	  primaryVertex = event->GetPrimaryVertex(j);
    if (!IsGoodVertex(primaryVertex)) continue;
		Int_t nPrimaryTracks = primaryVertex->GetNPrimaryTracks();
	    for(Int_t k = 0; k<nPrimaryTracks;k++){//loop over tracks
			  track = primaryVertex->GetPrimaryTrack(k);
        if (!IsGoodTrack(track)) continue;

				//require a minimum of one potential tof hit match and subzero velocities
        if (track->GetTofMatchFlag() < 1) continue;
				if (1.0/track->GetTofBeta() < 0.0) continue;

        etaphiHistTOFNoCuts->Fill(track->GetEta(),track->GetPhi());

        //pid variables and acceptance plots 
				Double_t eta = track->GetEta();
        Double_t q = track->GetCharge();
        Double_t pT = track->GetPt();
        Double_t p = sqrt(pow(pT,2) + pow(track->GetPz(),2));
        Double_t yPi = TMath::ATanH(track->GetPz() / sqrt(mPion*mPion + p*p));
        Double_t yPro = TMath::ATanH(track->GetPz() / sqrt(mProton*mProton + p*p));
        Double_t mTpi = sqrt(mPion*mPion + pT*pT);
        Double_t mTpro = sqrt(mProton*mProton + pT*pT);
				Double_t nSigmaPi = track->GetNSigmaPion();
				Double_t nSigmaPro = track->GetNSigmaProton();


        //Fill tof histograms
        etaphiHistTOFNoCuts->Fill(track->GetEta(),track->GetPhi());
        tofBetaNoCuts->Fill(q*p,1.0/track->GetTofBeta());
        tofBetaZoomNoCuts->Fill(q*p,1.0/track->GetTofBeta());
        htofmatchNoCuts->Fill(track->GetTofMatchFlag());
				hytofNoCuts->Fill(track->GetTofYLocal());
				hztofNoCuts->Fill(track->GetTofZLocal());
				h2yztofNoCuts->Fill(track->GetTofZLocal(),track->GetTofYLocal());

				//Fill eta-binned histos
        if (eta > -1.45 && eta < -1.35) h2yztofNoCuts6->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -1.35 && eta < -1.25) h2yztofNoCuts7->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -1.25 && eta < -1.15) h2yztofNoCuts8->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -1.15 && eta < -1.05) h2yztofNoCuts9->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -1.05 && eta < -0.95) h2yztofNoCuts10->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.95 && eta < -0.85) h2yztofNoCuts11->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.85 && eta < -0.75) h2yztofNoCuts12->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.75 && eta < -0.65) h2yztofNoCuts13->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.65 && eta < -0.55) h2yztofNoCuts14->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.55 && eta < -0.45) h2yztofNoCuts15->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.45 && eta < -0.35) h2yztofNoCuts16->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.35 && eta < -0.25) h2yztofNoCuts17->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.25 && eta < -0.15) h2yztofNoCuts18->Fill(track->GetTofZLocal(),track->GetTofYLocal());
        if (eta > -0.15 && eta < -0.05) h2yztofNoCuts19->Fill(track->GetTofZLocal(),track->GetTofYLocal());


        if (toftrackCuts && IsGoodTofTrack(track)){

					etaphiHistTOF->Fill(track->GetEta(),track->GetPhi());
					tofBeta->Fill(q*p,1.0/track->GetTofBeta());
					tofBetaZoom->Fill(q*p,1.0/track->GetTofBeta());
					htofmatch->Fill(track->GetTofMatchFlag());
					hytof->Fill(track->GetTofYLocal());
					hztof->Fill(track->GetTofZLocal());
					h2yztof->Fill(track->GetTofZLocal(),track->GetTofYLocal());

        }

        if(q < 0 && abs(nSigmaPi) < 2){
					pimAcceptTOFNoCuts->Fill(yPi, mTpi-mPion);
          if (toftrackCuts && IsGoodTofTrack(track)) pimAcceptTOF->Fill(yPi, mTpi-mPion);
				}
        if(q > 0 && abs(nSigmaPi) < 2 && nSigmaPro < -1){
				  pipAcceptTOFNoCuts->Fill(yPi, mTpi-mPion);
          if (toftrackCuts && IsGoodTofTrack(track)) pipAcceptTOF->Fill(yPi, mTpi-mPion);
        }
        if(q > 0 && abs(nSigmaPro) < 2){
          proAcceptTOFNoCuts->Fill(yPro, mTpro-mProton);
          if (toftrackCuts && IsGoodTofTrack(track)) proAcceptTOF->Fill(yPro, mTpro-mProton);
        }

		  }//end loop over tracks
 }//end of loop over vertices
}//end of loop over triggers

outFile->Write();

}//end of function
