#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TLatex.h>
#include <THistPainter.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TVector3.h>
#include <TMath.h>

//after- verify that you need all these classes
#include "globalDefinitions.h"
#include "../submodules/datacollectorreaderlibs/TrackInfo/TrackInfo.h"
#include "../submodules/datacollectorreaderlibs/PrimaryVertexInfo/PrimaryVertexInfo.h"
#include "../submodules/datacollectorreaderlibs/EventInfo/EventInfo.h"
#include "../submodules/datacollectorreaderlibs/DavisDstReader/DavisDstReader.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

using namespace std;

//_____MAIN____________________
void pidQAmaker(TString inputDataFile, TString outputFile, Int_t nEvents = -1, Bool_t makeTOFplots=0, Int_t nRapBinPlots=nRapidityBins, Double_t xRangeLo=-1.5, Double_t xRangeHi=1.5){
//This function takes your input data file and produces an output file with pid qa plots with the same binning as the spectra
//for different particle species: at the moment pions, protons, and kaons
//***Currently just for top 5% centrality

DavisDstReader davisDst(inputDataFile);

TFile *outFile  = new TFile(outputFile,"RECREATE");

TrackInfo *track = NULL;
PrimaryVertexInfo *primaryVertex = NULL;
EventInfo *event = NULL;

std::vector<int> centCuts = GetCentralityCuts();
const int NYBINS = nRapBinPlots;

//Declare and initialize histograms
TH2D* hdEdxmTm0Pro[NYBINS];
TH2D* hdEdxmTm0Pi[NYBINS];
TH2D* hdEdxmTm0K[NYBINS];

TH2D* hInvBetamTm0Pro[NYBINS];
TH2D* hInvBetamTm0Pi[NYBINS];
TH2D* hInvBetamTm0K[NYBINS];

for (int i=0; i<NYBINS; i++){
  hdEdxmTm0Pro[i] = new TH2D(Form("dEdxMtm0HistPro_%d",i),Form("dEdxMtm0HistPro_%d",i),300,xRangeLo,xRangeHi,1000,0,10);
  hdEdxmTm0Pi[i] = new TH2D(Form("dEdxMtm0HistPi_%d",i),Form("dEdxMtm0HistPi_%d",i),300,xRangeLo,xRangeHi,1000,0,10);
  hdEdxmTm0K[i] = new TH2D(Form("dEdxMtm0HistK_%d",i),Form("dEdxMtm0HistK_%d",i),300,xRangeLo,xRangeHi,1000,0,10);

	if (!makeTOFplots) continue;

	hInvBetamTm0Pro[i] = new TH2D(Form("InvBetaMtm0HistPro_%d",i),Form("InvBetaMtm0HistPro_%d",i),300,xRangeLo,xRangeHi,500,0,5);
	hInvBetamTm0Pi[i] = new TH2D(Form("InvBetaMtm0HistPi_%d",i),Form("InvBetaMtm0HistPi_%d",i),300,xRangeLo,xRangeHi,500,0,5);
	hInvBetamTm0K[i] = new TH2D(Form("InvBetaMtm0HistK_%d",i),Form("InvBetaMtm0HistK_%d",i),300,xRangeLo,xRangeHi,500,0,5);
}


Int_t pvEntries, nPrimaryTracks, nGoodTracks;
Double_t mPion = 0.13957018;
Double_t mProton = 0.938272046;
Double_t mKaon = 0.493677;
Double_t entries, ymin, ymax;

//Check to see if user specified nEvents, otherwise run over all events
if(nEvents > 0) entries = nEvents;
else entries = davisDst.GetEntries();

//Loop over events
for(Int_t i=0; i<entries; i++){//loop over triggers
	event = davisDst.GetEntry(i);

	//Cut on good events
	if (!IsGoodEvent(event)) continue;

	//Loop over primary vertices
	pvEntries = event->GetNPrimaryVertices();
  for (Int_t j=0; j<pvEntries; j++){//loop over vertices
	  primaryVertex = event->GetPrimaryVertex(j);

		//Cut on good vertices
    if (!IsGoodVertex(primaryVertex)) continue;
    
		//Centrality selection
		nGoodTracks = GetUserCentralityVariable(primaryVertex);
		if (nGoodTracks < centCuts.at(0) || nGoodTracks > GetPileUpCut()) continue;

    //Loop over tracks
		nPrimaryTracks = primaryVertex->GetNPrimaryTracks();
		for(Int_t k = 0; k<nPrimaryTracks;k++){//track loop 
			track = primaryVertex->GetPrimaryTrack(k);

			//Cut on good tracks
			if (!IsGoodTrack(track)) continue;
			//Get track variables to be used below
			Double_t eta       = track->GetEta();
			Double_t phi       = track->GetPhi();
			Double_t q = track->GetCharge();
			Double_t pT = track->GetPt();
			Double_t p = sqrt(pow(pT,2) + pow(track->GetPz(),2));
			Double_t dEdx = track->GetdEdx(1)*1000000;
			Double_t invBeta = 1.0/track->GetTofBeta();
			Double_t yPi = TMath::ATanH(track->GetPz() / sqrt(mPion*mPion + p*p));
			Double_t yPro = TMath::ATanH(track->GetPz() / sqrt(mProton*mProton + p*p));
			Double_t yK   = TMath::ATanH(track->GetPz() / sqrt(mKaon*mKaon + p*p));
			Double_t mTpro = sqrt(mProton*mProton + pT*pT);
			Double_t mTK = sqrt(mKaon*mKaon + pT*pT);
			Double_t mTm0Pi  = sqrt(mPion*mPion + pT*pT) - mPion;
			Double_t mTm0Pro = mTpro - mProton;
			Double_t mTm0K = mTK - mKaon;

			for (Int_t ybin=0;ybin<NYBINS;ybin++){
				ymin = rapidityMin + ybin*rapidityBinWidth;   
				ymax = ymin + rapidityBinWidth;
				if (yPro > ymin && yPro < ymax) hdEdxmTm0Pro[ybin]->Fill(mTm0Pro/q, dEdx);
				if (yPi  > ymin && yPi  < ymax) hdEdxmTm0Pi[ybin]->Fill(mTm0Pi/q, dEdx);
				if (yK   > ymin && yK   < ymax) hdEdxmTm0K[ybin]->Fill(mTm0K/q, dEdx);
				
				if (!IsGoodTofTrack(track) || !makeTOFplots) continue;
				if (yPro > ymin && yPro < ymax) hInvBetamTm0Pro[ybin]->Fill(mTm0Pro/q, invBeta);
				if (yPi  > ymin && yPi  < ymax) hInvBetamTm0Pi[ybin]->Fill(mTm0Pi/q, invBeta);
				if (yK   > ymin && yK   < ymax) hInvBetamTm0K[ybin]->Fill(mTm0K/q, invBeta);

			}

	  }//end loop over tracks
 }//end of loop over vertices
}//end of loop over triggers

outFile->Write();

}//end of function
