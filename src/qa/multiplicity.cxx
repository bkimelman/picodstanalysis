#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TLatex.h>
#include <THistPainter.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TVector3.h>
#include <TMath.h>

//after- verify that you need all these classes
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDstReader.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDst.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoEvent.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoTrack.h" 
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoBTofPidTraits.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

using namespace std;

//_____MAIN____________________
void multiplicity(TString inputDataFile, TString outputFile, Float_t yCM, Bool_t trackCuts = false, Int_t nEvents = -1){
//This function takes your input data file and produces an output file with vertex qa plots
//with no vertex cuts or with vertex-level cuts. Note that no track qa plots are made 
//you must use trackQAmaker.cxx for track qa. The purpose of this function is to allow 
//the user to optimize vertex cuts.

StPicoDstReader* picoReader = new StPicoDstReader(inputDataFile);
 picoReader->Init();
 if(trackCuts == false){
   //   picoReader->SetStatus("*",0);
   picoReader->SetStatus("Event",1);
   picoReader->SetStatus("Track",1);                                          
   picoReader->SetStatus("BTofPidTraits",1);
   //   picoReader->SetStatus("
 }

 if( !picoReader->chain() ) {
   std::cout << "No chain has been found." << std::endl;
 }
 Long64_t eventsInTree = picoReader->tree()->GetEntries();
 std::cout << "eventsInTree: "  << eventsInTree << std::endl;
 Long64_t events2read = picoReader->chain()->GetEntries();


 TFile *outFile  = new TFile(outputFile,"RECREATE");

 TH1D *nPions = new TH1D("nPions","Number of Primary #pi",501,-0.5,500.5);
 TH1D *nPions_yCM = new TH1D("nPions_yCM","Number of Primary #pi with |y-y_{CM}|<0.5",501,-0.5,500.5);

 Int_t nPi;
 Int_t nPiyCM;
 Int_t pvEntries;
 double mPion = 0.13957018;
 double mProton = 0.938272046;
 for(Int_t iEvent=0; iEvent<events2read; iEvent++){//loop over triggers
   
   Bool_t readEvent = picoReader->ReadPicoEvent(iEvent);
   if( !readEvent ) {
     std::cout << "Something went wrong, no events" << endl;
     break;
   }
   
   StPicoDst *dst = picoReader->picoDst();
   
   //access data and fill trigger level histograms                                      
   StPicoEvent *event = dst->event();
   if( !event ) {
     cout << "Something went wrong, event is empty" << endl;
     break;
   }
   if (!IsGoodEvent(event)) continue;
   if (!IsGoodVertex(event)) continue;
   TVector3 primVertex = event->primaryVertex();
   
   
   Int_t nTracks = dst->numberOfTracks();
   nPi = 0;
   nPiyCM = 0;
   // Loop to get numbers of primary tracks
   for(Int_t iTrk = 0; iTrk<nTracks;iTrk++){
     StPicoTrack *picoTrack = dst->track(iTrk);

     TVector3 momentum = picoTrack->gMom();
     if (trackCuts) {
       momentum = picoTrack->pMom();
       if (!IsGoodTrack(picoTrack,primVertex)) continue;
     }
     if( abs(picoTrack->nSigmaPion()) < 3 && ( (picoTrack->charge() < 0) || (picoTrack->charge() > 0 && picoTrack->nSigmaProton() < -2) ) ){
       nPi++;
     }

     Double_t q = picoTrack->charge();
     Double_t p = momentum.Mag();
     Double_t nSigmaPi = picoTrack->nSigmaPion();
     Double_t nSigmaPro = picoTrack->nSigmaProton();
     Double_t yPi = TMath::ATanH(momentum.Z() / sqrt(mPion*mPion + p*p));
     Double_t yPro = TMath::ATanH(momentum.Z() / sqrt(mProton*mProton + p*p));
     
     if(q < 0 && abs(nSigmaPi) < 3 && abs(yPi - yCM) < 0.5){
       nPiyCM++;
     }
     if(q > 0 && abs(nSigmaPi) < 3 && nSigmaPro < -2 && abs(yPi - yCM) < 0.5){
       nPiyCM++;
     }
     
   }//end loop over tracks
   
   nPions->Fill(nPi);
   nPions_yCM->Fill(nPiyCM);
   
 }//end of loop over triggers
 
 
 outFile->Write();

}//end of function
