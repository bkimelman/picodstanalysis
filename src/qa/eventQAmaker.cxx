#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TLatex.h>
#include <THistPainter.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TVector3.h>
#include <TMath.h>

#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDstReader.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoDst.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoEvent.h"
#include "../submodules/datacollectorreaderlibs/PicoDstReader/StPicoTrack.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

using namespace std;

//_____MAIN____________________
void eventQAmaker(TString inputDataFile, TString outFileName, TString eventConfig, Bool_t eventCuts = false, Bool_t vertexCuts = false, Int_t nEvents = -1){
//This function takes your input data file and produces an output file with event qa plots 
//with no cuts or with the trigger-level cuts in your user file. Note that no vertex or
//track qa plots are made- you must use vertexQAmaker.cxx or trackQAmaker.cxx for those.
//The purpose of this function is to allow the user to optimize triggered-event cuts.
  
  //Use the StPicoDstReader to open and read the file
  StPicoDstReader* picoReader = new StPicoDstReader(inputDataFile);
  picoReader->Init();
  if(eventCuts == false){
    picoReader->SetStatus("*",0);
    picoReader->SetStatus("Event",1);
    picoReader->SetStatus("Track",1);
  }
  
  if( !picoReader->chain() ) {
    std::cout << "No chain has been found." << std::endl;
  }
  Long64_t eventsInTree = picoReader->tree()->GetEntries();
  std::cout << "eventsInTree: "  << eventsInTree << std::endl;
  Long64_t events2read = picoReader->chain()->GetEntries();
  
  cout << "events2read: " << events2read << endl;
  
  TFile *outFile  = new TFile(outFileName,"RECREATE");
  
  
  //initializing trigger level histograms
  TH1D *htrigNoCuts = new TH1D("htrigNoCuts","Triggers",3,649999.5,650002.5);
  TH1D *hRefMultNoCuts = new TH1D("hRefMultNoCuts","Reference Multiplicity",500,-0.5,499.5);
  TH1D *tofMultHistNoCuts = new TH1D("tofMultHistNoCuts","TOF Multiplicity",500,0,500);
  TH1I *hrunNumNoCuts = new TH1I("hrunNumNoCuts","Run Number",100,20055000,20059000);
  TH2D *hVtxXYNoCuts = new TH2D("hVtxXYNoCuts","hVtxXY",200,-10.,10.,200,-10.,10.);
  TH1D *hVtxZNoCuts;
  if (eventConfig == "ColliderCenter"){
    cout << "eventConfig: " << eventConfig << endl;
    hVtxZNoCuts = new TH1D("hVtxZNoCuts","hVtxZ",421,-210.5,210.5);
  }
  else if(eventConfig == "FixedTarget2015"){
    cout << "eventConfig: " << eventConfig << endl;
    hVtxZNoCuts = new TH1D("hVtxZNoCuts","hVtxZ",301,179.5,210.5);
  }
  TH2D *TOFvsMultHistNoCuts = new TH2D("TOFvsMultHistNoCuts","Number of TOF Matches vs. Multiplicity",800,0,200,400,0,100);
  TH1D *htrig, *htrigNoVtxCuts, *tofMultHist;
  TH2D *hVtxXY, *hVtxXYNoVtxCuts, *TOFvsMultHistCuts;
  TH1D *hVtxZ, *hVtxZNoVtxCuts;
  TH1I *hrunNum;
  TH1D *hRefMult, *hRefMultHLT;
  
  
  if (eventCuts){
    htrigNoVtxCuts = new TH1D("htrigNoVtxCuts","Triggers",3,649999.5,650002.5);
    hRefMultHLT = new TH1D("hRefMultHLT","Reference Multiplicity",500,-0.5,499.5);
    tofMultHist = new TH1D("tofMultHist","TOF Multiplicity",500,0,500);
    hrunNum = new TH1I("hrunNum","Run Number",100,20055000,20059000);
    hVtxXYNoVtxCuts = new TH2D("hVtxXYNoVtxCuts","hVtxXY",200,-10.,10.,200,-10.,10.);
    if(eventConfig == "ColliderCenter") hVtxZNoVtxCuts = new TH1D("hVtxZNoVtxCuts","hVtxZ",421,-210.5,210.5);
    else if(eventConfig == "FixedTarget2015") hVtxZNoVtxCuts = new TH1D("hVtxZNoVtxCuts","hVtxZ",81,179.5,210.5);
  }
  
  if (vertexCuts){
    htrig = new TH1D("htrig","Triggers",3,649999.5,650002.5);
    hVtxXY = new TH2D("hVtxXY","hVtxXY",200,-10.,10.,200,-10.,10.);
    hRefMult = new TH1D("hRefMult","Reference Multiplicity",1000,-0.5,999.5);
    if(eventConfig == "ColliderCenter") hVtxZ = new TH1D("hVtxZ","hVtxZ",421,-210.5,210.5);
    else if(eventConfig == "FixedTarget2015") hVtxZ = new TH1D("hVtxZ","hVtxZ",81,189.5,210.5);
    TOFvsMultHistCuts = new TH2D("TOFvsMultHistCuts","Number of TOF Matches vs. Multiplicity",800,0,200,400,0,100);
  }
  
  Int_t nTrig;
  unsigned short tofMult;
  unsigned short tofMatch;
  int modNum = 1;
  for(Long64_t iEvent=0;iEvent<events2read;iEvent++){
    
    
    
    Bool_t readEvent = picoReader->ReadPicoEvent(iEvent);
    if( !readEvent ) {
      std::cout << "Something went wrong, no events" << endl;
      break;
    }
    
    StPicoDst *dst = picoReader->picoDst();
    
    //  if( iEvent % modNum == 0 ){
    cout << "EventQA - Working on event " << iEvent << endl;
    //if (iEvent == 10*modNum) modNum *= 10;
    //  }
    
    //access data and fill trigger level histograms
    StPicoEvent *event = dst->event();
    if( !event ) {
      cout << "Something went wrong, event is empty" << endl;
      break;
    }
    
    //  cout << "Got event" << endl;
    
    Int_t nPrimaryTracks = 0;
    
    vector <unsigned int> trigIDs = event->triggerIds();
    nTrig = trigIDs.size();
    tofMult = event->btofTrayMultiplicity();
    tofMatch = event->nBTOFMatch();
    for(int itrig=0;itrig<nTrig;itrig++){
      htrigNoCuts->Fill(trigIDs.at(itrig));
    }//currently we cannot access trigger ids, when this changes this code might need to be updated
    
    tofMultHistNoCuts->Fill(tofMult);
    hRefMultNoCuts->Fill( event->refMult() );
    hrunNumNoCuts->Fill(event->runId());
    hVtxZNoCuts->Fill(event->primaryVertex().Z());
    hVtxXYNoCuts->Fill( event->primaryVertex().X(), event->primaryVertex().Y() );
    
    TOFvsMultHistNoCuts->Fill(dst->numberOfTracks(), tofMatch);
    
    if(!eventCuts) continue; //if there are no event cuts we are done filling histograms
    if(!IsGoodEvent(event)) continue; //an event passes this only if there are cuts and that event passes those cuts
    
    Bool_t hltTrig = IsHLT(event);
    
    if (hltTrig){
      hVtxZNoVtxCuts->Fill(event->primaryVertex().Z());
      hVtxXYNoVtxCuts->Fill( event->primaryVertex().X(), event->primaryVertex().Y() );
      
      for(int jtrig=0;jtrig<nTrig;jtrig++){
	htrigNoVtxCuts->Fill(trigIDs.at(jtrig));
      }
      hRefMultHLT->Fill( event->refMult() );
      tofMultHist->Fill(tofMult);
      hrunNum->Fill(event->runId());
      
    }
    
    if(!vertexCuts) continue;
    if(!IsGoodVertex(event)) continue;
    
    for( Int_t iTrk=0; iTrk < (Int_t) dst->numberOfTracks(); iTrk++){
      StPicoTrack *picoTrack = dst->track(iTrk);
      if(IsGoodTrack(picoTrack,event->primaryVertex())) nPrimaryTracks++;
    }
    
    TOFvsMultHistCuts->Fill(nPrimaryTracks, tofMatch);
    
    for(int jtrig=0;jtrig<nTrig;jtrig++){
      htrig->Fill(trigIDs.at(jtrig));
    }
    
    hRefMult->Fill( event->refMult() );
    hVtxZ->Fill(event->primaryVertex().Z());
    hVtxXY->Fill( event->primaryVertex().X(), event->primaryVertex().Y() );
    
    cout << "Finished event" << endl;
    
  }//end loop over events
  
  picoReader->Finish();
  
  outFile->Write();
  outFile->Close();
  
  cout << "Finished with code" << endl;
}//end of function



