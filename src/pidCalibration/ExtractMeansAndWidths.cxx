#include <iostream>
#include <vector>

#include <TFile.h>
#include <TMath.h>
#include <TF1.h>
#include <TString.h>
#include <TTree.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TVirtualFitter.h>
#include <Math/MinimizerOptions.h>

#include "globalDefinitions.h"
#include "ParticleInfo.h"
#include "UserCuts.h"

#include "utilityFunctions.h"
#include "fitZTPCUtilities.h"


//Choose to draw or not
bool draw=false;

//__________________________________________
class SkewedNormalFit{

private:
  TF1 *gaus;
  double gausMinRange;
  double gausMaxRange;

  TF1 *skewedFit;
  double predictedMean;

  double SkewedNormalFunction(double *x, double *par);

public:
  SkewedNormalFit();
  ~SkewedNormalFit();
  int Fit(TH1D *histo);
  void SetPredictedMean(double val){predictedMean = val;}

  TF1 *GetFit(){return skewedFit;}

};

//__________________________________________
SkewedNormalFit::SkewedNormalFit(){

  gausMinRange = -4;
  gausMaxRange = 4;
  gaus = new TF1("","gaus(0)",gausMinRange,gausMaxRange);
  gaus->SetNpx(10000);

  skewedFit = new TF1("",this,&SkewedNormalFit::SkewedNormalFunction,
		      gausMinRange,gausMaxRange,4,"SkewedNormalFit","SkewedNormalFunction");
  skewedFit->SetNpx(10000);
  
}

//__________________________________________
SkewedNormalFit::~SkewedNormalFit(){

  if (gaus)
    delete gaus;

  if (skewedFit)
    delete skewedFit;
}

//__________________________________________
int SkewedNormalFit::Fit(TH1D *histo){
  
  skewedFit->SetParameter(0,100);
  skewedFit->SetParameter(1,predictedMean);
  skewedFit->SetParameter(2,.08);
  skewedFit->SetParameter(3,.9);

  skewedFit->SetParLimits(0,1,10000);
  skewedFit->SetParLimits(1,gausMinRange,gausMaxRange);
  skewedFit->SetParLimits(2,.06,.15);
  skewedFit->SetParLimits(3,0.5,3.0);
  
  return (int)histo->Fit(skewedFit,"NMS","",predictedMean-3*.08,predictedMean+3*.08);
}

//__________________________________________
double SkewedNormalFit::SkewedNormalFunction(double *x, double *par){

  double xx = x[0];

  double amp = par[0];
  double mean = par[1];
  double width = par[2];
  double skew = par[3];

  double gausPars[3] = {amp,mean,width};
  double cdfPars[1] = {skew * xx};

  gaus->SetParameters(gausPars);
  
  return gaus->Eval(xx) * gaus->Integral(gausMinRange,skew*xx);

}

//______MAIN________________________________________________
void ExtractMeansAndWidths(TString inputYieldHistoFile, TString pidCalibrationFile, 
			   TString outFileName, int pid=-1){

  gStyle->SetOptFit(1);
  ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(10000);

  //Create an instance of Particle Info
  ParticleInfo *particleInfo = new ParticleInfo(GetStarLibraryVersion(),true,nRapidityBins,pidCalibrationFile);

  //Open the Particle ID Calibration File
  //TFile *pidFile = new TFile(pidCalibrationFile,"READ");

  //Create an Output File
  TFile *outFile = new TFile(outFileName,"RECREATE");
  
  //Open the input Yield Histogram file
  TFile *yieldHistoFile = new TFile(inputYieldHistoFile,"READ");
  
  //Create a Canvas
  TCanvas *canvas = NULL;
  if (draw){
    canvas = new TCanvas("canvas","canvas",20,20,1600,900);
    canvas->SetLogy();    
  }
  
  //Particles Whose means and widths should be extracted
  std::vector<int> particles;
  particles.push_back(PION);
  particles.push_back(KAON);
  particles.push_back(PROTON);
  particles.push_back(DEUTERON);
  //particles.push_back(TRITON);
  //particles.push_back(ALPHA);

  const unsigned int nParticles = particles.size();
  const unsigned int nCentralityBins = GetNCentralityBins();
  
  //------- TPC ------------
  SkewedNormalFit skewFit;
  //Loop Over All of the Particles Of Interest
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){

    int speciesIndex = particles.at(iParticle);

    //This permits the whole script to be paralleized at the process level
    cout <<pid <<" " <<speciesIndex <<endl;
    if (pid >= 0 && speciesIndex != pid){
      cout <<"Skipping due to input pid request." <<endl;
      continue;
    }

    outFile->cd();
    outFile->mkdir(Form("TOFOptimizedFits_%s",particleInfo->GetParticleName(speciesIndex).Data()));
		   
    //Loop Over the Confounding Particles
    for (unsigned int iConfound=0; iConfound < nParticles; iConfound++){
      
      int confoundSpeciesIndex = particles.at(iConfound);
      double mass = particleInfo->GetParticleMass(speciesIndex);
            
      //Get the TOF Optimized 3D Histogram
      TH3D *h3D = (TH3D *)yieldHistoFile->Get(Form("YieldHistograms/zTPC_%s_%s",
						   particleInfo->GetParticleName(speciesIndex).Data(),
						   particleInfo->GetParticleName(confoundSpeciesIndex).Data()));

      if (!h3D)
	continue;
      
      //Loop Over the Rapidity Bins
      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
	
	double rapidity = GetRapidityRangeCenter(yIndex);
	int yBin = h3D->GetXaxis()->FindBin(rapidity);
	particleInfo->SetEmpiricalBichselFunction(speciesIndex,yIndex);
	
	//Loop Over mTm0 Bins
	for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
	  double mTm0 = GetmTm0RangeCenter(mTm0Index);
	  if (mTm0 > .8)
	    break;
	  
	  double mT = mTm0 + mass;
	  int mTm0Bin = h3D->GetYaxis()->FindBin(mTm0);

	  //Get the Tof Optimized Histogram for the Confounding particle species
	  TH1D *h = (TH1D *)h3D->ProjectionZ("",yBin,yBin,mTm0Bin,mTm0Bin);
	  if (h->GetEntries() < 100){
	    delete h;
	    continue;
	  }

	  double pT = ConvertmTm0ToPt(mTm0,mass);
	  double pZ = ComputepZ(mT,rapidity);
	  double pTotal = ComputepTotal(pT,pZ);
	  
	  double predictedMean = particleInfo->PredictZTPC(pTotal,speciesIndex,confoundSpeciesIndex);

	  if (fabs(predictedMean - h->GetMean()) > 5*.08){
	    delete h;
	    continue;
	  }
	  
	  skewFit.SetPredictedMean(predictedMean);

	  int status(-1);
	  int iTry(0);
	  double chi2NDF(999);
	  status = skewFit.Fit(h);
	  cout <<status <<endl;
	  while (status != 4000 && iTry < 10){
	    cout <<"Retrying Fit: " <<speciesIndex <<" " <<confoundSpeciesIndex <<" " <<yIndex <<" " <<mTm0Index <<endl;
	    status = skewFit.Fit(h);
	    cout <<status <<endl;
	    iTry++;
	  }

	  //Draw
	  if (draw){
	    canvas->cd();
	    h->Draw();
	    canvas->Update();
	    gSystem->Sleep(100);
	  }

	  //Save
	  outFile->cd();
	  outFile->cd(Form("TOFOptimizedFits_%s",particleInfo->GetParticleName(speciesIndex).Data()));
		      
	  TString name = TString::Format("tofOptFit_%s_%s_yIndex%02d_mTm0Index%02d",
					 particleInfo->GetParticleName(speciesIndex).Data(),
					 particleInfo->GetParticleName(confoundSpeciesIndex).Data(),
					 yIndex,mTm0Index);
	  skewFit.GetFit()->Write(name.Data(),TObject::kOverwrite);
	  
	  if (h)
	    delete h;
	  
	}//End Loop Over mTm0Index
	
      }//End Loop Over Rapidity Bins
      
    }//End Loop Over Confounding Particles
    
  }//End Loop Over Particles


  //------- TOF ------------
  TF1 *singleFit = new TF1("singleFit","gaus(0)",-2,2);
  singleFit->SetNpx(10000);
  for (unsigned int iParticle=0; iParticle<nParticles; iParticle++){
    
    int speciesIndex = particles.at(iParticle);
    
    //This permits the whole script to be paralleized at the process level
    cout <<pid <<" " <<speciesIndex <<endl;
    if (pid >= 0 && speciesIndex != pid){
      cout <<"Skipping due to input pid request." <<endl;
      continue;
    }
    
    outFile->cd();
    outFile->mkdir(Form("TOFFits_%s",particleInfo->GetParticleName(speciesIndex).Data()));

    //Get the 3D Histogram
    TH3D *h3D = (TH3D *)yieldHistoFile->Get(Form("YieldHistograms/zTOF_AllCentCharge_%s",
						 particleInfo->GetParticleName(speciesIndex).Data()));
    
    if (!h3D)
      continue;

    //Loop Over the Confounding Particles
    double width = .012;
    for (unsigned int iConfound=0; iConfound < nParticles; iConfound++){
      
      int confoundSpeciesIndex = particles.at(iConfound);
      double mass = particleInfo->GetParticleMass(speciesIndex);

      for (unsigned int yIndex=0; yIndex<nRapidityBins; yIndex++){
	
	double rapidity = GetRapidityRangeCenter(yIndex);
	int yBin = h3D->GetXaxis()->FindBin(rapidity);

	for (unsigned int mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
	  Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);
	  Double_t mT = mTm0 + mass;
	  Double_t pT   = ConvertmTm0ToPt(mTm0,mass);
	  Double_t rapidity = GetRapidityRangeCenter(yIndex);
	  Double_t pZ   = ComputepZ(mTm0+mass,rapidity);
	  Double_t pTotal = ComputepTotal(pT,pZ);
	  
	  int mTm0Bin = h3D->GetYaxis()->FindBin(mTm0);

	  TH1D *h1 = (TH1D *)h3D->ProjectionZ("pz",yBin,yBin,mTm0Bin,mTm0Bin);
	  double histoMin, histoMax;
	  histoMin = h1->GetBinCenter(1);
	  histoMax = h1->GetBinCenter(h1->GetNbinsX()-1);
	  
	  double mean = particleInfo->PredictZTOF(pTotal,speciesIndex,confoundSpeciesIndex);

	  if (mean < histoMin+5*h1->GetBinWidth(1) || mean > histoMax-5*h1->GetBinWidth(1)){
	    delete h1;
	    continue;
	  }	  
	  
	  //Set the Range in which to look for a peak
	  double minFitRange = FindMinBinCenter(h1,mean-3*width,mean);
	  double maxFitRange = FindMinBinCenter(h1,mean,mean+3*width);

	  //Look for the peak and recenter the range around it
	  mean = FindMaxBinCenter(h1,minFitRange,maxFitRange);
	  minFitRange = FindMinBinCenter(h1,mean-3*width,mean);
	  maxFitRange = FindMinBinCenter(h1,mean,mean+3*width);

	  //Make sure another peak isn't too close
	  if (fabs(mean - minFitRange) < 1*width || fabs(mean - maxFitRange) < 1*width){
	    if (mTm0 > 1.0)
	      break;
	    else
	      continue;
	  }

	  //Make sure there are enough entries in the range
	  double rangeEntries = h1->Integral(h1->FindBin(minFitRange),
					     h1->FindBin(maxFitRange));
	  if (rangeEntries < 50){
	    if (mTm0 > 1.0)
	      break;
	    else
	      continue;
	  }

	  

	  //Perform the Fit
	  h1->GetXaxis()->SetRangeUser(minFitRange-.5,maxFitRange+.5);
	  singleFit->SetParameter(0,h1->GetBinContent(h1->FindBin(mean)));
	  singleFit->SetParameter(1,mean);
	  singleFit->SetParameter(2,width);

	  singleFit->SetParLimits(0,singleFit->GetParameter(0)*.5,
				  singleFit->GetParameter(0)*1.5);
	  singleFit->SetParLimits(1,minFitRange,maxFitRange);
	  singleFit->SetParLimits(2,width*.2,width*2);

	  h1->Fit(singleFit,"RNQ","",minFitRange,maxFitRange);

	  if (draw){
	    canvas->cd();
	    h1->Draw();
	    singleFit->Draw("SAME");
	    canvas->Update();
	    gSystem->Sleep(1000);
	  }

	  outFile->cd();
	  outFile->cd(Form("TOFFits_%s",particleInfo->GetParticleName(speciesIndex).Data()));
		      
	  TString name = TString::Format("tofFit_%s_%s_yIndex%02d_mTm0Index%02d",
					 particleInfo->GetParticleName(speciesIndex).Data(),
					 particleInfo->GetParticleName(confoundSpeciesIndex).Data(),
					 yIndex,mTm0Index);
	  singleFit->Write(name.Data(),TObject::kOverwrite);

	  delete h1;

	}//End Loop Over mTm0Bins

      }//End Loop Over Rapidity Bins

    }//End Loop Over Confounding Particles


    delete h3D;
    
  }//End Loop Over Particles
  
}//End Main
			   
