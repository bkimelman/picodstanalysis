#!/bin/bash

#This runs the EventQA.C macro which loads the necessary libraries
#and then runs the eventQAmaker.cxx. The code is run for each root file in the data
# directory.

###########################################################
#SET THE DATA DIRECTORY HERE
#dataFileList="../AuAu_3p0GeV_2019.list"
dataFileList=$1

trigSetup="production_7.3GeV_fixedTarget_2019" #200GeV

round()
{
    echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
};

collisionStr=$(echo $trigSetup| cut -d'_' -f 2)

if [[ $collisionStr =~ "AuAu" ]]
then
    searchstring="AuAu"
    rest=${collisionStr#*$searchstring}
    Beg=$(( ${#collisionStr} - ${#rest} - ${#searchstring} ))
    collisionStrFin=$(( ${Beg} + ${rest} ))
else
    collisionStrFin=$collisionStr
fi

if [[ $collisionStrFin = "31GeV" ]]
then
    energyStrP="31p2"
elif [[ $collisionStrFin =~ "GeV" ]]
then
    energyStrP=${collisionStrFin::-3}
else
    energyStrP=$collisionStrFin
fi

energyStr=$(echo "$energyStrP" | tr "p" .)

if [[ $trigSetup =~ "fixed" || $trigSetup =~ "target" ]]
then
    eventConfig=FixedTarget2015
    collision=AuAu_${energyStrP}GeV_FXT
    energyBC=$(echo "scale=1; sqrt ( 2*(0.9315*0.9315) + 2*($energyStr*0.9315) )" | bc -l)
    energy=$(round $energyBC*1.0 1);

    beamP=$(echo "sqrt ( $energyStr*$energyStr - (0.9315*0.9315) )" | bc -l)
    yCMbc=$(echo "0.5*(l(($energyStr+$beamP)/($energyStr-$beamP)))/2" | bc -l)
    yCM=-$(round $yCMbc 2);
else
    eventConfig=ColliderCenter
    collision=AuAu_${energyStrP}GeV
    energy=$energyStr
    yCM=0.0
fi

echo "Trigger configuration: $trigSetup"
echo "Collision: $collision"
echo "sqrt{s_NN}: ${energy}GeV"
echo "Event config: $eventConfig"
echo "y_CM: $yCM"

#outputDirectory=../userfiles/${collision}_2019_Sept17_19/qa/
outputDirectory=$2
#   outputDirectory=../userfiles/AuAu_3p0GeV_FXT/day$day/run$run/

mkdir -p $outputDirectory

#SET THE NUMBER OF EVENTS HERE (USE -1 FOR ALL)
nEvents=-1 #1000

#IF FALSE PRODUCES ROOT FILE OF HISTOS WITH NO EVENT CUTS,
#IF TRUE INCLUDES HISTOS WITH EVENT CUTS AS WELL
eventCuts=true
vertexCuts=true

#SET THE STAR LIBRARY VERSION
starlib=SL19c

#SET THE COLLISION ENERGY HERE
#    energy=19.6
#    energy=7.7
#   energy=3.0
#    energy=3.9
#    energy=3.2

#SET MIDRAPIDITY
#    yCM=-1.05
#    yCM=-1.37
#    yCM=0
#    yCM=-1.14

#SET THE EVENT CONFIGURATION HERE
#    eventConfig=ColliderCenter
#  eventConfig=FixedTarget2015

########################################################### 

#RUN EVENT QA

processIDEvent=()
outFilesEvent=()

echo "Running on dataFile: " $dataFileList
outFileEvent=$(basename $dataFileList .list)

outFileEvent=$outputDirectory"$outFileEvent"_EventQA_Processed.root

outFilesEvent+=($outFileEvent)

root -l -q -b ../macros/RunEventQA.C\(\"$dataFileList\",\"$outFileEvent\",\"$starlib\",$eventCuts,$vertexCuts,$nEvents,$energy,\"$eventConfig\"\) 

processIDEvent+=($!)
echo ${processIDEvent[@]}
wait ${processIDEvent[@]}

: '
#RUN TRACK QA WITHOUT CUTS
processIDTrNo=()

outFilesTrNo=()

echo "Running on dataFile: " $dataFileList
outFileTrNo=$(basename $dataFileList .list)                                                                                            
outFileTrNo=$outputDirectory"$outFileTrNo"_TrackQA_NoCuts_Processed.root                                                                                        
outFilesTrNo+=($outFileTrNo)                                                                                                                              

root -l -q -b ../macros/RunTrackQA.C\(\"$dataFileList\",\"$outFileTrNo\",\"$starlib\",$yCM,false,false,$nEvents,$energy,\"$eventConfig\"\)                                   

processIDTrNo+=($!)                                                                                                                                   
echo ${processIDTrNo[@]}                                                                                                                                                    
wait ${processIDTrNo[@]}                                                                                                                                    


#RUN TRACK QA WITH SOME CUTS                                                                                                                                                             

processIDTr=()                                                                                                                                                                      
outFilesTr=()            

echo "Running on dataFile: " $dataFileList                                                                                                                                        
outFileTr=$(basename $dataFileList .list)                                                                                                                               
outFileTr=$outputDirectory"$outFileTr"_TrackQA_someTrackCuts_Processed.root                                                                                                   
outFilesTr+=($outFileTr)                                                                                                                                                 

root -l -q -b ../macros/RunTrackQA.C\(\"$dataFileList\",\"$outFileTr\",\"$starlib\",$yCM,true,false,$nEvents,$energy,\"$eventConfig\"\)                                           

processIDTr+=($!)                                                                                                                                                        
echo ${processIDTr[@]}                                                                                                                                                    
wait ${processIDTr[@]}                                                                                                                                                     
'

#RUN TRACK QA WITH ALL CUTS                                                                                                                                                             

processIDTrAll=()                                                                                                                                                                      
outFilesTrAll=()            

echo "Running on dataFile: " $dataFileList                                                                                                                                        
outFileTrAll=$(basename $dataFileList .list)                                                                                                                               
outFileTrAll=$outputDirectory"$outFileTrAll"_TrackQA_allTrackCuts_Processed.root                                                                                                   
outFilesTrAll+=($outFileTrAll)                                                                                                                                                 

root -l -q -b ../macros/RunTrackQA.C\(\"$dataFileList\",\"$outFileTrAll\",\"$starlib\",$yCM,true,true,$nEvents,$energy,\"$eventConfig\"\)                                   

processIDTrAll+=($!)                                                                                                                                                        
echo ${processIDTrAll[@]}                                                                                                                                                    
wait ${processIDTrAll[@]}                                                                                                                                                     

exit
