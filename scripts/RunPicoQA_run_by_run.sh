#!/bin/bash

#This runs the EventQA.C macro which loads the necessary libraries
#and then runs the eventQAmaker.cxx. The code is run for each root file in the data
# directory.

###########################################################
#SET THE DATA DIRECTORY HERE
#dataDirectory=/scratch_rigel/FixedTargetData/AuAl_DavisDSTs/AuAl_3_0_2010/
#dataDirectory=/scratch_altair/bkimelman/data/
#dataDirectory=/scratch_altair/STAR_Run20_Data/AuAu_19p6GeV/

runNumberStart=$1
runNumberEnd=$2

#echo "$ROOTSYS/bin/thisroot.csh"
#source "$ROOTSYS/bin/thisroot.csh"

#trigSetup="testFixed_3p85_2019"  #FXT
#trigSetup="production_19GeV_2019" #19p6GeV
#trigSetup="production_14p5GeV_2019" #14p5GeV
#collision="AuAu_14p5GeV"
#trigSetup="production_7p7GeV_2019" #7p7GeV
#collision="AuAu_7p7GeV"
#trigSetup="production_7.3GeV_fixedTarget_2019" #7p3GeV FXT
#collision="AuAu_7p3GeV"
#trigSetup="production_4p59GeV_fixedTarget_2019" #4p59GeV FXT
#collision="AuAu_4p59GeV"
#trigSetup="production_9p2GeV_2019" #9.2GeV
#trigSetup="production_31GeV_fixedTarget_2019" #31.2GeV -> 7.7GeV FXT
trigSetup="production_AuAu200_2019" #200GeV

round()
{
    echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+0.5)/(10^$2)" | bc))
};

collisionStr=$(echo $trigSetup| cut -d'_' -f 2)

if [[ $collisionStr =~ "AuAu" ]]
then
    searchstring="AuAu"
    rest=${collisionStr#*$searchstring}
    Beg=$(( ${#collisionStr} - ${#rest} - ${#searchstring} ))
    collisionStrFin=$(( ${Beg} + ${rest} ))
else
    collisionStrFin=$collisionStr
fi

if [[ $collisionStrFin = "31GeV" ]]
then
    energyStrP="31p2"
elif [[ $collisionStrFin =~ "GeV" ]]
then
    energyStrP=${collisionStrFin::-3}
else
    energyStrP=$collisionStrFin
fi

energyStr=$(echo "$energyStrP" | tr "p" .)

if [[ $trigSetup =~ "fixed" || $trigSetup =~ "target" ]]
then
    eventConfig=FixedTarget2015
    collision=AuAu_${energyStrP}GeV_FXT
    energyBC=$(echo "scale=1; sqrt ( 2*(0.9315*0.9315) + 2*($energyStr*0.9315) )" | bc -l)
    energy=$(round $energyBC*1.0 1);

    beamP=$(echo "sqrt ( $energyStr*$energyStr - (0.9315*0.9315) )" | bc -l)
    yCMbc=$(echo "0.5*(l(($energyStr+$beamP)/($energyStr-$beamP)))/2" | bc -l)
    yCM=-$(round $yCMbc 2);
else
    eventConfig=ColliderCenter
    collision=AuAu_${energyStrP}GeV
    energy=$energyStr
    yCM=0.0
fi

echo "Trigger configuration: $trigSetup"
echo "Collision: $collision"
echo "sqrt{s_NN}: ${energy}GeV"
echo "Event config: $eventConfig"
echo "y_CM: $yCM"


runNumberNum=$runNumberStart
while [ $runNumberNum -lt $runNumberEnd ]
do
    runNumber=$( printf '%d' $runNumberNum )
    day="${runNumber:2:3}"
    run="${runNumber:5:3}"

    data09=1
    data10=1
    data11=1
    data12=1

    if [ ! -d "/star/data09/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/" ]
    then
	data09=0
    fi
    if [ ! -d "/star/data10/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/" ]
    then
	data10=0
    fi
    if [ ! -d "/star/data11/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/" ]
    then
	data11=0
    fi
    if [ ! -d "/star/data12/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/" ]
    then
	data12=0
    fi

    if [ $data09 -eq 0 ] && [ $data10 -eq 0 ] && [ $data11 -eq 0 ] && [ $data12 -eq 0 ]
    then
	runNumberNum=$(( $runNumberNum + 1 ))
	continue
    fi
    
    fileList=fileLists/run${runNumber}_list.txt
    
    if [ -e $fileList ]
    then
	rm $fileList
    fi

    touch $fileList
    
    if [ $data09 -eq 1 ]
    then
	`ls /star/data09/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/*pico* >> $fileList`
    fi
    if [ $data10 -eq 1 ]
    then
	`ls /star/data10/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/*pico* >> $fileList`
    fi
    if [ $data11 -eq 1 ]
    then
	`ls /star/data11/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/*pico* >> $fileList`
    fi
    if [ $data12 -eq 1 ]
    then
	`ls /star/data12/reco/$trigSetup/ReversedFullField/dev/2019/$day/$runNumber/*pico* >> $fileList`
    fi

#    sed -i '/st_physics_adc_20092055_raw_6000003.picoDst.root/d' $fileList
    
    #SET THE OUTPUT DIRECTORY HERE
#    outputDirectory=../userfiles/AuAu_19p6GeV/day$day/run$run/
    outputDirectory=../userfiles/${collision}_Aug12/day$day/run$run/
#    outputDirectory=../userfiles/AuAu_3p0GeV_FXT/day$day/run$run/
    
    mkdir -p $outputDirectory
    
    #SET THE NUMBER OF EVENTS HERE (USE -1 FOR ALL)
    nEvents=-1 #1000
    
    #IF FALSE PRODUCES ROOT FILE OF HISTOS WITH NO EVENT CUTS,
    #IF TRUE INCLUDES HISTOS WITH EVENT CUTS AS WELL
    eventCuts=true
    vertexCuts=true
    
    #SET THE STAR LIBRARY VERSION
    starlib=SL19c
    
    #SET THE COLLISION ENERGY HERE
#    energy=19.6
#    energy=7.7
 #   energy=3.0
#    energy=3.9
#    energy=3.2
    
    #SET MIDRAPIDITY
    #    yCM=-1.05
    #    yCM=-1.37
    #    yCM=0
#    yCM=-1.14
    
    #SET THE EVENT CONFIGURATION HERE
    #    eventConfig=ColliderCenter
    #  eventConfig=FixedTarget2015
    
    ########################################################### 
    
    #RUN EVENT QA
    
    #Array containing all of the dataFiles
    #dataFiles=( $dataDirectory/*.root )
    #dataFiles=AuAu_4_5GeV_2015_0.root
    processIDEvent=()
    #numberOfFiles=${#dataFiles[@]}
    #numberOfFiles=1
    outFilesEvent=()
    #i=$dataDirectory
    #i+=$dataFiles
    
    for i in `cat $fileList`
    #${dataFiles[@]}
    do
	echo "Running on dataFile: " $i
	outFileEvent=$(basename $i .picoDst.root)
	
	outFileEvent=$outputDirectory"$outFileEvent"_EventQA_Processed.root
	
	outFilesEvent+=($outFileEvent)
	
	root -l -q -b ../macros/RunEventQA.C\(\"$i\",\"$outFileEvent\",\"$starlib\",$eventCuts,$vertexCuts,$nEvents,$energy,\"$eventConfig\"\) 
	
	processIDEvent+=($!)
	echo ${processIDEvent[@]}
    done
    
    wait ${processIDEvent[@]}
    
    suffix=eventQA
    if ($eventCuts); then
	suffix=$suffix"_eventCuts"
    else
	suffix=$suffix"_noEventCuts"
    fi
    if $vertexCuts; then
	suffix=$suffix"_vertexCuts"
    else
	suffix=$suffix"_noVertexCuts"
    fi
    
    hadd $outputDirectory/$suffix.root ${outFilesEvent[@]}
    
    wait
    
    rm ${outFilesEvent[@]}
    
    
    #RUN TRACK QA WITHOUT CUTS
    
    processIDTrNo=()
    #numberOfFiles=${#dataFiles[@]}
    #numberOfFiles=1
    outFilesTrNo=()
    
: '    
    for i in `cat $fileList`
    #${dataFiles[@]}
    do
	echo "Running on dataFile: " $i
	outFileTrNo=$(basename $i .picoDst.root)
	
	outFileTrNo=$outputDirectory"$outFileTrNo"_TrackQA_NoCuts_Processed.root
	
	outFilesTrNo+=($outFileTrNo)
	
	root -l -q -b ../macros/RunTrackQA.C\(\"$i\",\"$outFileTrNo\",\"$starlib\",$yCM,false,false,$nEvents,$energy,\"$eventConfig\"\)
	
	processIDTrNo+=($!)
	echo ${processIDTrNo[@]}
    done
    
    wait ${processIDTrNo[@]}
    
    suffix=trackQA_noCuts
    
    hadd $outputDirectory/$suffix.root ${outFilesTrNo[@]}
    
    wait
    
    rm ${outFilesTrNo[@]}
    
    
    
    #RUN TRACK QA WITH CUTS
    
    processIDTr=()
    #numberOfFiles=${#dataFiles[@]}
    #numberOfFiles=1                                                                      
    outFilesTr=()
    
    for i in `cat $fileList`
    #${dataFiles[@]}
    do
	echo "Running on dataFile: " $i
	outFileTr=$(basename $i .picoDst.root)
	
	outFileTr=$outputDirectory"$outFileTr"_TrackQA_trackCuts_Processed.root
	
	outFilesTr+=($outFileTr)
	
	root -l -q -b ../macros/RunTrackQA.C\(\"$i\",\"$outFileTr\",\"$starlib\",$yCM,true,false,$nEvents,$energy,\"$eventConfig\"\)
	
	processIDTr+=($!)
	echo ${processIDTr[@]}
    done
    
    wait ${processIDTr[@]}
    
    suffix=trackQA_trackCuts
    
    hadd $outputDirectory/$suffix.root ${outFilesTr[@]}
    
    wait
    
    rm ${outFilesTr[@]}
    
    root -l -q -b ../drawingmacros/DrawRun20QA.C\(\"$runNumber\",\"$collision\",$energy,$yCM,\"$eventConfig\",\"$starlib\"\)
    
    convert $outputDirectory/run${runNumber}_eventQA_plots.png $outputDirectory/run${runNumber}_nHits_noCuts.png $outputDirectory/run${runNumber}_nHits_Cuts.png $outputDirectory/run${runNumber}_pT_noCuts.png $outputDirectory/run${runNumber}_pT_Cuts.png $outputDirectory/run${runNumber}_dEdx.png $outputDirectory/run${runNumber}_all_phi_dEdx.png $outputDirectory/run${runNumber}_all_pT_dEdx.png $outputDirectory/run${runNumber}_QA.pdf
'    
    runNumberNum=$(( $runNumberNum + 1 ))
    
done

#Run yieldsRun.C

#root -l -b -q ../macros/yieldRuns.C\($runNumberStart,$runNumberEnd,\"$collision\"\)
 
exit
