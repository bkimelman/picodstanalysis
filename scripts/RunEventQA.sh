#!/bin/bash

#This runs the EventQA.C macro which loads the necessary libraries
#and then runs the eventQAmaker.cxx. The code is run for each root file in the data
# directory.

###########################################################
#SET THE DATA DIRECTORY HERE
#dataDirectory=/scratch_rigel/FixedTargetData/AuAl_DavisDSTs/AuAl_3_0_2010/
#dataDirectory=/scratch_altair/bkimelman/data/
dataDirectory=/scratch_altair/STAR_Run20_Data/AuAu_19p6GeV/

fileList=/star/data03/pwg/bkimel/run20/repoTest/picodstanalysis/userfiles/AuAu_19p6GeV/run20072009_014.txt

#SET THE OUTPUT DIRECTORY HERE
outputDirectory=../userfiles/AuAu_19p6GeV/

#SET THE NUMBER OF EVENTS HERE (USE -1 FOR ALL)
nEvents=-1 #1000

#IF FALSE PRODUCES ROOT FILE OF HISTOS WITH NO EVENT CUTS,
#IF TRUE INCLUDES HISTOS WITH EVENT CUTS AS WELL
eventCuts=true
vertexCuts=true

#SET THE STAR LIBRARY VERSION
starlib=SL16a

#SET THE COLLISION ENERGY HERE
energy=19.6

#SET THE EVENT CONFIGURATION HERE
eventConfig=ColliderCenter

########################################################### 

#Array containing all of the dataFiles
#dataFiles=( $dataDirectory/*.root )
#dataFiles=AuAu_4_5GeV_2015_0.root
processID=()
#numberOfFiles=${#dataFiles[@]}
#numberOfFiles=1
outFiles=()
#i=$dataDirectory
#i+=$dataFiles

for i in `cat $fileList`
#${dataFiles[@]}
do
    echo "Running on dataFile: " $i
    outFile=$(basename $i .picoDst.root)

    outFile=$outputDirectory"$outFile"_EventQA_Processed.root
 
    outFiles+=($outFile)

    root -l -q -b ../macros/RunEventQA.C\(\"$i\",\"$outFile\",\"$starlib\",$eventCuts,$vertexCuts,$nEvents,$energy,\"$eventConfig\"\) 

    processID+=($!)
    echo ${processID[@]}
done

wait ${processID[@]}

suffix=eventQA
if ($eventCuts); then
  suffix=$suffix"_eventCuts"
else
  suffix=$suffix"_noEventCuts"
fi
if $vertexCuts; then
  suffix=$suffix"_vertexCuts"
else
  suffix=$suffix"_noVertexCuts"
fi

hadd $outputDirectory/$suffix"_FXT_v2".root ${outFiles[@]}

wait

rm ${outFiles[@]}

exit
