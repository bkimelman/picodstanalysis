#!/bin/bash

#best fit histogram directory found from RunCentralityDetermination.sh
fitDirectory=../userfiles/AlAu_4_9GeV_2015/centralityBins/

# data sorted by run number with included cuts in UserCuts.cxx
dataFile=../userfiles/AlAu_4_9GeV_2015/centralityVariableRuns/CentralityVariableDistributions.root

#glauber directory
glauberFile=../userfiles/AlAu_4_9GeV_2015/Glauber_27_197_30mb_WoodsSaxon.root

#output directory
outputDirectory=../userfiles/AlAu_4_9GeV_2015/pileupLumi/

energy=4.9

eventConfig="FixedTarget2015"

starLibrary="SL16a"

#region where to normalize. *Should* match the bins used in RunCentralityDetermination.sh
normStartBin=50
normEndBin=120

#bin where all bins beyond are pileup
lastBin=125

#Negative Binomial Parameters
npp=0.400991
k=0.751261

#Number of histograms to be generated
numberOfPercentages=21

#Starting and ending percentages describing how often a pileup event occurs
percentBeg=1
percentEnd=3

#number of Simulated Events
nSimulatedEvents=500000

#arrays
fitFiles=($fitDirectory/CentralityBins.root)
processID=()
numberOfFiles=${#fitFiles[@]}
outFiles=()

for i in ${fitFiles[@]}
do
	echo "Running on file: " $i

	outFile=$(basename $i .root)
	outFile=$outputDirectory/"$outFile"_pileupChiSquaredMinimization.root

	outFiles+=($outFile)

	root -l -q -b ../macros/RunPileUpChiSquaredMinimization.C\(\"$i\",\"$dataFile\",\"$glauberFile\",\"$outFile\",$normStartBin,$normEndBin,$lastBin,$npp,$k,$numberOfPercentages,$percentBeg,$percentEnd,$nSimulatedEvents\) #> /dev/null 2>&1 &

	processID+=($!)
	echo ${processID[@]}
done
wait ${processID[@]}

exit

