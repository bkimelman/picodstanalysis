#!/bin/bash

#This runs the calls the RunSkimmerAndBinner.C macro which loads the necessary libraries
#and then runs skimmerAndBinner.cxx. The code is run for each root file in the data
# directory.

###########################################################
#SET THE DATA DIRECTORY HERE
inputFile=$1

#SET THE OUTPUT DIRECTORY HERE
outputDirectory=$2
echo $outputDirectory
#SET THE NUMBER OF EVENTS HERE (USE -1 FOR ALL)
nEvents=-1 #1000

#SET THE STAR LIBRARY VERSION
starlib=SL16a

#SET THE COLLISION ENERGY HERE
energy=3.9

#SET THE EVENT CONFIGURATION HERE
eventConfig=FixedTarget2015

########################################################### 

#Array containing all of the dataFiles
processID=()
echo "Running on dataFile: " $inputFile

outFile=$(basename $inputFile .root)
outFile=$outputDirectory/"$outFile"_Processed.root

root -l -q -b ../macros/RunSkimmerAndBinner.C\(\"$inputFile\",\"$starlib\",$nEvents,\"$outFile\",$energy,\"$eventConfig\"\) > /dev/null 2>&1 &

processID+=($!)
echo ${processID[@]}
wait ${processID[@]}

exit
