#!/bin/bash

#This runs the RunTrackQA.C macros which loads the necessary libraries
#and then runs the trackQAmaker.cxx binary. The code is run for each root file in the data
# directory.

###########################################################
#SET THE DATA DIRECTORY HERE
#dataDirectory=/scratch_altair/bkimelman/data/
dataDirectory=/scratch_altair/STAR_Run20_Data/AuAu_19p6GeV/FastOffline/run20070007/picoDsts/

fileList=/star/data03/pwg/bkimel/run20/repoTest/picodstanalysis/userfiles/AuAu_19p6GeV/run20072009_014.txt

#SET THE OUTPUT DIRECTORY HERE
outputDirectory=../userfiles/AuAu_19p6GeV/

#SET THE NUMBER OF EVENTS HERE (USE -1 FOR ALL)
nEvents=-1 #1000

#IF FALSE PRODUCES THE ROOT FILE OF HISTOS WITH NO TRACK CUTS
#IF TRUE PRODUCES THE ROOT FILE OF HISTOS WITH TRACKS CUTS
trackCuts=false #Note if this is true, the output root file will NOT have pre-cut plots

#SET MIDRAPIDITY
yCM=0
  
#SET THE STAR LIBRARY VERSION
starlib=SL16a

#SET THE COLLISION ENERGY HERE
energy=19.6

#SET THE EVENT CONFIGURATION HERE
eventConfig=ColliderCenter

########################################################### 

#Array containing all of the dataFiles
#dataFiles=( $dataDirectory/*.root )
#dataFiles=AuAu_4_5GeV_2015_0.root
processID=()
numberOfFiles=${#dataFiles[@]}
#numberOfFiles=1
outFiles=()
#i=$dataDirectory
#i+=$dataFiles

for i in `cat $fileList`#${dataFiles[@]}
do
    echo "Running on dataFile: " $i
    outFile=$(basename $i .picoDst.root)

    outFile=$outputDirectory"$outFile"_TrackQA_Processed.root
 
    outFiles+=($outFile)

    root -l -q -b ../macros/RunTrackQA.C\(\"$i\",\"$outFile\",\"$starlib\",$yCM,$trackCuts,$nEvents,$energy,\"$eventConfig\"\) 

    processID+=($!)
    echo ${processID[@]}
done

wait ${processID[@]}

suffix=trackQA
if $trackCuts; then
  suffix=$suffix"_trackCuts" 
else
  suffix=$suffix"_noCuts"
fi

hadd $outputDirectory/$suffix"_tof".root ${outFiles[@]}

wait

rm ${outFiles[@]}

exit
